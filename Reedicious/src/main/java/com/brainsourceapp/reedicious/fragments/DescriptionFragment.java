package com.brainsourceapp.reedicious.fragments;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.brainsourceapp.reedicious.R;
import com.brainsourceapp.reedicious.beans.MyAccount;
import com.brainsourceapp.reedicious.rest.results.Gallery;
import com.brainsourceapp.reedicious.rest.results.PlaceDetail;
import com.brainsourceapp.reedicious.transformers.image.RoundedTransformation;
import com.brainsourceapp.reedicious.views.DescPropertyView;
import com.flurry.android.FlurryAgent;
import com.squareup.picasso.Picasso;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.res.StringRes;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@EFragment(R.layout.fragment_description)
public class DescriptionFragment extends Fragment {
    public static int imageWidth, imageHeight;

    private static final String TAG_LOC = "location";
    private static final String TAG_OPENTIME = "open_time";
    private static final String TAG_TEL = "telephone";
    private static final String TAG_WEB = "web";
    private static final String TAG_FACE = "facebook";

    public interface DescriptionFragmentListener {
        public void onFBShareButtonClicked();
    }

    public static DescriptionFragment newInstance( PlaceDetail.Data mData ) {
        DescriptionFragment fragment = new DescriptionFragment_();

        fragment.setGalleryList(mData.getGalleryList());
        fragment.title = mData.title;
        fragment.desc = mData.desc;
        fragment.displayedLocation = mData.address;
        fragment.displayedOpenTime = mData.openTime;
        fragment.displayedTelephoneNumber = mData.phone;
        fragment.displayedWebURL = mData.website;
        if(mData.getContacts() != null && mData.getContacts().size() > 0) {
            fragment.displayedFacebookURL = mData.getContacts().get(0);
        }

        return fragment;
    }

    private boolean isOnScreen;
    public boolean isOnScreen() {
        return isOnScreen;
    }
    public void setOnScreen(boolean onScreen) {
        isOnScreen = onScreen;
    }

    ArrayList<Gallery> galleries;
    public void setGalleryList(ArrayList<Gallery> galleryList) {
        this.galleries = new ArrayList<Gallery>();
        for(Gallery item : galleryList) {
            this.galleries.add( item );
        }
    }
    public ArrayList<Gallery> getGalleryList() {
        ArrayList<Gallery> list = new ArrayList<Gallery>();
        for(Gallery item : this.galleries) {
            list.add( item );
        }
        return list;
    }

    String title;
    String desc;
    String displayedLocation;
    String displayedOpenTime;
    String displayedTelephoneNumber;
    String displayedWebURL;
    String displayedFacebookURL;

    @ViewById(R.id.detail_view_pager)
    ViewPager mViewPager;

    @ViewById(R.id.detail_page_indicator_text)
    TextView mIndicText;

    @ViewById(R.id.detail_page_indicator_left_button)
    Button leftButton;

    @ViewById(R.id.description_location_container)
    DescPropertyView locationView;

    @ViewById(R.id.description_officetime_container)
    DescPropertyView timeView;

    @ViewById(R.id.description_telephone_container)
    DescPropertyView telView;

    @ViewById(R.id.description_website_container)
    DescPropertyView webView;

    @ViewById(R.id.description_facebook_container)
    DescPropertyView facebookView;

    @Click(R.id.detail_page_indicator_left_button)
    void onLeftButtonClicked() {
        int current = mViewPager.getCurrentItem();

        if(0 <= current - 1) {
            mViewPager.setCurrentItem( current - 1, true );
        }
    }

    @ViewById(R.id.detail_page_indicator_right_button)
    Button rightButton;

    @Click(R.id.detail_page_indicator_right_button)
    void onRightButtonClicked() {
        int current = mViewPager.getCurrentItem();

        if(current + 1 < galleries.size()) {
            mViewPager.setCurrentItem( current + 1, true );
        }
    }

    @ViewById(R.id.detail_title)
    TextView mTitleText;

    @ViewById(R.id.detail_description)
    TextView mDescText;

    @StringRes(R.string.address_label)
    String addressLabel;

    @StringRes(R.string.open_time_label)
    String opentimeLabel;

    @StringRes(R.string.telephone_label)
    String telephoneLabel;

    @AfterViews
    void afterDescriptionViews() {
        assert getActivity() != null;
        Activity activity = getActivity();

        // Set actionbar title
        ActionBar actionBar = activity.getActionBar();
        assert actionBar != null;
        actionBar.setTitle( title );

        // Get ScreenSize
        DisplayMetrics dm = new DisplayMetrics();
        if(activity != null) {
            WindowManager wm = activity.getWindowManager();

            if(wm != null) {
                activity.getWindowManager().getDefaultDisplay().getMetrics( dm );

                imageWidth = activity.getResources().getDimensionPixelOffset( R.dimen.description_image_width );
                imageHeight = activity.getResources().getDimensionPixelSize( R.dimen.description_image_height );
            }
        }

        mIndicText.setText( String.format("1 / %d", galleries.size()) );

        MyPagerAdapter adapter = new MyPagerAdapter(getActivity(), galleries);
        setButtonEnabled(leftButton, false);
        mViewPager.setAdapter( adapter );

        mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i2) {

            }

            @Override
            public void onPageSelected(int i) {
                mIndicText.setText( String.format("%d / %d", i + 1, galleries.size()) );

                if(i == 0) {
                    setButtonEnabled(leftButton, false);
                    return;
                }

                if(i + 1 == galleries.size() ) {
                    setButtonEnabled(rightButton, false);
                    return;
                }

                if(!leftButton.isEnabled()) {
                    setButtonEnabled(leftButton, true);
                }

                if(!rightButton.isEnabled()) {
                    setButtonEnabled(rightButton, true);
                }

            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });

        mTitleText.setText( title );

        mDescText.setText( desc );

        // Init IconWithTitle Views (Text)
        locationView.setDynamicHeight( true );
        locationView.setTitleText( addressLabel + displayedLocation );
        timeView.setTitleText( displayedOpenTime + displayedOpenTime );
        telView.setTitleText( displayedTelephoneNumber );
        webView.setTitleText( displayedWebURL );
        facebookView.setTitleText( displayedFacebookURL );

        // Init IconWithTitle Views (Icon)
        locationView.setImageWithResourceID(R.drawable.icon_location);
        timeView.setImageWithResourceID(R.drawable.icon_time);
        telView.setImageWithResourceID(R.drawable.icon_phone);
        webView.setImageWithResourceID(R.drawable.icon_net);
        facebookView.setImageWithResourceID(R.drawable.facebook);

        // Set it's type
        locationView.settype(ShopDesc.LOCATION_TAB);
        timeView.settype(ShopDesc.OFFICETIME_TAB);
        telView.settype(ShopDesc.TELEPHONE_TAB);
        webView.settype(ShopDesc.WEB_TAB);
        facebookView.settype(ShopDesc.FACEBOOK_TAB);

        Map<String, String> param = new HashMap<String, String>();
        param.put("screenName", MyAccount.getInstance().getAccountScreenName());
        param.put("placeTitle", title);
        FlurryAgent.logEvent("Read Description in Details Page", param);
    }

    private void setButtonEnabled(View view, boolean enabled) {
        view.setEnabled( enabled );
        view.setAlpha( enabled ? 1 : 0.5f );
    }

    /** ------------------------------------------------------------------
     *
     *  Nested MyPagerAdapterClass
     *
     *  ------------------------------------------------------------------
     */
    public static class MyPagerAdapter extends PagerAdapter implements View.OnClickListener {

        private LayoutInflater mLayoutInflater;
        private Context context;
        private ArrayList<Gallery> mData;

        public MyPagerAdapter(Context context, ArrayList<Gallery> mData) {

            this.context = context;
            this.mLayoutInflater = ((Activity) context).getLayoutInflater();
            this.mData = new ArrayList<Gallery>();
            this.mData.addAll(mData);
        }

        @Override
        public int getCount() {
            return mData.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object o) {
            return view == o;
        }

        @Override
        public View instantiateItem(ViewGroup container, int position) {
            View view = mLayoutInflater.inflate(R.layout.fragment_image, null);

            if(view != null) {
                ImageView imageView = (ImageView) view.findViewById(R.id.detail_gallery_image);
                Button button = (Button) view.findViewById(R.id.detail_gallery_share_button);

                try {
                    Picasso.with( context )
                            .load( mData.get(position).url )
                            .placeholder(R.drawable.default_700x500)
                            .transform(new RoundedTransformation(10, 0))
                            .into(imageView);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                button.setOnClickListener(this);

                container.addView(view, 0);
            }
            return view;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

        @Override
        public void onClick(View view) {
            // TODO: Share to facebook here
            Log.d("test", "share facebook");

            try {
                ((DescriptionFragmentListener) this.context).onFBShareButtonClicked();
            } catch (Exception e) {
                Log.d("test", " error share facebook button : " + e.getMessage());
            }
        }
    }

}
