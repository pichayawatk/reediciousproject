package com.brainsourceapp.reedicious;

import android.app.ActionBar;
import android.app.Activity;
import android.graphics.Typeface;
import android.support.v4.app.NavUtils;
import android.text.SpannableString;
import android.text.Spanned;
import android.util.Log;

import com.brainsourceapp.reedicious.beans.MyAccount;
import com.flurry.android.FlurryAgent;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.OptionsItem;
import org.androidannotations.annotations.OptionsMenu;
import org.androidannotations.annotations.res.StringRes;

import java.util.HashMap;
import java.util.Map;

@EActivity(R.layout.activity_recommended)
@OptionsMenu(R.menu.recommended)
public class RecommendedActivity extends Activity {

    /**
     * Menu Category
     * 0    :   All
     * 1    :   Inter
     * 2    :   Bakery
     * 3    :   Thai
     * 5    :   Health
     * 7    :   Unseen
     */
    @Click(R.id.recommended_inter)
    void onInterButtonClicked() {
        didClickMenuButton(1);
    }

    @Click(R.id.recommened_thai)
    void onThaiButtonClicked() {
        didClickMenuButton(3);
    }

    @Click(R.id.recommened_bakery)
    void onBakeryClicked() {
        didClickMenuButton(2);
    }

    @Click(R.id.recommended_fusion)
    void onFusionClicked() {
        didClickMenuButton(5);
    }

    @Click(R.id.recommened_dishes)
    void onDishesClicked() {
        didClickMenuButton(7);
    }

    void didClickMenuButton(int catID) {
        String title = getResources().getStringArray(R.array.menu_titles)[catID];

        Map<String, String> map = new HashMap<String, String>();
        map.put("screenName", MyAccount.getInstance().getAccountScreenName());
        map.put("type", title);
        FlurryAgent.logEvent("View Recommended Menu with Type", map);

        MenuListActivity_.intent(this).catID(catID).mTitle(title).limit(20).start();
    }

    @AfterViews
    void afterRecommendedViews() {

        Log.d("test", "recommended acivity start...");
        Log.d("test", "my context is : " + this);
        /** my context is : com.brainsourceapp.reedecious.RecommendedActivity_ */

        setupActionBar();
    }

    @StringRes(R.string.title_recommended_activity)
    String mTitle;

    private void setupActionBar() {

        ActionBar actionBar = getActionBar();
        if(actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);

            // Set custom text size and color
            SpannableString sTitle = new SpannableString( mTitle );
            Typeface font = Typeface.createFromAsset(getAssets(), "fonts/th_k2d.ttf");
            sTitle.setSpan(font, 0, sTitle.length(), Spanned.SPAN_INCLUSIVE_INCLUSIVE);
            actionBar.setTitle(sTitle.toString());
        }
    }

    @OptionsItem(android.R.id.home)
    void didClickedHomeMenu() {
        try {
            NavUtils.navigateUpFromSameTask(this);
        } catch (Exception e) {
            finish();
        }
    }
}
