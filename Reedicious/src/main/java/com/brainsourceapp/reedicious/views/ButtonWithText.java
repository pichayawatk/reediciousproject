package com.brainsourceapp.reedicious.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.brainsourceapp.reedicious.R;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

@EViewGroup(R.layout.btn_subtext)
public class ButtonWithText extends LinearLayout {

    public ButtonWithText(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @ViewById(R.id.mbtn_subtext_ain_layout)
    View mainLayout;

    @ViewById(R.id.btn_with_text_btn)
    Button btn;

    @ViewById(R.id.btn_with_text_text)
    TextView subTitleText;

    public void bind(String btnText, String subTitle) {
        this.btn.setText( btnText );
        this.subTitleText.setText( subTitle );
    }

    public void hideButton() {
        this.btn.setVisibility(GONE);
    }

    @Override
    public void setOnClickListener(OnClickListener l) {
        btn.setOnClickListener( l );
    }
}
