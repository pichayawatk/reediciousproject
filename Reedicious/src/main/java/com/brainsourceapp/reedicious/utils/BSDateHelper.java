package com.brainsourceapp.reedicious.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class BSDateHelper {

    private static BSDateHelper instance;

    private BSDateHelper() {

    }

    public static BSDateHelper getInstance() {
        if(instance == null) {
            instance = new BSDateHelper();
        }
        return instance;
    }

    public static String timestampToThaiDateFormat(long timestamp, String format) {
        try {
            java.util.Date date = new Date(timestamp * 1000);
            DateFormat dateFormat = new SimpleDateFormat(format);
            String dateString = dateFormat.format( date );
            String dates[] = dateString.split("[//]");

            String day = trimDay(dates[0]);
            String month = convertToThaiMonth(dates[1]);
            String year = convertToThaiYear(dates[2]);

            return day + " " + month + " " + year;
        } catch (Exception e) {
            return timestamp + "";
        }
    }

    public static String timestampToThaiDateFormat(long timestamp, String format, String separator) {
        try {
            java.util.Date date = new Date(timestamp * 1000);
            DateFormat dateFormat = new SimpleDateFormat(format);
            String dateString = dateFormat.format( date );
            String dates[] = dateString.split(separator);

            String day = trimDay(dates[0]);
            String month = convertToThaiMonth(dates[1]);
            String year = convertToThaiYear(dates[2]);

            return day + " " + month + " " + year;
        } catch (Exception e) {
            return timestamp + "";
        }
    }

    private static String trimDay(String dayStr) {
        if(dayStr == null || dayStr.trim().length() == 0) {
            dayStr = "1";
        }
        int dayInt = Integer.parseInt(dayStr);
        return dayInt + "";
    }

    public static final String[] THAI_MONTHS = {"มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม"};
    private static String convertToThaiMonth(String monthStr) {
        if(monthStr == null || monthStr.trim().length() == 0) {
            monthStr = "1";
        }
        int monthInt = Integer.valueOf(monthStr);
        String ret = monthStr;
        if(monthInt >= 1 && monthInt <= 12) {
            ret = THAI_MONTHS[monthInt - 1];
        }
        return ret;
    }

    private static String convertToThaiYear(String yearStr) {
        if(yearStr == null || yearStr.trim().length() == 0) {
            yearStr = "2000";
        }
        int yearInt = Integer.parseInt(yearStr);
        yearInt += 543;
        return yearInt + "";
    }
}
