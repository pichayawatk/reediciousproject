package com.brainsourceapp.reedicious.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

import com.brainsourceapp.reedicious.R;

public class RDTextView extends TextView {

    public static final String Regular = "fonts/th_k2d.ttf";
    public static final String Bold = "fonts/th_k2d_bold.ttf";
    public static final String BoldItalic = "fonts/th_k2d_bold_italic.ttf";
    public static final String Italic = "fonts/th_k2d_italic.ttf";

    public RDTextView(Context context) {
        super(context);
    }

    public RDTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        if(!isInEditMode()) {
            parseAttributes(context, attrs);
        }
    }

    public RDTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        if(!isInEditMode()) {
            parseAttributes(context, attrs);
        }
    }

    private void parseAttributes(Context context, AttributeSet attrs) {
        TypedArray values = context.obtainStyledAttributes(attrs, R.styleable.RDTextView);

        assert values != null;
        int typeface = values.getInt(R.styleable.RDTextView_typeface, 0);

        switch (typeface) {
            case 0:
                setTypeface(getFontRegular( context ));
                break;
            case 1:
                setTypeface(getFontBold( context ));
                break;
            case 2:
                setTypeface(getFontBoldItalic( context ));
                break;
            case 3:
                setTypeface(getFontItalic( context ));
                break;
        }
    }

    /**
     * Singleton font typeface
     */
    private static Typeface font;
    public static Typeface getFontRegular(Context context) {
        if(font == null) {
            font = Typeface.createFromAsset(context.getAssets(), Regular);
        }
        return font;
    }
    private static Typeface fontBold;
    public static Typeface getFontBold(Context context) {
        if(fontBold == null) {
            fontBold = Typeface.createFromAsset(context.getAssets(), Bold);
        }
        return fontBold;
    }
    private static Typeface fontBoldItalic;
    public static Typeface getFontBoldItalic(Context context) {
        if(fontBoldItalic == null) {
            fontBoldItalic = Typeface.createFromAsset(context.getAssets(), BoldItalic);
        }
        return fontBoldItalic;
    }
    private static Typeface fontItalic;
    public static Typeface getFontItalic(Context context) {
        if(fontItalic == null) {
            fontItalic = Typeface.createFromAsset(context.getAssets(), Italic);
        }
        return fontItalic;
    }

}
