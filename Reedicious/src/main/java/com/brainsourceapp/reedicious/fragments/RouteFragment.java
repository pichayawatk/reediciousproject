package com.brainsourceapp.reedicious.fragments;

import android.content.DialogInterface;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.brainsourceapp.reedicious.R;
import com.brainsourceapp.reedicious.beans.MyAccount;
import com.brainsourceapp.reedicious.rest.data.MyMinimart;
import com.brainsourceapp.reedicious.rest.results.PlaceDetail;
import com.brainsourceapp.reedicious.utils.BSAlertDialog;
import com.brainsourceapp.reedicious.utils.BSDialogIntentBuilder;
import com.brainsourceapp.reedicious.utils.RDMapHelper;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

public class RouteFragment extends MapFragment implements GooglePlayServicesClient.ConnectionCallbacks, GooglePlayServicesClient.OnConnectionFailedListener {

    public static RouteFragment newInstance(PlaceDetail.Data mData, LatLng currentLatLng, boolean ignoreCurrentLocation) {
        RouteFragment fragment = new RouteFragment();

        double lat = currentLatLng.latitude, lng = currentLatLng.longitude;

        try {
            lat = Double.valueOf( mData.lat );
            lng = Double.valueOf( mData.lng );
        } catch (Exception e) {
            Log.d("test", "... from api lat, lng : ( " + mData.lat + ", " + mData.lng + " )");
        }

        fragment.destinationLatitude = lat;
        fragment.destinationLongitude = lng;
        fragment.currentLatLng = new LatLng(currentLatLng.latitude, currentLatLng.longitude);
        fragment.respinID = RDMapHelper.getPinResID(mData.category);
        fragment.ignoreCurrentLocation = ignoreCurrentLocation;
        fragment.mTitle = mData.title;

        // Minimart
        fragment.minimartList = (ArrayList<MyMinimart>) mData.getMinimartList();

        return fragment;
    }

    float zoomBound;

    public static final int DEFAULT_ZOOM_LEVEL = 13;

    private int respinID;

    private static final String DLAT = "destinationLatitude";
    private static final String DLNG = "destinationLongitude";
    private static final String MART = "minimartList";

    private double destinationLatitude;
    private double destinationLongitude;

    private LatLng currentLatLng;

    private boolean ignoreCurrentLocation;

    ArrayList<MyMinimart> minimartList;

    String mTitle;

    LocationClient locationClient;
    Location location;

    Marker mMarker;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(!ignoreCurrentLocation) {
            locationClient = new LocationClient(getActivity(), this, this);
        }

        assert getMap() != null;

        if(savedInstanceState != null) {

            if(savedInstanceState.containsKey(DLAT))       destinationLatitude = savedInstanceState.getDouble(DLAT);
            if(savedInstanceState.containsKey(DLNG))       destinationLongitude = savedInstanceState.getDouble(DLNG);
            if(savedInstanceState.containsKey(MART)) {
                ArrayList<MyMinimart> list = savedInstanceState.getParcelableArrayList(MART);
                minimartList = new ArrayList<MyMinimart>();
                minimartList.addAll( list );
            }
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putDouble(DLAT, destinationLatitude);
        outState.putDouble(DLNG, destinationLongitude);
        outState.putParcelableArrayList(MART, minimartList);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);

        try {

            initMapIfNeed();

            if(!ignoreCurrentLocation) {
                zoomBound = ( getMap().getMaxZoomLevel() + getMap().getMinZoomLevel() ) / 2;
            }
        } catch (Exception e) {
            Log.d("test", "reason : " + e.getLocalizedMessage());
            e.printStackTrace();
        }

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();

        if(!ignoreCurrentLocation && locationClient != null)  locationClient.connect();
    }

    @Override
    public void onStop() {
        if(!ignoreCurrentLocation && locationClient != null)  locationClient.disconnect();

        super.onStop();
    }

    /**
     *  Callback
     *
     */
    @Override
    public void onConnected(Bundle bundle) {

        // Get last location
        location = locationClient.getLastLocation();
    }

    public synchronized void updateLayout() {

        try {
            initMapIfNeed();
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            initrouteMapViews();

            updateCamera();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void initMapIfNeed() throws Exception {

        getMap().setMyLocationEnabled(ignoreCurrentLocation);
        getMap().getUiSettings().setMyLocationButtonEnabled(true);
        getMap().getUiSettings().setCompassEnabled(true);
        getMap().getUiSettings().setAllGesturesEnabled(true);
        getMap().getUiSettings().setZoomGesturesEnabled(true);
    }

    void initrouteMapViews() throws Exception {

            LatLng dlatLng = new LatLng( destinationLatitude, destinationLongitude );

            // Add Marker
            mMarker = getMap().addMarker(
                    new MarkerOptions()
                            .position( dlatLng )
                            .title(mTitle)
                            .icon(BitmapDescriptorFactory.fromResource(respinID))
            );

            // Add Minimart
            for(MyMinimart minimart : minimartList) {
                getMap().addMarker(
                        new MarkerOptions()
                            .position(new LatLng(minimart.getMartLat(), minimart.getMartLng()))
                            .icon(BitmapDescriptorFactory.fromResource( R.drawable.ic_minimart )));
            }

                    // Listener
                    getMap().setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                        @Override
                        public boolean onMarkerClick(Marker marker) {
                            assert getActivity() != null;
                            BSAlertDialog.newInstance(getActivity().getResources().getString(R.string.call_googlemap_direction), new BSAlertDialog.CallbackDialogListener() {
                                @Override
                                public void onBSDialogPosBtnClicked(DialogInterface dialog, String tag) {
                                    dialog.dismiss();

                                    BSDialogIntentBuilder.newInstance(getActivity()).openGoogleMapIntent(
                                            location.getLatitude(), location.getLongitude(),
                                            destinationLatitude, destinationLongitude,
                                            mTitle
                                    );
                                }
                            });
                            return true;
                        }
                    });

            getMap().setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                @Override
                public boolean onMarkerClick(Marker marker) {
                    try {
                        // TODO: dialog for google map
                        assert getActivity() != null;
                        BSAlertDialog.newInstance(getActivity().getResources().getString(R.string.call_googlemap_direction), new BSAlertDialog.CallbackDialogListener() {
                            @Override
                            public void onBSDialogPosBtnClicked(DialogInterface dialog, String tag) {
                                // TODO: open googlemap intent
                                BSDialogIntentBuilder.newInstance(getActivity()).openGoogleMapIntent(
                                        location.getLatitude(), location.getLongitude(),
                                        destinationLatitude, destinationLongitude,
                                        mTitle
                                );
                            }
                        }).show(getActivity().getFragmentManager(), "alert");
                    }
                    catch (Exception e) {
                        e.printStackTrace();
                    }
                    return true;
                }
            });


        if(location == null) {
            location = getMap().getMyLocation();
        }

        updateCamera();
    }

    LatLngBounds latLngBounds;

    void updateCamera() throws Exception {

        if(getMap() == null) {
            return;
        }

        if(ignoreCurrentLocation) {
            assert getActivity() != null;

            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    getMap().animateCamera(
                            CameraUpdateFactory.newLatLngZoom( new LatLng(destinationLatitude, destinationLongitude), DEFAULT_ZOOM_LEVEL),
                            400,
                            null
                    );
                }
            });

            return;
        }

        double curLat, curLng;

        try {
            curLat = location.getLatitude();
            curLng = location.getLongitude();
        } catch (Exception e) {
            curLat = MyAccount.getInstance().getLatestLatLng().latitude;
            curLng = MyAccount.getInstance().getLatestLatLng().longitude;
        }

        Log.d("test", "lat, lng : ( " + curLat + ", " + curLng + " )");

        // Exceed 90 degress
        if(curLat < -90) {
            curLat = -180 - curLat;
            curLng = curLng + 180;
        }
        if(curLat > 90) {
            curLat = 180 - curLat;
            curLng = curLng + 180;
        }

        LatLng curlatLng = new LatLng(curLat, curLng);
        LatLng destLatLng = new LatLng( destinationLatitude, destinationLongitude );

        if(curlatLng.latitude >= destinationLatitude) {
            latLngBounds = new LatLngBounds(
                    destLatLng, curlatLng
            );
        } else {

            latLngBounds = new LatLngBounds(
                    curlatLng, destLatLng
            );
        }

        getMap().setOnMyLocationChangeListener(new GoogleMap.OnMyLocationChangeListener() {
            @Override
            public void onMyLocationChange(Location location) {

                LatLngBounds.Builder builder = new LatLngBounds.Builder();
                if (location != null) {
                    currentLatLng = new LatLng(location.getLatitude(), location.getLongitude());
                }
                builder.include(currentLatLng);
                builder.include(new LatLng(destinationLatitude, destinationLongitude));
                LatLngBounds bounds = builder.build();

                float zoom = getMap().getCameraPosition().zoom;

                if (zoom < zoomBound) {
                    zoom += 2;

                    getMap().animateCamera(
                            CameraUpdateFactory.newLatLngZoom(bounds.getCenter(), zoom),
                            600,
                            null
                    );
                }

            }

        });
    }

    @Override
    public void onDisconnected() {
        // TODO: fallback handler.
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        // TODO: fallback handler.
    }
}
