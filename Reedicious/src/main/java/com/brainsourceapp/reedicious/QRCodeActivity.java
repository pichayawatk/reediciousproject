package com.brainsourceapp.reedicious;

import android.app.ActionBar;
import android.app.Activity;
import android.content.DialogInterface;
import android.graphics.PointF;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;

import com.brainsourceapp.reedicious.beans.MyAccount;
import com.brainsourceapp.reedicious.database.QRCodeDBHelper;
import com.brainsourceapp.reedicious.database.data.MyQRItem;
import com.brainsourceapp.reedicious.utils.BSAlertDialog;
import com.brainsourceapp.reedicious.utils.BSDialogIntentBuilder;
import com.dlazaro66.qrcodereaderview.QRCodeReaderView;
import com.flurry.android.FlurryAgent;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.OptionsItem;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import java.util.HashMap;
import java.util.Map;

@EActivity(R.layout.activity_qr_code)
public class QRCodeActivity extends Activity implements QRCodeReaderView.OnQRCodeReadListener, BSAlertDialog.CallbackDialogListener {

    @ViewById(R.id.qr_decoderview)
    QRCodeReaderView decoderView;

    @ViewById(R.id.qr_decodermask)
    ImageView decoderMask;

    boolean cansave;

    MyQRItem item;

    boolean isMaskOpened;

    QRCodeDBHelper qrcodeDbHelper;

    @Click(R.id.qr_history_btn)
    public void onqrhistoryClicked() {
        QRHistoryActivity_.intent(this).start();
    }

    @ViewById(R.id.qr_camera_btn)
    Button cameraBtn;

    @Click(R.id.qr_camera_btn)
    public void oncameraBtnClicked() {

        boolean show = !isMaskOpened;
        showdecoderView(show, null);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(qrcodeDbHelper == null) {
            qrcodeDbHelper = new QRCodeDBHelper(this);
        }
        qrcodeDbHelper.open();
    }

    @AfterViews
    void afterQRViews() {

        FlurryAgent.logEvent("View QR Scan Page");

        try {
            setupActionBar();
            qrcodeDbHelper.open();
            showdecoderView(true, null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @UiThread
    public void showdecoderView(boolean show, Integer millisec) {
        cansave = show;
        isMaskOpened = show;
        if(show) {
            // if show = open Camera
            Animation anim = AnimationUtils.loadAnimation(this, R.anim.collapse_to_top);
            if(anim != null) {
                if(millisec != null) {
                    anim.setDuration( millisec );
                } else {
                    anim.setDuration( 0 );
                }
                anim.setFillAfter(true);
                decoderMask.startAnimation(anim);
                decoderView.setOnQRCodeReadListener(QRCodeActivity.this);
                decoderView.getCameraManager().startPreview();
            }

        } else {
            // if !show = close Camera
            Animation anim = AnimationUtils.loadAnimation(this, R.anim.extend_to_bottom);
            if(anim != null) {
                if(millisec != null) {
                    anim.setDuration( millisec );
                }
                anim.setFillAfter(true);
                decoderMask.startAnimation(anim);
                decoderView.getCameraManager().stopPreview();
            }
        }
    }

    @Override
    protected void onPause() {
        qrcodeDbHelper.close();
        showdecoderView(false, 0);
        super.onPause();
    }

    private void setupActionBar() {
        ActionBar actionBar = getActionBar();
        if(actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    @OptionsItem(android.R.id.home)
    void didClickedHomeMenu() {
        try {
            NavUtils.navigateUpFromSameTask(this);
        } catch (Exception e) {
            finish();
        }
    }


    /**
     *
     *  ON QRCODE READ LISTENER
     *
     */

    @Override
    public synchronized void onQRCodeRead(String text, PointF[] points) {

        if(cansave) {
            cansave = false;
            Log.d("test", "read text : " + text);

            String memberID = MyAccount.getInstance().getAccountID();

            MyQRItem item = new MyQRItem(memberID, text);
            item.setAlertMsg(this);

            Log.d("test", "QR Item type : " + item.gettype());

            if(item.gettype() == 7) {
                // Do nothing.
                return;
            }

            // Stop the decoder when detect expected result
            showdecoderView(false, null);

            qrcodeDbHelper.addQRCodeResponse(item, memberID);

            // Reference it
            this.item = item;

            Map<String, String> param = new HashMap<String, String>();
            param.put("screenName", MyAccount.getInstance().getAccountScreenName());
            param.put("data", item.getAlertMsg());
            FlurryAgent.logEvent("Scanned QR Code", param);

            BSAlertDialog.newInstance(item.getAlertMsg(), this).show(getFragmentManager(), "alert");
        }
    }

    @Override
    public void cameraNotFound() {
        BSAlertDialog.newInstance().easyDialog("Camera not found").show(getFragmentManager(), "error");
    }

    @Override
    public void QRCodeNotFoundOnCamImage() {
        Log.d("test", "Where is my QRCode ?");
    }

    @Override
    public void onBSDialogPosBtnClicked(DialogInterface dialog, String tag) {
        dialog.dismiss();
        // TODO: restore item (MyQRItem)
        int type = item.gettype();
        switch (type) {
            case MyQRItem.TYPE_MOBILE:
                BSDialogIntentBuilder.newInstance(this).openPhoneNumbersIntent(new String[]{item.getIntentData()});
                break;

            case MyQRItem.TYPE_WEB:
                // TODO: web intent
                BSDialogIntentBuilder.newInstance(this).openWebIntent(item.getIntentData());
                break;

            case MyQRItem.TYPE_SMS:
                // TODO: sms intent
                BSDialogIntentBuilder.newInstance(this).openSMSIntent(item.getIntentData(), item.getResponseMsg());
                break;

            default:
                break;
        }
    }
}
