package com.brainsourceapp.reedicious.rest.response;

public class Campaign {

    private String id;
    private String join_campaign;
    private String campaign_icon;
    private String campaign_url;
    private String title;
    private String cover;
    private int start;
    private int end;

    public Campaign() {

    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getEnd() {
        return end;
    }

    public void setEnd(int end) {
        this.end = end;
    }

    public int getStart() {
        return start;
    }

    public void setStart(int start) {
        this.start = start;
    }

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCampaign_url() {
        return campaign_url;
    }

    public void setCampaign_url(String campaign_url) {
        this.campaign_url = campaign_url;
    }

    public String getCampaign_icon() {
        return campaign_icon;
    }

    public void setCampaign_icon(String campaign_icon) {
        this.campaign_icon = campaign_icon;
    }

    public String getJoin_campaign() {
        return join_campaign;
    }

    public void setJoin_campaign(String join_campaign) {
        this.join_campaign = join_campaign;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    private String url;



}
