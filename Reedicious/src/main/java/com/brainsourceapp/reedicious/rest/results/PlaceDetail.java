package com.brainsourceapp.reedicious.rest.results;

import com.brainsourceapp.reedicious.rest.data.MyMinimart;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PlaceDetail {

    public Header header;

    public Data data;

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class Header {
        public int code;

        public String message;
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class Data {

        public int id;

        public String title = "";

        public String desc = "";

        public String recipes = "";

        public int category;

        @JsonProperty(value = "restaurant_type")
        public String restaurantType = "";

        @JsonProperty(value = "onairdate")
        public int onAirDate;

        public String address = "";

        public String cover = "";

        public String phone = "";

        @JsonProperty(value = "opentime")
        public String openTime = "";

        public String website = "";

        @JsonProperty(value = "reviewid")
        public int reviewID;

        @JsonProperty(value = "ytid")
        public String youtubeID = "";

        @JsonProperty(value = "yturl")
        public String youtubeURL = "";

        @JsonProperty(value = "video_desc")
        public String videoDesc = "";

        @JsonProperty(value = "latitude")
        public String lat = "";

        @JsonProperty(value = "longitude")
        public String lng = "";

        public String distance = "";

        @JsonProperty(value = "gallery")
        public ArrayList<Gallery> galleries;

        public ArrayList<Gallery> getGalleryList() {
            ArrayList<Gallery> list = new ArrayList<Gallery>();
            for(Gallery item : galleries) {
                list.add( item );
            }
            return list;
        }

        // facebookURL
        @JsonProperty(value = "contact")
        public ArrayList<String> contacts = new ArrayList<String>();
        public void setContacts(ArrayList<String> contacts) {
            if(contacts == null) {
                contacts = new ArrayList<String>();
            }
            this.contacts = contacts;
        }
        public ArrayList<String> getContacts() {
            ArrayList<String> ret = new ArrayList<String>();
            ret.addAll(this.contacts);
            return ret;
        }

        // minimart
        @JsonProperty(value = "minimart")
        public List<MyMinimart> minimartList;

        @JsonAnySetter
        public void setMinimartList(@Nullable ArrayList<MyMinimart> minimartList) {
            if(minimartList == null) {
                minimartList = new ArrayList<MyMinimart>();
            }

            if(this.minimartList == null) {
                this.minimartList = new ArrayList<MyMinimart>();
            }

            if(!this.minimartList.isEmpty()) {
                this.minimartList.clear();
            }

            for(MyMinimart item : minimartList) {
                this.minimartList.add( item );
            }
        }

        @JsonAnySetter
        public List<MyMinimart> getMinimartList() {
            List<MyMinimart> list = new ArrayList<MyMinimart>();
            for(MyMinimart item : this.minimartList) {
                list.add( item );
            }
            return list;
        }

        // tag_ig
        @JsonProperty(value = "tag_ig")
        public String tagIG = "";
        public String getsmartTagIG() {
            if(tagIG == null || tagIG.trim().length() == 0) {
                return "#";
            }
            if( tagIG.trim().length() > 0 && !tagIG.startsWith("#") ) {
                tagIG = "#" + tagIG;
            }
            return tagIG;
        }

        // tag_tw
    }

}
