package com.brainsourceapp.reedicious.rest.results;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Commentator {

    public int comment_id;

    public int site_id;

    public int entry_id;

    public int weblog_id;

    public int author_id;

    public String status;

    public String name;

    public String email;

    public String url;

    public String location;

    public String ip_address;

    public int comment_date;

    public String edit_date;

    public String comment;

    public String notify;

    public String avatar;

}
