package com.brainsourceapp.reedicious.utils;

import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.widget.Toast;

import java.util.Locale;

public class BSDialogIntentBuilder {

    private Context context;

    private BSDialogIntentBuilder(Context context) {
        this.context = context;
    }

    public static BSDialogIntentBuilder newInstance(Context context) {
        return new BSDialogIntentBuilder(context);
    }

    public void openPhoneNumbersIntent(final CharSequence[] phoneNumbers) {
        Log.d("test", "phones count : " + phoneNumbers.length);

        AlertDialog.Builder builder = new AlertDialog.Builder(this.context);
        builder.setTitle("Select one :");
        builder.setSingleChoiceItems(phoneNumbers, -1, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                String number = (String)phoneNumbers[which];
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + number));
                callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(callIntent);
            }
        });
        builder.setNegativeButton(com.brainsourceapp.reedicious.R.string.btn_cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public void openWebIntent(String url) {
        if(!url.startsWith("http://") && !url.startsWith("https://")) {
            url = "http://" + url;
        }

        // TODO: show embed webview with url
        Log.d("test", "url : " + url);
        Intent web = new Intent(Intent.ACTION_VIEW);
        web.setData(Uri.parse(url));
        web.putExtra("actionbarTitle", "");
        web.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(web);
    }

    public void openSMSIntent(String targetNumber, String msg) {
        Intent sendIntent = new Intent(Intent.ACTION_VIEW);
        sendIntent.putExtra("address", targetNumber);
        sendIntent.putExtra("sms_body", msg);
        sendIntent.setType("vnd.android-dir/mms-sms");
        context.startActivity(sendIntent);
    }

    public void openGoogleMapIntent(double sourceLatitude, double sourceLongitude, double destinationLatitude, double destinationLongitude, String title) {
        String uri = String.format(Locale.ENGLISH, "http://maps.google.com/maps?saddr=%f,%f(%s)&daddr=%f,%f (%s)", sourceLatitude, sourceLongitude, "CurrentLocation", destinationLatitude, destinationLongitude, title);
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
        intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
        try
        {
            context.startActivity(intent);
        }
        catch(ActivityNotFoundException ex)
        {
            try
            {
                Intent unrestrictedIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                context.startActivity(unrestrictedIntent);
            }
            catch(ActivityNotFoundException innerEx)
            {
                Context mContext = context.getApplicationContext();
                assert mContext != null;
                Toast.makeText(mContext, "Please install a maps application", Toast.LENGTH_LONG).show();
            }
        }
    }

}
