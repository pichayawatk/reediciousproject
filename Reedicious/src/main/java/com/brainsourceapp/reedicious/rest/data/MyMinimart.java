package com.brainsourceapp.reedicious.rest.data;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import com.fasterxml.jackson.annotation.JsonProperty;

import org.jetbrains.annotations.Nullable;

public class MyMinimart implements Parcelable {

    private String mart_id;
    private String mart_name;
    private String mart_lat;
    @JsonProperty(value = "mart_lon")
    private String mart_lng;

    private MyMinimart(Parcel in) {
        mart_id = in.readString();
        mart_name = in.readString();
        mart_lat = in.readString();
        mart_lng = in.readString();
    }

    public MyMinimart() {

    }

    public void antiNull() {
        if(this.mart_id == null)            this.mart_id = "";
        if(this.mart_name == null)          this.mart_name = "";
        if(this.mart_lat == null)           this.mart_lat = "0";
        if(this.mart_lng == null)           this.mart_lng = "0";
    }

    public String getMart_id() {
        return mart_id;
    }

    public void setMart_id(String mart_id) {
        this.mart_id = mart_id;
    }

    public String getMart_name() {
        return mart_name;
    }

    public void setMart_name(String mart_name) {
        this.mart_name = mart_name;
    }

    public String getMart_lat() {
        return mart_lat;
    }

    public void setMart_lat(String mart_lat) {
        this.mart_lat = mart_lat;
    }

    public String getMart_lng() {
        return mart_lng;
    }

    public void setMart_lng(String mart_lng) {
        this.mart_lng = mart_lng;
    }

    // Additional Implement Methods
    public double getMartLat() {
        if(mart_lat == null || mart_lat.trim().length() == 0) {
            Log.e("test", "WARNING__ : no Minimart Latitude");
            return -1;
        }
        return Double.parseDouble( mart_lat );
    }

    public double getMartLng() {
        if(mart_lng == null || mart_lng.trim().length() == 0) {
            Log.e("test", "WARNING__ : no Minimart Longitude");
            return -1;
        }
        return Double.parseDouble( mart_lng );
    }

    /**
     *
     *  Parcelable
     *
     */
    @Override
    public int describeContents() {
        return 4;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mart_id);
        dest.writeString(mart_name);
        dest.writeString(mart_lat);
        dest.writeString(mart_lng);
    }

    public static final Parcelable.Creator<MyMinimart> CREATOR = new Creator<MyMinimart>() {
        @Nullable
        @Override
        public MyMinimart createFromParcel(Parcel source) {
            return new MyMinimart(source);
        }

        @Override
        public MyMinimart[] newArray(int size) {
            return new MyMinimart[size];
        }
    };

}
