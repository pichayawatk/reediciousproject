package com.brainsourceapp.reedicious.rest.results;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PlacePromotion {

    public Header header;

    public Property property;

    public Data data;

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class Data {

        public Data() {}

        public int id;

        public String title;

        public String desc;

        public int date;

        public String cover;

        public String relationship;

        @JsonProperty(value = "videoid")
        public String videoID;

        @JsonProperty(value = "promotion_link")
        public String promotionLink;

        public double latitude;

        public double longitude;

        public float distance;

    }

}
