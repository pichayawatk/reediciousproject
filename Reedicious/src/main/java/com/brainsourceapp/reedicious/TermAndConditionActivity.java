package com.brainsourceapp.reedicious;

import android.app.ActionBar;
import android.app.Activity;
import android.support.v4.app.NavUtils;
import android.text.Html;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.brainsourceapp.reedicious.beans.MyAccount;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.OptionsItem;
import org.androidannotations.annotations.OptionsMenu;
import org.androidannotations.annotations.ViewById;

@EActivity(R.layout.activity_term_and_condition)
@OptionsMenu(R.menu.term_and_condition)
public class TermAndConditionActivity extends Activity {

    @ViewById(R.id.tac_image)
    ImageView mImageView;

    @ViewById(R.id.tac_desc)
    TextView mTextView;

    String mTitle, mLinkedURL;

    @AfterViews
    void afterTermAndConditionViews() {

        setupActionBar();

        // Check is there any campaign ?


        // Setup TextView movement method
        mTextView.setMovementMethod(new ScrollingMovementMethod());

        mTextView.setAlpha(100f);
        mImageView.setAlpha(100f);

        // Set description
        // Remove html
        String noHtml = Html.fromHtml( MyAccount.getInstance().getCampaignDesc() ).toString();
        mTextView.setText( noHtml );

        // Set listener
        mLinkedURL = MyAccount.getInstance().getCampaignLink();
        mImageView.setOnClickListener( new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                WebViewActivity_.intent(TermAndConditionActivity.this)
                        .url(mLinkedURL)
                        .actionbarTitle( mTitle )
                        .start();
            }
        });

        // Load image
        String url = MyAccount.getInstance().getCampaignCover();

        if(url != null && !url.isEmpty()) {
            Picasso.with(this).load(url).into(mImageView, new Callback() {
                @Override
                public void onSuccess() {

                }

               @Override
                public void onError() {

                }
            });
        }
    }

    private void setupActionBar() {
        ActionBar actionBar = getActionBar();
        if(actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            mTitle = MyAccount.getInstance().getCampaignTitle();
            actionBar.setTitle( mTitle );
        }
    }

    @OptionsItem(android.R.id.home)
    void didClickedHomeMenu() {
        try {
            NavUtils.navigateUpFromSameTask(this);
        } catch (Exception e) {
            finish();
        }
    }

}
