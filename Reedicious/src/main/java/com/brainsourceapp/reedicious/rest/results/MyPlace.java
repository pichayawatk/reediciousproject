package com.brainsourceapp.reedicious.rest.results;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MyPlace implements Parcelable{

    public int id;

    public String title;

    public String desc;

    public int category;

    public int onairdate;

    public String address;

    public String cover;

    public double latitude;

    public double longitude;

    public String distance;

    public MyPlace() {

    }

    public MyPlace(Parcel in) {
        id = in.readInt();
        title = in.readString();
        desc = in.readString();
        category = in.readInt();
        onairdate = in.readInt();
        address = in.readString();
        cover = in.readString();
        latitude = in.readDouble();
        longitude = in.readDouble();
        distance = in.readString();
    }

    @Override
    public int describeContents() {
        return 10;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeString(title);
        parcel.writeString(desc);
        parcel.writeInt(category);
        parcel.writeInt(onairdate);
        parcel.writeString(address);
        parcel.writeString(cover);
        parcel.writeDouble(latitude);
        parcel.writeDouble(longitude);
        parcel.writeString(distance);
    }

    public static final Parcelable.Creator<MyPlace> CREATOR = new Creator<MyPlace>() {
        @Override
        public MyPlace createFromParcel(Parcel parcel) {
            return new MyPlace(parcel);
        }

        @Override
        public MyPlace[] newArray(int size) {
            return new MyPlace[size];
        }
    };
}
