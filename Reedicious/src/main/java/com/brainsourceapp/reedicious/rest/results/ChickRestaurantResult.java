package com.brainsourceapp.reedicious.rest.results;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ChickRestaurantResult {

    public Header header;

    public Property property;

    @JsonProperty(value = "data")
    public ArrayList<MyPlace> datas;

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class Header {

        public int code;

        public String message;
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class Property {

        public int totalrecord;

        public int totalpage;

        public int page;

        public int limit;

    }

    /**
     |-------------------------------------------
     |  Category Mapping
     |-------------------------------------------
     |
     |  0   -   all
     |  1   -   inter
     |  2   -   bakery
     |  3   -   thai
     |  4   -
     |  5   -   healthy
     |  6   -
     |  7   -   unseen
     |
     |
     */

}
