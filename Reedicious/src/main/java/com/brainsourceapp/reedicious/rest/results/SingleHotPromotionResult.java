package com.brainsourceapp.reedicious.rest.results;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SingleHotPromotionResult {

    public Header header;

    @JsonProperty(value = "data")
    public ArrayList<Data> datas;

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class Header {

        public int code;

        public String message;

    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class Data {

        public Data() {}

        @JsonProperty(value = "id")
        public String ID;

        public String title;

        public String desc;

        public int date;

        public String cover;

        public String relationship;

        @JsonProperty(value = "videoid")
        public String videoID;

        @JsonProperty(value = "promotion_link")
        public String promotionLink;

        public double latitude;

        public double longitude;

        public double distance;

    }
}
