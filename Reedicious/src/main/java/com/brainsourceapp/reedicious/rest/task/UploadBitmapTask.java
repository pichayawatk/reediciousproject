package com.brainsourceapp.reedicious.rest.task;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.util.Log;

import com.cloudinary.Cloudinary;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class UploadBitmapTask extends AsyncTask<String, String, String> {

    public interface UploadBitmapInterface {
        public void onImageUploaded(String url);
    }

    UploadBitmapInterface listener;
    Context context;
    Bitmap bitmap;
    ProgressDialog pg;
    Map<String, String> config;
    String userID;

    public UploadBitmapTask(Context context, Bitmap bitmap, String userID, UploadBitmapInterface listener) {
        this.config = new HashMap<String, String>();
        this.bitmap = bitmap;
        this.context = context;
        this.userID = userID;
        this.listener = listener;

        try {
            pg = new ProgressDialog(context);
            pg.setCanceledOnTouchOutside(false);
            pg.setMessage("LOADING");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        try {
            ((Activity) context).runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if(pg != null && !pg.isShowing()) {
                        pg.show();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected String doInBackground(String... params) {

        // Image title
        String imageTitle = "" + (int)System.currentTimeMillis();
        String imageDesc = "Reedicious_Profile_Image_" + (int)System.currentTimeMillis();

        // Create url string from bitmap
        String imageUrl = MediaStore.Images.Media.insertImage(context.getContentResolver(), bitmap, imageTitle, imageDesc);

        if(imageUrl == null || imageUrl.length() == 0) {
            return null;
        }

        // Cloudinary configuration parameters
        String cloud_name = "brainsource";
        String api_key = "987674942549526";
        String api_secret = "O2qkWJMaU6UDYmuv9T7D85Z3joE";

        config.put("cloud_name", cloud_name);
        config.put("api_key", api_key);
        config.put("api_secret", api_secret);
        Cloudinary mobileCloudinary = new Cloudinary(config);

        // Bitmap to InputStream
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bos);
        byte[] bitmapdata = bos.toByteArray();
        ByteArrayInputStream bs = new ByteArrayInputStream(bitmapdata);

        try {
            mobileCloudinary.uploader().upload(bs, Cloudinary.asMap(
                    "public_id", userID,
                    "signature", userID
            ));

            return mobileCloudinary.url().generate(imageTitle);

        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);

        if(pg != null && pg.isShowing()) {
            pg.dismiss();
            pg = null;
        }
        if(!bitmap.isRecycled()) {
            bitmap.recycle();
            bitmap = null;
        }

        if(result != null) {
            Log.d("test", "Cloudinary generated url : " + result);
            if(listener != null) {
                Log.d("test", "Upload Bitmap Completed with return generated url : ");
                listener.onImageUploaded( result );
            }
        }

    }
}
