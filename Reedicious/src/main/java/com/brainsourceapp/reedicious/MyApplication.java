package com.brainsourceapp.reedicious;

import android.app.Application;
import android.content.Context;

import com.brainsourceapp.reedicious.beans.MyAccount;
import com.brainsourceapp.reedicious.beans.MyLifecycleBean;
import com.brainsourceapp.reedicious.services.GPSTracker;

import org.androidannotations.annotations.EApplication;

@EApplication
public class MyApplication extends Application {

    private boolean promotion;
    public boolean isStartUpPromotionShowed() {
        return promotion;
    }
    public void setStartUpPromotionShowed(boolean promotion) {
        this.promotion = promotion;
    }

    private boolean apitoken;
    public boolean isApiToken() {
        return apitoken;
    }
    public void setApiToken(boolean apitoken) {
        this.apitoken = apitoken;
    }

    private GPSTracker gpsTracker;
    public GPSTracker getGpsTracker(Context context) {
        if(gpsTracker == null) {
            gpsTracker = new GPSTracker(context);
        }
        return gpsTracker;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        // Simple add the handler
        registerActivityLifecycleCallbacks(new MyLifecycleBean());

        // Default init
        promotion = false;

        // Default init
        apitoken = false;

        MyAccount.getInstance().resumeFromSharedPreferences(getApplicationContext());

        if(gpsTracker == null) {
            gpsTracker = new GPSTracker(getApplicationContext());
        }
    }
}
