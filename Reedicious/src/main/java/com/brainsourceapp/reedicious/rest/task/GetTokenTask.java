package com.brainsourceapp.reedicious.rest.task;

import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class GetTokenTask extends AsyncTask<String, String, String> {

    public interface GetTokenListener {
        public void didFinishGetToken(String token);
    }

    private GetTokenListener listener;

    public GetTokenTask(GetTokenListener listener) {
        this.listener = listener;
    }

    @Override
    protected String doInBackground(String... params) {
        String appid = "1";
        String apikey = "574936";

        String url = "http://api.reedicious.com/rest/auth/gettoken?appid=" + appid + "&apikey=" + apikey;

        HttpClient httpClient = new DefaultHttpClient();
        HttpGet get = new HttpGet(url);

        // Execute the request
        HttpResponse response;
        try {
            response = httpClient.execute(get);
            // Examine the response status
            int statusCode = response.getStatusLine().getStatusCode();
            Log.d("test", "get token result status code : " + statusCode);

            // Get the hold response entity
            HttpEntity entity = response.getEntity();

            if(entity != null) {
                InputStream inputStream = entity.getContent();
                String resultString = convertStreamToString(inputStream);
                inputStream.close();

                return getTokenFromString( resultString );
            }

        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return "";
    }

    private String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        String result = "";

        String line;
        try {
            while ((line = reader.readLine()) != null) {
                result = result + line + "\n";
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    private String getTokenFromString(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            if(jsonObject.has("data")) {
                JSONObject data = jsonObject.getJSONObject("data");
                if(data.has("token")) {
                    return data.getString("token");
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(String token) {
        super.onPostExecute(token);

        listener.didFinishGetToken(token);
    }
}
