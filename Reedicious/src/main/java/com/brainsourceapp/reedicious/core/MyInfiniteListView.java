package com.brainsourceapp.reedicious.core;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ListView;

public abstract class MyInfiniteListView extends ListView {

    private static final int MAX_Y_OVERSCROLL_DISTANCE = 200;

    int mMaxOverScrollDistance;

    public MyInfiniteListView(Context context) {
        super(context);
        instantiate(context);
    }

    public MyInfiniteListView(Context context, AttributeSet attrs) {
        super(context, attrs);
        instantiate(context);
    }

    public MyInfiniteListView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        instantiate(context);
    }

    private void instantiate(Context context) {

    }
}
