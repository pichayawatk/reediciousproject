package com.brainsourceapp.reedicious.rest.results;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CampaignResult {

    public Header header;

    public Property property;

    public ArrayList<Data> data;


    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class Header {
        public int code;
        public String message;
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class Property {
        public int totalrecord;
        public int totalpage;
        public int page;
        public int limit;
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class Data {

        public int id;

        @JsonProperty(value = "join_campaign")
        public String joinCampaign;

        @JsonProperty(value = "campaign_icon")
        public String campaignIconURL;

        @JsonProperty(value = "campaign_url")
        public String campaignURL;

        public String title;

        public String cover;

        public int start;

        public int end;

        @JsonProperty(value = "url")
        public String url;
    }

}
