package com.brainsourceapp.reedicious;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.NavUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.LruCache;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.Volley;
import com.brainsourceapp.reedicious.beans.MyAccount;
import com.brainsourceapp.reedicious.constants.BSTag;
import com.brainsourceapp.reedicious.rest.task.UpdateUserInfoTask;
import com.brainsourceapp.reedicious.utils.BSAlertDialog;
import com.brainsourceapp.reedicious.utils.BSProgressHUD;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.InstanceState;
import org.androidannotations.annotations.OptionsItem;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import java.io.InputStream;

@EActivity(R.layout.activity_update_profile)
public class UpdateProfileActivity extends Activity {

    String tempEmail;
    String tempScreenName;
    String profileImageURL;

    @ViewById(R.id.update_profile_avatar)
    NetworkImageView profileImage;

    Bitmap profileImageBitmap;
    Uri contentUri;

    @InstanceState
    boolean isNewProfileImageBitmap;

    ImageLoader mImageLoader;
    RequestQueue mRequestQueue;

    /** ------------------------------------------------------------------------
     *      Profile Image Preparing Methods
     *  ------------------------------------------------------------------------
     *
     */

    @Click(R.id.update_profile_avatar)
    public void onProfileImageClicked() {

        // TODO: show image picker
        Intent photoIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(photoIntent, BSTag.Request.PhotoSelector);
    }

    /**
     *  Crop the image
     *
     */
    public void performCropImage(Uri contentUri) {

        // Start crop intent image
        Intent intent = new Intent("com.android.camera.action.CROP");
        intent.setDataAndType(contentUri, "image/*");

        // Get screen dimension
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);

        intent.putExtra("scale", true);

        // Image ratio
        intent.putExtra("aspectX", 1);
        intent.putExtra("aspectY", 1);
        intent.putExtra("return-data", true);

        try {
            startActivityForResult(intent, BSTag.Request.ImageCropper);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(this, "Your devices does not support crop image.", Toast.LENGTH_LONG).show();

            // Create bitmap from Uri
            onCropImageFailed(contentUri);
        }
    }

    @UiThread
    public void onCropImageFailed(Uri contentUri) {

        // Create bitmap from Uri
        try {
            InputStream inputStream = getContentResolver().openInputStream( contentUri );
            Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
            profileImage.setImageBitmap( bitmap );

            // reference the contentUri for elimination after registrar complete.
            this.contentUri = contentUri;
        } catch (Exception e) {
            e.printStackTrace();
            profileImage.setImageResource(R.drawable.reedicious_avatar);
        }
    }

    /**
     * On Activity Result
     *
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        /**
         * Aftet Image Picker
         */
        if(requestCode == BSTag.Request.PhotoSelector) {
            if(resultCode == RESULT_OK) {
                Uri contentUri = data.getData();
                if(contentUri != null) {
                    Log.d("test", "Return uri (content uri) : " + contentUri.toString());
                    performCropImage( contentUri );
                }
            }
        }

        /**
         * After Crop Image
         */
        if(requestCode == BSTag.Request.ImageCropper) {
            try {
                Bundle extras = data.getExtras();
                assert extras != null;

                profileImageBitmap = extras.getParcelable("data");

                if(profileImageBitmap != null) {
                    // Save temporary bitmap (should update it after registrar success)
                    profileImage.setImageBitmap( profileImageBitmap );

                } else {
                    profileImage.setImageResource(R.drawable.reedicious_avatar);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public boolean deleteProfileImageCacheAfterCompletedRegistrar() {

        try {
            getContentResolver().delete(this.contentUri, null, null);

            // Run the MediaScanner service run again.
            sendBroadcast(new Intent(Intent.ACTION_MEDIA_MOUNTED, Uri.parse("file://" + Environment.getExternalStorageDirectory())));

            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /** ------------------------------------------------------------ */

    @Click(R.id.update_profile_okbtn)
    public void updateProfile() {
        assert screenNameEditText.getText() != null && emailEditText.getText() != null;
        String screenName = screenNameEditText.getText().toString();
        String email = emailEditText.getText().toString();

        if(screenName == null || screenName.trim().length() == 0) {
            String errorMsg = getResources().getString(R.string.error_code_506);
            BSAlertDialog.newInstance(errorMsg, new BSAlertDialog.CallbackDialogListener() {
                @Override
                public void onBSDialogPosBtnClicked(DialogInterface dialog, String tag) {
                    dialog.dismiss();
                }
            }).show(getFragmentManager(), "no screenname");
            return;
        }

        if(email == null || email.trim().length() == 0) {
            String errorMsg = getResources().getString(R.string.error_code_507);
            BSAlertDialog.newInstance(errorMsg, new BSAlertDialog.CallbackDialogListener() {
                @Override
                public void onBSDialogPosBtnClicked(DialogInterface dialog, String tag) {
                    dialog.dismiss();
                }
            }).show(getFragmentManager(), "no email");
            return;
        }

        if(!email.contains("@")) {
            String errorMsg = getResources().getString(R.string.error_code_508);
            BSAlertDialog.newInstance(errorMsg, new BSAlertDialog.CallbackDialogListener() {
                @Override
                public void onBSDialogPosBtnClicked(DialogInterface dialog, String tag) {
                    dialog.dismiss();
                }
            }).show(getFragmentManager(), "wrong email");
            return;
        }

        tempScreenName = screenName;
        tempEmail = email;

        String apiToken = MyAccount.getInstance().getApiToken();
        String userToken = MyAccount.getInstance().getAccountToken();
        String memberID = MyAccount.getInstance().getAccountID();
        String imageURL = MyAccount.getInstance().getAccountAvatar();

        String fbId = MyAccount.getInstance().getFacebookID();
        String fbToken = MyAccount.getInstance().getFacebookToken();
        String fbMappedStatus = MyAccount.getInstance().isFacebookStatusMapped() ? "1" : "0";

        if(profileImageBitmap != null) {
            Log.d("test", "Upload bitmap together with update profile.");
        }

        UpdateUserInfoTask task = new UpdateUserInfoTask(this,
                apiToken,
                userToken,
                memberID,
                email,
                null,
                screenName,
                fbId,
                fbToken,
                fbMappedStatus,
                imageURL,
                profileImageBitmap,
                new UpdateUserInfoTask.UpdateUserCallback() {
                    @Override
                    public void onFinishedUpdateUserInfoTask(boolean success, String imageURL) {

                        onUpdateUserResult(success, imageURL );
                    }
                });
        BSProgressHUD.showProgressHUD(this, "UPDATING");
        task.execute();
    }

    @ViewById(R.id.update_profile_screenname)
    EditText screenNameEditText;

    @ViewById(R.id.update_profile_email)
    EditText emailEditText;

    @Click(R.id.update_profile_cancelbtn)
    public void cancelUpdateProfile() {
        setResult(RESULT_CANCELED);
        finish();
    }

    @UiThread
    public void onUpdateUserResult(boolean success, String imageURL) {
        BSProgressHUD.dismissProgressHUD();

        Context appContext = this.getApplicationContext();
        assert appContext != null;
        if(success) {
            Toast.makeText(appContext, "สำเร็จ", Toast.LENGTH_SHORT).show();

            // ScreenName, Email, ImageURL should be updated from child's activity
            Intent intent = new Intent();
            intent.putExtra("imageURL", imageURL);
            setResult((RESULT_OK), intent);
        } else {
            Toast.makeText(appContext, "ล้มเหลว", Toast.LENGTH_SHORT).show();
            setResult(RESULT_CANCELED);
        }

        this.onBackPressed();
    }

    @AfterViews
    public void initUpdateProfileViews() {
        // Do nothing.
    }

    @OptionsItem(android.R.id.home)
    void didClickedHomeMenu() {
        try {
            deleteProfileImageCacheAfterCompletedRegistrar();
            NavUtils.navigateUpFromSameTask(this);
        } catch (Exception e) {
            finish();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(savedInstanceState != null) {

            if(savedInstanceState.containsKey("profileImage")) {
                profileImageBitmap = savedInstanceState.getParcelable("profileImage");
                profileImage.setImageBitmap( profileImageBitmap );
            }

            if(savedInstanceState.containsKey("isNewProfileImageBitmap")) {
                isNewProfileImageBitmap = savedInstanceState.getBoolean("isNewProfileImageBitmap", false);
            }
        }
    }

    @AfterViews
    public void afterUpdateInfoViews() {

        screenNameEditText.setText( MyAccount.getInstance().getAccountScreenName() );

        emailEditText.setText( MyAccount.getInstance().getAccountEmail() );
        emailEditText.setEnabled( false );
        emailEditText.setFocusable( false );

        // Init volley
        mRequestQueue = Volley.newRequestQueue(this);
        mImageLoader = new ImageLoader(mRequestQueue, new ImageLoader.ImageCache() {
            private final LruCache<String, Bitmap> mCache = new LruCache<String, Bitmap>(10);
            @Override
            public Bitmap getBitmap(String s) {
                return mCache.get(s);
            }

            @Override
            public void putBitmap(String s, Bitmap bitmap) {
                mCache.put(s, bitmap);
            }
        });

        // User Avatar
        String imageURL = MyAccount.getInstance().getAccountAvatar();
        profileImage.setImageUrl(imageURL, mImageLoader);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        /**
         *
         *  Update profile image save instance state
         *
         */
        outState.putParcelable("profileImage", profileImageBitmap);
    }
}
