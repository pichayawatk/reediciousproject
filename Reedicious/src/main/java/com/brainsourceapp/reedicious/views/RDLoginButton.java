package com.brainsourceapp.reedicious.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

import com.brainsourceapp.reedicious.R;
import com.facebook.widget.LoginButton;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

@EViewGroup(R.layout.rd_login_button_view)
public class RDLoginButton extends RelativeLayout {

    public RDLoginButton(Context context) {
        super(context);
    }

    public RDLoginButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public RDLoginButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @ViewById(R.id.rd_fb_core_login_btn)
    LoginButton fbBtn;

    @Click(R.id.rd_fb_surface_login_btn)
    void onSufaceBtnClicked() {
        fbBtn.performClick();
    }
}
