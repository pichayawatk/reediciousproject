package com.brainsourceapp.reedicious.utils;

import com.brainsourceapp.reedicious.R;

public class RDMapHelper {

    public static int getPinResID(int category) {
        int respinID = R.drawable.pin_thai;

        // Pin icon classification by categoryID
        if(category == 1) {
            // inter
            respinID = R.drawable.pin_inter;
        }
        else if(category == 2) {
            // bakery
            respinID = R.drawable.pin_bakery;
        }
        else if(category == 3) {
            // thai
            respinID = R.drawable.pin_thai;
        }
        else if(category == 5) {
            // healthy
            respinID = R.drawable.pin_healthy;
        }
        else if(category == 7) {
            // unseen
            respinID = R.drawable.pin_unseen;
        }

        return respinID;
    }
}
