package com.brainsourceapp.reedicious.beans;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;

import com.brainsourceapp.reedicious.rest.response.Campaign;
import com.brainsourceapp.reedicious.rest.utils.BSHttpUtils;
import com.flurry.android.FlurryAgent;
import com.google.android.gms.maps.model.LatLng;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MyAccount {

    private static final String TAG = MyAccount.class.getSimpleName();
    private static final String DEFAULT_AVATAR_URL = "http://res.cloudinary.com/brainsource/image/upload/v1391495815/reedicious_avatar_android_720_njhivw.png";
    private static final String FB_PROFILE_BY_ID_HOLDER = "http://graph.facebook.com/$(facebook_id)/picture?type=large";

    /**
     * Interface
     */
    public interface MyAccountLoginListener {
        void didLoginWithAccount(boolean success, String errorMsg);
    }
    public interface MyAccountLoginWithFacebookListener {
        void didLoginWithFacebook(boolean success);
    }
    public interface MyAccountFetchInfoListener {
        void didFinishFetchInfo(boolean success);
    }
    public interface MyAccountRegisterWithFacebookListener {
        void didFinishRegisterWithFacebook(boolean success);
    }
    public interface MyAccountRegisterListener {
        void didFinishRegister(boolean success);
    }
    public interface MyAccountUpdateUserInfoListener {
        void didFinishUpdateUserInfo(boolean success);
    }

    /** ----------------------------------------------------------------
     *
     * Implement Methods
     *
     * ----------------------------------------------------------------
     *
     *
     */
    public void login(String username, String password, MyAccountLoginListener listener) {
        this.mLoginListener = listener;
        new LoginTask().execute(username, password);
    }
    public void loginWithFacebook(String facebookID, String facebookToken, MyAccountLoginWithFacebookListener listener) {
        this.mLoginWithFacebookListener = listener;
        new LoginWithFacebookTask().execute(facebookID, facebookToken);
    }
    public void fetchInfo(MyAccountFetchInfoListener listener) {
        this.mFetchInfoListener = listener;
        new FetchInfoTask().execute();
    }
    public void registerWithFacebook(
            String accountEmail,
            String accountScreenName,
            String facebookID,
            String facebookToken,
            MyAccountRegisterWithFacebookListener listener) {
        this.mRegisterWithFacebookListener = listener;

        this.accountEmail = accountEmail;
        this.accountScreenName = accountScreenName;
        this.facebookID = facebookID;
        this.facebookToken = facebookToken;
        this.facebookEmail = accountEmail;
        this.accountAvatar = FB_PROFILE_BY_ID_HOLDER.replace("$(facebook_id)", facebookID);
        Log.d("test", "facebook init profile picture : " + accountAvatar);
        this.facebookMappedStatus = 1;

        new RegisterWithFacebookTask().execute();
    }
    public void register(String accountEmail, String accountPassword, String accountScreenName, MyAccountRegisterListener listener) {
        this.mRegisterListener = listener;
        this.accountEmail = accountEmail;
        this.accountPassword = accountPassword;
        this.accountScreenName = accountScreenName;
        this.accountAvatar = DEFAULT_AVATAR_URL;

        new RegisterTask().execute();
    }

    public void resumeFromSharedPreferences(Context context) {
        assert context.getApplicationContext() != null;
        SharedPreferences pref = context.getApplicationContext().getSharedPreferences(TAG, 0);

        isLoggedIn = pref.getBoolean("isLoggedIn", false);

        accountToken = pref.getString("accountToken", "");
        accountID = pref.getString("accountID", "");
        accountPassword = pref.getString("accountPassword", "");
        accountScreenName = pref.getString("accountScreenName", "");
        accountEmail = pref.getString("accountEmail", "");
        accountAvatar = pref.getString("accountAvatar", "");

        facebookID = pref.getString("facebookID", "");
        facebookToken = pref.getString("facebookToken", "");
        //pref.getString("facebookRetoken", "");
        //pref.getString("facebookScreenName", "");
        facebookEmail = pref.getString("facebookEmail", "");
        facebookMappedStatus = pref.getInt("facebookMappedStatus", 0);

        lat = pref.getFloat("lat", 0);
        lng = pref.getFloat("lng", 0);
        isGooglePlayServicesAvailable = pref.getBoolean("isGooglePlayServicesAvailable", false);
        canGetGoogleLocation = pref.getBoolean("canGetGoogleLocation", false);

        campaignCover = pref.getString("campaignCover", "");
        campaignDesc = pref.getString("campaignDesc", "");
        campaignLink = pref.getString("campaignLink", "");
        campaignTitle = pref.getString("campaignTitle", "");
        campaignID = pref.getString("campaignID", "");
    }

    // Shittttt !!!
    public void mapFacebookStatus(String facebookID, String facebookToken, String facebookEmail) {
        List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
        params.add(new BasicNameValuePair("fb_id", facebookID));
        params.add(new BasicNameValuePair("fb_token", facebookToken));
        params.add(new BasicNameValuePair("fb_status", "1"));
        this.facebookEmail = facebookEmail;

        new UpdateInfoTask(params).execute();
        this.facebookID = facebookID;
        this.facebookToken = facebookToken;
        this.facebookMappedStatus = 1;

        Map<String, String> param = new HashMap<String, String>();
        param.put("memberID", accountID);
        param.put("fbID", facebookID);
        FlurryAgent.logEvent("Map Facebook Account", param);
    }
    public void unmapFacebookStatus() {
        List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
        params.add(new BasicNameValuePair("fb_status", "0"));
        new UpdateInfoTask(params).execute();
        this.facebookMappedStatus = 0;
        this.facebookEmail = "";
        this.facebookToken = "";
        this.facebookID = "";

        Map<String, String> param = new HashMap<String, String>();
        param.put("memberID", accountID);
        param.put("fbID", facebookID);
        FlurryAgent.logEvent("Unmapped Facebook Account", param);
    }

    public void clearAccountData() {

        this.isLoggedIn = false;

        this.accountToken = "";
        this.accountID = "";
        this.accountEmail = "";
        this.accountScreenName = "";
        this.accountPassword = "";
        this.accountAvatar = "";

        this.facebookID = "";
        this.facebookToken = "";
        //this.facebookScreenName = "";
        this.facebookEmail = "";
        this.facebookMappedStatus = 0;
        //this.facebookAvatarURL = "";

        Map<String, String> param = new HashMap<String, String>();
        param.put("memberID", accountID);
        FlurryAgent.logEvent("Logout", param);
    }

    public void saveInSharedPreferences(Context context) {
        assert context.getApplicationContext() != null;
        SharedPreferences pref = context.getApplicationContext().getSharedPreferences(TAG, 0);
        SharedPreferences.Editor editor = pref.edit();

        editor.putBoolean("isLoggedIn", isLoggedIn);

        editor.putString("accountToken", accountToken);
        editor.putString("accountID", accountID);
        editor.putString("accountPassword", accountPassword);
        editor.putString("accountScreenName", accountScreenName);
        editor.putString("accountEmail", accountEmail);
        editor.putString("accountAvatar", accountAvatar);

        editor.putString("facebookID", facebookID);
        editor.putString("facebookToken", facebookToken);
        //editor.putString("facebookRetoken", facebookRetoken);
        //editor.putString("facebookScreenName", facebookScreenName);
        editor.putString("facebookEmail", facebookEmail);
        editor.putInt("facebookMappedStatus", facebookMappedStatus);

        editor.putFloat("lat", (float)lat);
        editor.putFloat("lng", (float)lng);
        editor.putBoolean("isGooglePlayServicesAvailable", isGooglePlayServicesAvailable);
        editor.putBoolean("canGetGoogleLocation", canGetGoogleLocation);

        editor.putString("campaignCover", campaignCover);
        editor.putString("campaignDesc", campaignDesc);
        editor.putString("campaignLink", campaignLink);
        editor.putString("campaignTitle", campaignTitle);
        editor.putString("campaignID", campaignID);

        editor.commit();
    }

    /**
     *  --------------------------------------------------------------
     *
     *  Class Properties
     *
     *  --------------------------------------------------------------
     *
     */

    MyAccountLoginListener mLoginListener;
    MyAccountLoginWithFacebookListener mLoginWithFacebookListener;
    MyAccountRegisterWithFacebookListener mRegisterWithFacebookListener;
    MyAccountFetchInfoListener mFetchInfoListener;
    MyAccountRegisterListener mRegisterListener;

    private int currentErrorCode;
    public int getCurrentErrorCode() {
        return currentErrorCode;
    }
    private String currentResponseMsg;
    public String getCurrentResponseMsg() {
        return currentResponseMsg;
    }

    private String apiToken;
    private Boolean isLoggedIn;
    public boolean isLoggedIn() {
        return isLoggedIn;
    }

    private String accountToken;
    private String accountID;
    private String accountLoginName;
    private String accountPassword;
    private String accountScreenName;
    private String accountEmail;
    private String accountAvatar;

    private String facebookID;
    private String facebookEmail;
    //private String facebookScreenName;
    //private String facebookAvatarURL;
    private String facebookToken;
    //private String facebookRetoken;
    private Integer facebookMappedStatus;

    private static MyAccount instance;

    public static MyAccount getInstance() {
        if(instance == null) {
            instance = new MyAccount();
        }
        return instance;
    }

    private MyAccount() {
        this.apiToken = "";
        this.isLoggedIn = false;

        this.accountID = "";
        this.accountLoginName = "";
        this.accountPassword = "";
        this.accountScreenName = "";
        this.accountEmail = "";
        this.accountAvatar = "";

        this.facebookID = "";
        this.facebookEmail = "";
        //this.facebookScreenName = "";
        //this.facebookAvatarURL = "";
        this.facebookToken = "";
        //this.facebookRetoken = "";
        this.facebookMappedStatus = 0;
    }

        /**
         *
         *  FBAccount Getter & Setter
         *
         */

        public String getFacebookID() {
            return facebookID;
        }

        public void setFacebookID(String facebookID) {
            this.facebookID = facebookID;
        }

        public String getFacebookEmail() {
            return facebookEmail;
        }

        public void setFacebookEmail(String facebookEmail) {
            this.facebookEmail = facebookEmail;
        }

    /*
        public String getFacebookScreenName() {
            return facebookScreenName;
        }

        public void setFacebookScreenName(String facebookScreenName) {
            this.facebookScreenName = facebookScreenName;
        }

        public String getFacebookAvatarURL() {
            return facebookAvatarURL;
        }

        public void setFacebookAvatarURL(String facebookAvatarURL) {
            this.facebookAvatarURL = facebookAvatarURL;
        }
        */

        public String getFacebookToken() {
            return facebookToken;
        }

        public void setFacebookToken(String facebookToken) {
            this.facebookToken = facebookToken;
        }

        public boolean isFacebookStatusMapped() {
            return facebookMappedStatus != 0;
        }

        public void setFacebookMappedStatus(boolean mapped) {
            if(mapped) {
                this.facebookMappedStatus = 1;
            } else {
                this.facebookMappedStatus = 0;
            }
        }

    /**
     *
     * Local Getter & Setter
     *
     */
    public String getApiToken() {
        return apiToken;
    }
    public void setApiToken(String apiToken) {
        this.apiToken = apiToken;
    }

    /**
     *
     * Account Getter & Setter
     *
     */
    public String getAccountToken() {
        return accountToken;
    }

    public void setAccountToken(String accountToken) {
        this.accountToken = accountToken;
    }

    public String getAccountID() {
        return accountID;
    }

    public void setAccountID(String accountID) {
        this.accountID = accountID;
    }

    public String getAccountLoginName() {
        return accountLoginName;
    }

    public void setAccountLoginName(String accountLoginName) {
        this.accountLoginName = accountLoginName;
    }

    public String getAccountPassword() {
        return accountPassword;
    }

    public void setAccountPassword(String accountPassword) {
        this.accountPassword = accountPassword;
    }

    public String getAccountScreenName() {
        return accountScreenName;
    }

    public void setAccountScreenName(String accountScreenName) {
        this.accountScreenName = accountScreenName;
    }

    public String getAccountEmail() {
        return accountEmail;
    }

    public void setAccountEmail(String accountEmail) {
        this.accountEmail = accountEmail;
    }

    public String getAccountAvatar() {
        return accountAvatar;
    }

    public void setAccountAvatar(String accountAvatar) {
        this.accountAvatar = accountAvatar;
    }


    /**
     * ------------------------------------------------------------------
     *
     * Nested AsyncTask
     *
     * ------------------------------------------------------------------
     *
     *
     */

    /**
     *  Global Response Header
     *
     */
    protected int httpStatusCode;
    protected int code;
    protected String message;

    private class LoginTask extends AsyncTask<String, Void, Boolean> {

        @Override
        protected Boolean doInBackground(String... params) {
            if(params == null || params.length != 2) {
                return false;
            }

            // For clarify.
            httpStatusCode = 0;
            code = 0;
            message = "";

            String username = params[0];
            String password = params[1];

            String baseURL = "http://api.reedicious.com/rest/auth/memberlogin";
            String url = BSHttpUtils.newInstance().getfullURL(baseURL, new String[] {
                    "appid", "1",
                    "token", apiToken
            });

            HttpClient client = new DefaultHttpClient();
            HttpGet httpGet = new HttpGet(url);

            httpGet.setHeader("username", username);
            httpGet.setHeader("password", password);

            try {
                HttpResponse httpResponse = client.execute(httpGet);

                httpStatusCode = httpResponse.getStatusLine().getStatusCode();

                if(httpStatusCode == 200) {

                    HttpEntity entity = httpResponse.getEntity();
                    String responseString = BSHttpUtils.newInstance().streamToString( entity.getContent() );

                    Log.d("test", "account login response : " + responseString);

                    return mapResponse( responseString );
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            return false;
        }

        protected boolean mapResponse(String response) {
            try {
                JSONObject jsonObject = new JSONObject(response);

                if(!jsonObject.isNull("header")) {
                    JSONObject header = jsonObject.getJSONObject("header");
                    if(!header.isNull("code"))          code = header.getInt("code");
                    if(!header.isNull("message"))       message = header.getString("message");
                }

                if(!jsonObject.isNull("data")) {
                    JSONObject data = jsonObject.getJSONArray("data").getJSONObject(0);

                    accountID = data.getString("member_id");
                    accountScreenName = data.getString("screen_name");
                    accountEmail = data.getString("email");
                    accountToken = data.getString("accesstoken");

                    /** Have no avatar parameter from backend, Feb 4, 2014 - 10:33 */
                    if(!data.isNull("avatar")) {
                        accountAvatar = data.getString("avatar");
                    } else {
                        accountAvatar = DEFAULT_AVATAR_URL;
                    }

                    return true;
                }

            } catch (JSONException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return false;
        }

        @Override
        protected void onPostExecute(Boolean success) {
            super.onPostExecute(success);
            isLoggedIn = success;

            try {

                if(!success) {
                    clearAccountData();
                } else {

                    Map<String, String> param = new HashMap<String, String>();
                    param.put("email", accountEmail);
                    FlurryAgent.logEvent("Login By Facebook Account", param);
                }

                Log.d("test", "finish login with account" +
                        "\ntoken : " + accountToken +
                        "\nmember_id : " + accountID +
                        "\nscreen_name : " + accountScreenName +
                        "\nemail : " + accountEmail);

                // Get Friendly Message
                String errorMsg = "";
                if(!success) {

                    /** Wrong password but server return "Error : 204, No Content" */
                    errorMsg = message;
                }

                mLoginListener.didLoginWithAccount(success, errorMsg);
            } catch (ClassCastException e) {
                throw new ClassCastException(TAG + ": must implement LoginInterface.");
            } catch (Exception e) {
                e.printStackTrace();
                isLoggedIn = false;
            }
        }
    }

    private class LoginWithFacebookTask extends AsyncTask<String, Void, Boolean> {

        private String fbId;
        private String fbToken;

        @Override
        protected Boolean doInBackground(String... params) {
            if(params == null || params.length != 2) {
                return false;
            }

            String fbid = params[0];
            String fbtoken = params[1];

            String baseURL = "http://api.reedicious.com/rest/auth/memberlogin";
            String url = BSHttpUtils.newInstance().getfullURL(baseURL, new String[] {
                    "appid", "1",
                    "token", apiToken,
                    "social", "FB"
            });

            HttpClient client = new DefaultHttpClient();
            HttpGet httpGet = new HttpGet(url);

            httpGet.setHeader("username", fbid);
            httpGet.setHeader("password", fbtoken);

            try {
                HttpResponse httpResponse = client.execute(httpGet);

                int statusCode = httpResponse.getStatusLine().getStatusCode();

                if(statusCode == 200) {

                    HttpEntity entity = httpResponse.getEntity();
                    String responseString = BSHttpUtils.newInstance().streamToString( entity.getContent() );

                    fbId = fbid;
                    fbToken = fbtoken;

                    Log.d("test", "account login response : " + responseString);

                    return mapResponse( responseString );
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            return false;
        }

        protected boolean mapResponse(String response) {
            try {
                JSONObject jsonObject = new JSONObject(response);

                if(!jsonObject.isNull("data")) {
                    JSONObject data = jsonObject.getJSONArray("data").getJSONObject(0);

                    accountID = data.getString("member_id");
                    accountScreenName = data.getString("screen_name");
                    accountEmail = data.getString("email");
                    accountToken = data.getString("accesstoken");

                    /** Have no avatar parameter from backend, Feb 4, 2014 - 10:33 */
                    if(!data.isNull("avatar")) {
                        accountAvatar = data.getString("avatar");
                    } else {
                        String[] urlHolder = FB_PROFILE_BY_ID_HOLDER.split("[\\$\\(facebook_id\\)]");
                        accountAvatar = urlHolder[0] + fbId + urlHolder[1];
                        facebookID = fbId;
                        facebookToken = fbToken;
                    }

                    return true;
                }

            } catch (JSONException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return false;
        }

        @Override
        protected void onPostExecute(Boolean success) {
            super.onPostExecute(success);

            try {
                isLoggedIn = success;
                if(!success) {
                    clearAccountData();
                } else {

                    Map<String, String> param = new HashMap<String, String>();
                    param.put("fbID", facebookID);
                    param.put("screenName", accountScreenName);
                    FlurryAgent.logEvent("Login By Master Account", param);
                }

                mLoginWithFacebookListener.didLoginWithFacebook(success);
            } catch (ClassCastException e) {
                throw new ClassCastException(TAG + ": must implement LoginWithFacebookInterface.");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private class RegisterWithFacebookTask extends AsyncTask<String, Void, Boolean> {

        @Override
        protected Boolean doInBackground(String... params) {
            String url = BSHttpUtils.newInstance().getfullURL("http://api.reedicious.com/rest/member/register", new String[] {
                    "appid", "1",
                    "token", apiToken,
                    "social", "FB"
            });

            HttpClient client = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(url);

            List<NameValuePair> postParams = BSHttpUtils.newInstance().initPostParams(
                    "email", accountEmail,
                    "password", facebookToken,
                    "screenname", accountScreenName,
                    "fb_id", facebookID,
                    "fb_token", facebookToken,
                    "fb_status", "1",
                    "avatar", accountAvatar
            );

            try {
                httpPost.setEntity(new UrlEncodedFormEntity(postParams, "UTF-8"));

                HttpResponse response = client.execute(httpPost);

                int statusCode = response.getStatusLine().getStatusCode();

                if(statusCode == 200) {

                    HttpEntity entity = response.getEntity();
                    String responseString = BSHttpUtils.newInstance().streamToString( entity.getContent() );

                    return mapResponse( responseString );
                }
            } catch (IOException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }

            return false;
        }

        private boolean mapResponse(String res) {

            try {
                JSONObject jsonObject = new JSONObject(res);

                if(!jsonObject.isNull("data")) {
                    JSONObject data = jsonObject.getJSONObject("data");
                    if(!data.isNull("accesstoken")) {
                        accountToken = data.getString("accesstoken");
                        return true;
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return false;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);

            if(!aBoolean) {
                clearAccountData();
            } else {

                Map<String, String> param = new HashMap<String, String>();
                param.put("fbID", facebookID);
                param.put("screenName", accountScreenName);
                param.put("email", accountEmail);
                FlurryAgent.logEvent("New Register By Facebook", param);
            }

            try {
                mRegisterWithFacebookListener.didFinishRegisterWithFacebook(aBoolean);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private class FetchInfoTask extends AsyncTask<Void, Void, Boolean> {

        boolean isAuthen = true;

        @Override
        protected Boolean doInBackground(Void... non) {

            String baseURL = "http://api.reedicious.com/rest/member/getprofile";
            String url = BSHttpUtils.newInstance().getfullURL(baseURL, new String[] {
                    "appid", "1",
                    "token", apiToken
            });

            HttpClient client = new DefaultHttpClient();
            HttpGet httpGet = new HttpGet(url);

            if(accountToken == null || accountToken.length() == 0) {
                this.isAuthen = false;
                return false;
            }

            httpGet.setHeader("accesstoken", accountToken);

            try {
                HttpResponse httpResponse = client.execute(httpGet);

                int statusCode = httpResponse.getStatusLine().getStatusCode();

                if(statusCode == 200) {

                    HttpEntity entity = httpResponse.getEntity();
                    String responseString = BSHttpUtils.newInstance().streamToString( entity.getContent() );

                    Log.d("test", "fetch info response : " + responseString);

                    return mapResponse( responseString );
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            return false;
        }

        protected boolean mapResponse(String response) {
            try {
                JSONObject jsonObject = new JSONObject(response);

                if(!jsonObject.isNull("data")) {
                    JSONObject data = jsonObject.getJSONArray("data").getJSONObject(0);

                    if(!data.isNull("member_id"))               accountID = data.getString("member_id");
                    if(!data.isNull("screen_name"))             accountScreenName = data.getString("screen_name");
                    if(!data.isNull("email"))                   accountEmail = data.getString("email");
                    if(!data.isNull("fb_id"))                   facebookID = data.getString("fb_id");
                    if(!data.isNull("fb_token"))                facebookToken = data.getString("fb_token");
                    if(!data.isNull("fb_status"))               facebookMappedStatus = data.getInt("fb_status");
                    // TODO: implement twitter and instagram
                    if(!data.isNull("accesstoken"))             accountToken = data.getString("accesstoken");

                    /** Have no avatar parameter from backend, Feb 4, 2014 - 10:33 */
                    if(!data.isNull("avatar")) {
                        accountAvatar = data.getString("avatar");
                    } else {
                        accountAvatar = DEFAULT_AVATAR_URL;
                    }

                    return true;
                }

            } catch (JSONException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return false;
        }

        @Override
        protected void onPostExecute(Boolean success) {
            super.onPostExecute(success);

            try {

                if(!success) {
                    clearAccountData();

                    if(!this.isAuthen) {
                        // TODO: alert dialog;
                        Log.d("test", "un authorized user");
                    }
                }

                Log.d("test", "finish login with account" +
                        "\ntoken : " + accountToken +
                        "\nid : " + accountID +
                        "\nscreenname : " + accountScreenName +
                        "\nemail : " + accountEmail +
                        "\navatar : " + accountAvatar);

                mFetchInfoListener.didFinishFetchInfo(success);
            } catch (ClassCastException e) {
                throw new ClassCastException(TAG + ": must implement FetchInfoInterface.");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private class RegisterTask extends AsyncTask<String, Void, Boolean> {

        @Override
        protected Boolean doInBackground(String... params) {
            String url = BSHttpUtils.newInstance().getfullURL("http://api.reedicious.com/rest/member/register", new String[] {
                    "appid", "1",
                    "token", apiToken
            });

            HttpClient client = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(url);

            List<NameValuePair> postParams = BSHttpUtils.newInstance().initPostParams(
                    "email", accountEmail,
                    "password", accountPassword,
                    "screenname", accountScreenName,
                    "avatar", accountAvatar
            );

            try {
                httpPost.setEntity(new UrlEncodedFormEntity(postParams, "UTF-8"));

                HttpResponse response = client.execute(httpPost);

                int statusCode = response.getStatusLine().getStatusCode();

                if(statusCode == 200) {

                    HttpEntity entity = response.getEntity();
                    String responseString = BSHttpUtils.newInstance().streamToString( entity.getContent() );

                    return mapResponse( responseString );
                }
            } catch (IOException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }

            return false;
        }

        private boolean mapResponse(String res) {

            try {
                JSONObject jsonObject = new JSONObject(res);

                if(!jsonObject.isNull("header")) {
                    JSONObject header = jsonObject.getJSONObject("header");
                    if(!header.isNull("code"))              currentErrorCode = header.getInt("code");
                    else                                    currentErrorCode = -1;

                    if(!header.isNull("message"))           currentResponseMsg = header.getString("message");
                    else                                    currentResponseMsg = "";
                }

                if(!jsonObject.isNull("data")) {
                    JSONObject data = jsonObject.getJSONObject("data");
                    if(!data.isNull("accesstoken")) {
                        accountToken = data.getString("accesstoken");
                        return true;
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return false;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);

            if(!aBoolean) {
                clearAccountData();
            } else {

                Map<String, String> param = new HashMap<String, String>();
                param.put("memberID", accountID);
                param.put("screenName", accountScreenName);
                param.put("email", accountEmail);
                FlurryAgent.logEvent("New Register By Email", param);

            }

            try {
                mRegisterListener.didFinishRegister(aBoolean);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private class UpdateInfoTask extends AsyncTask<Void, Void, Boolean> {

        List<BasicNameValuePair> putParams;

        public UpdateInfoTask(List<BasicNameValuePair> putParams) {
            this.putParams = new ArrayList<BasicNameValuePair>();
            this.putParams.addAll( putParams );
        }

        @Override
        protected Boolean doInBackground(Void... params) {

            String baseURL = "http://api.reedicious.com/rest/member/update";
            String[] urlParams = new String[] {
                    "appid", "1",
                    "token", apiToken
            };

            String url = BSHttpUtils.newInstance().getfullURL(baseURL, urlParams);

            HttpClient client = new DefaultHttpClient();
            HttpPut put = new HttpPut(url);

            // Set Header
            put.setHeader("accesstoken", accountToken);

            try {
                put.setEntity(new UrlEncodedFormEntity(putParams, "UTF-8"));
                HttpResponse response = client.execute(put);

                int statusCode = response.getStatusLine().getStatusCode();
                Log.d("test", "Update Info response status code : " + statusCode);

                HttpEntity responseEntity = response.getEntity();
                String responseString = BSHttpUtils.newInstance().streamToString( responseEntity.getContent() );

                Log.d("test", "Update Info string response : " + responseString);

                return mapResponse( responseString );
            } catch (Exception e) {
                e.printStackTrace();
            }

            return false;
        }

        private boolean mapResponse(String res) {
            try {
                JSONObject jsonObject = new JSONObject(res);
                if(!jsonObject.isNull("data")) {
                    JSONObject data = jsonObject.getJSONObject("data");
                    if(!data.isNull("accesstoken")) {
                        accountToken = data.getString("accesstoken");
                        return true;
                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

            return false;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);

            // No callback interface
            Log.d("test", TAG + " : update user info result : " + aBoolean);
        }
    }

    /**
     *
     *  Location
     *
     */
    private double lat, lng;
    private boolean isGooglePlayServicesAvailable;
    private boolean canGetGoogleLocation;

    public boolean canGetGoogleLocation() {
        return canGetGoogleLocation;
    }
    public void setCanGetGoogleLocation(boolean can) {
        canGetGoogleLocation = can;
    }


    public boolean isGooglePlayServicesAvailable() {
        return isGooglePlayServicesAvailable;
    }
    public void setGooglePlayServicesAvailable(boolean available) {
        isGooglePlayServicesAvailable = available;
    }

    public void setLatestLatLng(double lat, double lng) {
        this.lat = lat;
        this.lng = lng;
    }

    public LatLng getLatestLatLng() {
        return new LatLng(lat, lng);
    }




    /**
     *
     *  Campaign
     *
     */
    private String campaignTitle;
    private String campaignLink;
    private String campaignCover;
    private String campaignDesc;
    private String campaignID;

    public String getCampaignTitle() {
        return campaignTitle;
    }

    public void setCampaignTitle(String campaignTitle) {
        this.campaignTitle = campaignTitle;
    }

    public String getCampaignLink() {
        return campaignLink;
    }

    public void setCampaignLink(String campaignLink) {
        this.campaignLink = campaignLink;
    }

    public String getCampaignCover() {
        return campaignCover;
    }

    public void setCampaignCover(String campaignCover) {
        this.campaignCover = campaignCover;
    }

    public String getCampaignDesc() {
        return campaignDesc;
    }

    public void setCampaignDesc(String campaignDesc) {
        this.campaignDesc = campaignDesc;
    }

    public String getCampaignID() {
        return campaignID;
    }

    public void setCampaignID(String campaignID) {
        this.campaignID = campaignID;
    }

    public void saveCampaign(Campaign campaign) {
        this.campaignID = campaign.getId();
        this.campaignLink = campaign.getCampaign_url();
        this.campaignDesc = campaign.getJoin_campaign();
        this.campaignCover = campaign.getCover();
        this.campaignTitle = campaign.getTitle();
    }
}
