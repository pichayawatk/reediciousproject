package com.brainsourceapp.reedicious.rest.results;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Property {

    public int totalrecord;

    public int totalpage;

    public int page;

    public int limit;
}
