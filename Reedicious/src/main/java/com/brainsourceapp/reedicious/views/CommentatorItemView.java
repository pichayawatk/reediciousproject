package com.brainsourceapp.reedicious.views;

import android.content.Context;
import android.util.Log;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.brainsourceapp.reedicious.R;
import com.brainsourceapp.reedicious.rest.results.Commentator;
import com.brainsourceapp.reedicious.utils.BSDateHelper;
import com.squareup.picasso.Picasso;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

import java.util.Calendar;

@EViewGroup(R.layout.cell_comment)
public class CommentatorItemView extends LinearLayout {

    static final int NOW = 10;
    static final int MIN = 60;
    static final int HOUR = 3600;
    static final int DAY = 86400;

    @ViewById(R.id.cell_comment_profile_pic)
    ImageView profilePic;

    @ViewById(R.id.cell_comment_time)
    RDTextView time;

    @ViewById(R.id.cell_comment_username)
    RDTextView userName;

    @ViewById(R.id.cell_comment_info)
    RDTextView info;

    public CommentatorItemView(Context context) {
        super(context);
    }

    public void bind(Commentator commentator) {

        int commentedTime = commentator.comment_date;
        int currentTime = (int) (System.currentTimeMillis() / 1000);

        int duration = currentTime - commentedTime;
        String date = getDateFromCurrentTimestamp( duration );

        time.setText(date);

        userName.setText(commentator.name);

        info.setText(commentator.comment);

        try {
            Log.d("test", "avatar url : " + commentator.avatar);
            Picasso.with(getContext())
                    .load( commentator.avatar )
                    .placeholder(R.drawable.reedicious_avatar)
                    .into( profilePic );
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    String getDateFromCurrentTimestamp(int duration) {

        if(duration < NOW) {

            return String.format("Just now.");
        }
        else if(duration < MIN) {

            return String.format("%d seconds ago.", duration);
        }
        else if(duration == MIN) {

            return "1 minute ago.";
        }
        else if(duration < HOUR) {

            return String.format("%d minutes ago.", duration/MIN);
        }
        else if(duration == HOUR) {

            return "1 hour ago.";
        }
        else if(duration < DAY) {

            return String.format("%d hours ago.", duration/HOUR);
        }
        else if(duration == DAY) {

            return "1 day ago.";
        }
        else {
            // More than 1 day, set to default date format
            long current = Calendar.getInstance().getTimeInMillis() / 1000;
            current = current - duration;
            return BSDateHelper.timestampToThaiDateFormat(current, "dd/MM/yyyy");
        }
    }
}
