package com.brainsourceapp.reedicious.rest.response;

import com.brainsourceapp.reedicious.rest.results.Header;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MemberUpdateResponse {

    public Header header;

    public Data data;

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class Data {

        public String accesstoken;

    }

}
