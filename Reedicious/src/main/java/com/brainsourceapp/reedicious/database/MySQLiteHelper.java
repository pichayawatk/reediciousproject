package com.brainsourceapp.reedicious.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class MySQLiteHelper extends SQLiteOpenHelper {

    private static MySQLiteHelper instance;

    public static MySQLiteHelper getInstance(Context context) {
        if(instance == null) {
            instance = new MySQLiteHelper(context.getApplicationContext());
        }
        return instance;
    }

    public static final String TABLE_USER = "rd_user";
    public static final String USER_ID = "_id";
    public static final String USER_MEMBERID = "member_id";
    public static final String USER_SCREEN_NAME = "screen_name";

    public static final String TABLE_QRCODE = "rd_qrcode";
    public static final String QR_ID = "_id";
    public static final String QR_TYPE = "type";
    public static final String QR_RESPONSE_MESSAGE = "response_message";
    public static final String QR_ALERT_MESSAGE = "alert_message";
    public static final String QR_INTENT_DATA = "intent_data";
    public static final String QR_MEMBER_ID = "member_id";
    public static final String QR_CREATED_AT = "created_at";

    private static final String DATABASE_NAME = "reedicious.db";
    private static final int DATABASE_VERSION = 1;

    // Database creation sql statement
    private static final String DATABASE_USER_CREATE = "create table "
            + TABLE_USER + "("
            + USER_ID + " integer primary key autoincrement,"
            + USER_MEMBERID + " text not null,"
            + USER_SCREEN_NAME + " text not null)";

    private static final String DATABASE_QRCODE_CREATE = "create table "
            + TABLE_QRCODE + "("
            + QR_ID + " integer primary key autoincrement,"
            + QR_TYPE + " integer not null,"
            + QR_RESPONSE_MESSAGE + " text not null,"
            + QR_ALERT_MESSAGE + " text not null,"
            + QR_INTENT_DATA + " text not null,"
            + QR_MEMBER_ID + " text not null,"
            + QR_CREATED_AT + " integer not null)";

    public MySQLiteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DATABASE_USER_CREATE);
        db.execSQL(DATABASE_QRCODE_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //Log.w(MySQLiteHelper.class.getName(),
        //        "Upgrading database from version " + oldVersion + " to "
        //                + newVersion + ", which will destroy all old data");
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_USER);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_QRCODE);
        onCreate(db);
    }
}
