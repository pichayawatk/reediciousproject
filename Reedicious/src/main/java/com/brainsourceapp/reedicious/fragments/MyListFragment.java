package com.brainsourceapp.reedicious.fragments;

import android.app.Activity;
import android.app.ListFragment;
import android.content.Context;
import android.graphics.drawable.StateListDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.brainsourceapp.reedicious.DetailActivity_;
import com.brainsourceapp.reedicious.R;
import com.brainsourceapp.reedicious.rest.data.MyMinimart;
import com.brainsourceapp.reedicious.rest.results.MyPlace;
import com.brainsourceapp.reedicious.transformers.image.CropSquareTransformation;
import com.brainsourceapp.reedicious.transformers.image.RoundedTransformation;
import com.squareup.picasso.Picasso;

import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.UiThread;

import java.util.ArrayList;

@EFragment
public class MyListFragment extends ListFragment {

    MyListAdapter mAdapter;

    public static MyListFragment newInstance() {
        return new MyListFragment_();
    }

    public interface ListFragmentCallback {
        void onLastCellDidAppear();
    }

    public ListFragmentCallback mCallback;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        assert getActivity() != null;
        try {
            mCallback = (ListFragmentCallback) getActivity();
        } catch (ClassCastException e) {
            throw new ClassCastException(getActivity().getLocalClassName() + " must implement interface");
        }

        if(mAdapter == null) {
            mAdapter = new MyListAdapter(getActivity(), mCallback);
            assert getListView() != null;
            getListView().setAdapter( mAdapter );
            getListView().setSelector(new StateListDrawable());
            setListShown(true);
        }
    }

    @UiThread
    public void updatePlaces(ArrayList<MyPlace> places, ArrayList<MyMinimart> minimarts, boolean clear) {
        // Avoid crash : Content view not yet created.
        try {
            if(getListView() != null) {
                setListShown(false);

                if(clear) {
                    mAdapter.clearAllData();
                }

                mAdapter.addAllItems(places);
                mAdapter.addmMarts( minimarts );
                setListShown(true);
            }
        } catch (IllegalStateException e) {
            Log.i("test", "Content view not yet created");
            Log.i("test", e.getMessage());
        }

        try {
            setListShown( true );
        } catch (Exception e) {
            Log.i("test", "told you that content view is not yet created");
        }
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        // Get object from adapter
        MyPlace place = mAdapter.getItem( position );

        // Get the place's id
        String placeID = String.valueOf( place.id );

        // And parse as a parameter to Detail Activity
        DetailActivity_.intent(getActivity()).placeID( placeID ).start();

    }

    /**
     |-------------------------------------------------------
     |  Adapter
     |-------------------------------------------------------
     |
     |
     |
     */
    private static class MyListAdapter extends BaseAdapter {

        private Context mContext;
        private LayoutInflater mInflater;
        private ArrayList<MyPlace> mData;
        private ArrayList<MyMinimart> mDMarts;
        private String distancePrefix;
        private String distancePostfix;
        private ListFragmentCallback callback;

        public MyListAdapter(Context context, ListFragmentCallback callback) {
            mContext = context;
            mInflater = ((Activity) context).getLayoutInflater();
            mData = new ArrayList<MyPlace>();
            mDMarts = new ArrayList<MyMinimart>();
            distancePrefix = context.getResources().getString(R.string.label_distance);
            distancePostfix = context.getResources().getString(R.string.unit_distance);
            this.callback = callback;
        }

        /*
        public ArrayList<MyPlace> getAllItems() {
            ArrayList<MyPlace> list = new ArrayList<MyPlace>();
            list.addAll(mData);
            return list;
        }
        */

        public void addmMarts(ArrayList<MyMinimart> marts) {
            mDMarts.addAll( marts );
        }

        public void clearAllData() {
            if(mData != null) {
                mData.clear();
            }

            if(mDMarts != null) {
                mDMarts.clear();
            }

            mData = new ArrayList<MyPlace>();
            mDMarts = new ArrayList<MyMinimart>();
        }

        public void addAllItems(ArrayList<MyPlace> places) {
            for(MyPlace place : places) {
                mData.add( place );
            }
            notifyDataSetChanged();
        }

        @Override
        public int getCount() {
            return mData.size();
        }

        @Override
        public MyPlace getItem(int position) {
            return mData.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View view, ViewGroup parent) {
            ViewHolder holder;

            if(view == null) {
                holder = new ViewHolder();
                view = mInflater.inflate(R.layout.cell_list_layout, null);

                assert view != null;
                holder.title = (TextView) view.findViewById(R.id.cell_list_title);
                holder.address = (TextView) view.findViewById(R.id.cell_list_address);
                holder.distance = (TextView) view.findViewById(R.id.cell_list_distance);
                holder.image = (ImageView) view.findViewById(R.id.cell_list_thumb);

                view.setTag( holder );
            } else {

                holder = (ViewHolder) view.getTag();
            }

            // Update view
            MyPlace item = mData.get( position );
            try {
                Picasso.with(mContext)
                        .load(item.cover)
                        .placeholder(R.drawable.default_700x700)
                        .transform(new CropSquareTransformation())
                        .transform(new RoundedTransformation(15, 0))
                        .into(holder.image);
            } catch (Exception e) {
                e.printStackTrace();
            }


            holder.title.setText(item.title);
            holder.address.setText( item.address );

            String distanceText = String.format( "%.2f", Double.valueOf( item.distance ) / 1000f );
            if(distanceText.contains("0.00")) {
                holder.distance.setText("");
            } else {
                holder.distance.setText( distancePrefix + " " + distanceText + " " + distancePostfix );
            }


            // Animation
            //Animation animation = AnimationUtils.loadAnimation(mContext, R.anim.scale_to_right_bounce);
            //view.startAnimation(animation);

            if(position == mData.size() - 1) {

                if(callback != null) {
                    callback.onLastCellDidAppear();
                }
            }

            return view;
        }

        /**
         |--------------------------------------
         |  ViewHolder pattern
         |--------------------------------------
         |
         |
         */
        private static class ViewHolder {
            TextView title, address, distance;
            ImageView image;
        }
    }
}
