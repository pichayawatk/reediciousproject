package com.brainsourceapp.reedicious.rest.task;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.brainsourceapp.reedicious.R;
import com.brainsourceapp.reedicious.rest.results.Header;
import com.brainsourceapp.reedicious.rest.utils.BSHttpUtils;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;

public class ResetPasswordRequestTask extends AsyncTask<String, String, Header> {

    Context mContext;
    String apiToken;
    String receiverEmail;

    public ResetPasswordRequestTask(Context mContext, String apiToken, String receiverEmail) {
        this.mContext = mContext;
        this.apiToken = apiToken;
        this.receiverEmail = receiverEmail;
    }

    @Override
    protected Header doInBackground(String... params) {
        String baseURL = "http://api.reedicious.com/rest/auth/forgot";
        String url = BSHttpUtils.newInstance().getfullURL(baseURL, new String[]{
                "appid", "1",
                "token", apiToken
        });

        HttpClient client = new DefaultHttpClient();
        HttpPost httpPost = new HttpPost(url);

        List<NameValuePair> nameValuePairs = BSHttpUtils.newInstance().initPostParams(
                "email", receiverEmail
        );

        // Add data
        try {
            httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs, "UTF-8"));

            HttpResponse response = client.execute(httpPost);

            // Get status code
            int statusCode = response.getStatusLine().getStatusCode();
            Log.d("test", "global statusCode : " + statusCode);

            String responseString = BSHttpUtils.newInstance().streamToString( response.getEntity().getContent() );

            // Mapping response
            JSONObject jsonObject = new JSONObject( responseString );
            if(jsonObject.has("header")) {
                Header ret = new Header();
                JSONObject header = jsonObject.getJSONObject("header");
                if(header.has("code"))                      ret.setCode( header.getInt("code") );
                if(header.has("message"))                   ret.setMessage( header.getString("message"));

                return ret;
            }

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onPostExecute(Header response) {
        super.onPostExecute(response);

        if(response == null) {
            Log.d("test", "error null response");
        } else {
            int code = response.getCode();
            Log.d("test", "status Code : " + code);
            String alertMsg = mContext.getResources().getString(R.string.reset_password_sent_email_success);
            Toast.makeText(mContext, alertMsg, Toast.LENGTH_LONG).show();
        }
    }
}