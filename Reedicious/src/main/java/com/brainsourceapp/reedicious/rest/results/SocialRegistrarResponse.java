package com.brainsourceapp.reedicious.rest.results;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SocialRegistrarResponse {

    public Header header;

    public SocialRegistrarResponseData data;


    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class SocialRegistrarResponseData {

        public String accesstoken;

    }

}
