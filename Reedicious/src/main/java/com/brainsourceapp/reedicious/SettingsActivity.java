package com.brainsourceapp.reedicious;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.view.View;

import com.brainsourceapp.reedicious.beans.MyAccount;
import com.brainsourceapp.reedicious.fragments.LoginFragment;
import com.brainsourceapp.reedicious.fragments.SettingsFragment;
import com.brainsourceapp.reedicious.utils.BSAlertDialog;
import com.brainsourceapp.reedicious.utils.BSProgressHUD;
import com.brainsourceapp.reedicious.views.FacebookMapperView;
import com.brainsourceapp.reedicious.views.RegistrarView;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.model.GraphUser;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.OptionsItem;
import org.androidannotations.annotations.OptionsMenu;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.json.JSONException;

import java.util.Arrays;

@EActivity(R.layout.activity_settings)
@OptionsMenu(R.menu.settings)
public class SettingsActivity extends Activity implements
        FacebookMapperView.ToggleStatusDialog.SocialMappingStatusListener,
        RegistrarView.RegistrarViewListener,
        LoginFragment.LoginFragmentFBLoginListener,
        LoginFragment.LoginFragmentAccountLoginListener,
        SettingsFragment.SettingsFragmentListener {

    private static final String TAG = SettingsActivity.class.getSimpleName();

    public static final int UPDATE_PROFILE_ACTIVITY_CODE = 4002;
    public static final int REGISTER_ACTIVITY_CODE = 5003;

    Context context;

    @Extra
    boolean returnUser;

    @ViewById(R.id.settings_registrar_layout)
    RegistrarView registrarView;

    SettingsFragment mSettingsFragment;
    LoginFragment mLoginFragment;

    @AfterViews
    void afterSettingsViews() {

        ActionBar actionBar = getActionBar();

        context = this;

        if(actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled( true );
        }

        if(MyAccount.getInstance().isLoggedIn()) {
            showSettingsFragment();
        } else {
            showLoginFragment();
        }
    }

    @OptionsItem(android.R.id.home)
    void didClickedHomeMenu() {
        try {
            NavUtils.navigateUpFromSameTask(this);
        } catch (Exception e) {
            finish();
        }
    }

    @Override
    public void onMappingStatusChanged(boolean willMap) {
        assert mSettingsFragment != null;
        if(willMap) {

            didClickFBLoginButton();
        } else {

            /** UNMAPPED FACEBOOK */
            MyAccount.getInstance().unmapFacebookStatus();
            mSettingsFragment.getFacebookMapperView().updateEmail("คุณยังไม่ได้เชื่อมต่อ");
            mSettingsFragment.getFacebookMapperView().updateButton("เพิ่ม");
        }
    }

    @Override
    public void didClickRegistrarButton(View view) {
        RegistrarActivity_.intent(this).startForResult(REGISTER_ACTIVITY_CODE);
    }

    /**
     *
     *  Facebook Zone
     *
     */

    private UiLifecycleHelper uiLifecycleHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        uiLifecycleHelper = new UiLifecycleHelper(this, statusCallback);
        uiLifecycleHelper.onCreate(savedInstanceState);
    }

    @Override
    protected void onResume() {
        super.onResume();
        uiLifecycleHelper.onResume();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        uiLifecycleHelper.onActivityResult(requestCode, resultCode, data);

        if(requestCode == UPDATE_PROFILE_ACTIVITY_CODE && resultCode == RESULT_OK) {
            if(mSettingsFragment != null) {
                mSettingsFragment.updateUserInfo();
            }
        }

        if(requestCode == REGISTER_ACTIVITY_CODE && resultCode == RESULT_OK) {
            Log.d("test", TAG + " finish registrar and will login with accesstoken : " + MyAccount.getInstance().getAccountToken());
            showSettingsFragment();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        uiLifecycleHelper.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        uiLifecycleHelper.onDestroy();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        uiLifecycleHelper.onSaveInstanceState(outState);
    }

    @UiThread
    public void showSettingsFragment() {
        registrarView.setVisibility(View.INVISIBLE);
        mSettingsFragment = SettingsFragment.newInstance();
        getFragmentManager().beginTransaction()
                .replace(R.id.settings_fragment_container, mSettingsFragment)
                .commit();
    }

    @UiThread
    public void showLoginFragment() {
        registrarView.setVisibility(View.VISIBLE);
        mLoginFragment = LoginFragment.newInstance();
        getFragmentManager().beginTransaction()
                .replace(R.id.settings_fragment_container, mLoginFragment)
                .commit();
    }

    /**
     *
     *  Callback Interface From another fragments / Activities.
     *
     */
    @Override
    public void didSuccessAccountLogin() {
        showSettingsFragment();
    }

    @Override
    public void didClickLogOutButton() {
        BSAlertDialog.newInstance("คุณแน่ใจที่จะลงชื่อออกจาก Application", "ยืนยัน", "ยกเลิก", new BSAlertDialog.CallbackDialogListener() {
            @Override
            public void onBSDialogPosBtnClicked(DialogInterface dialog, String tag) {
                dialog.dismiss();
                try {
                    Session.getActiveSession().closeAndClearTokenInformation();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                MyAccount.getInstance().clearAccountData();
                showLoginFragment();

            }
        }).show(getFragmentManager(), "logout dialog");
    }

    @Override
    public void didClickFBLoginButton() {
        onClickLogin();
    }

    private Session.StatusCallback statusCallback = new SessionStatusCallback();

    private void onClickLogin() {
        Session session = Session.getActiveSession();
        if (!session.isOpened() && !session.isClosed()) {
            session.openForRead(new Session.OpenRequest(this)
                    .setPermissions(Arrays.asList("email"))
                    .setCallback(statusCallback));
        } else {
            Session.openActiveSession(this, true, statusCallback);
        }
    }

    private class SessionStatusCallback implements Session.StatusCallback {
        @Override
        public void call(Session session, SessionState state, Exception exception) {

            Log.d("test", "Session state changed : " + state.toString());

            if(MyAccount.getInstance().isLoggedIn() && MyAccount.getInstance().isFacebookStatusMapped()) {
                // Do nothing if user is already logged in.

                // If Facebook Status is Unmapped (user is already logged in), just map (after request user info)
                return;
            }

            // Respond to session state changes, ex: updating the view
            BSProgressHUD.showProgressHUD(context, "LOADING");
            Request.newMeRequest(session, new Request.GraphUserCallback() {
                @Override
                public void onCompleted(GraphUser user, Response response) {

                    BSProgressHUD.dismissProgressHUD();
                    if(user != null) {
                        mFacebookID = user.getId();
                        mFacebookToken = Session.getActiveSession().getAccessToken();

                        try {
                            mFacebookEmail = user.getInnerJSONObject().getString("email");

                            // Mapped status case (LoggedIn User), change social only
                            if(MyAccount.getInstance().isLoggedIn() && !MyAccount.getInstance().isFacebookStatusMapped()) {
                                MyAccount.getInstance().mapFacebookStatus(mFacebookID, mFacebookToken, mFacebookEmail);
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        mSettingsFragment.getFacebookMapperView().updateEmail(mFacebookEmail);
                                        mSettingsFragment.getFacebookMapperView().updateButton("ยกเลิก");
                                    }
                                });
                                return;
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            BSAlertDialog.showEasyDialog("ขออภัยค่ะ ไม่สามารถใช้อีเมล์ในการลงทะเบียน").show(getFragmentManager(), "no email");
                            return;
                        }
                        mFacebookName = user.getName();

                        Log.d("test", "\nfacebook id : " + mFacebookID +
                                "\nfacebookToken : " + mFacebookToken +
                                "\nuse for login with social");

                        startLoginWithSocial(mFacebookID, mFacebookToken);
                    }
                }
            }).executeAsync();
        }
    }

    String mFacebookID;
    String mFacebookToken;
    String mFacebookName;
    String mFacebookEmail;

    public void startLoginWithSocial(String facebookID, String facebookToken) {

        BSProgressHUD.showProgressHUD(this, "LOADING");
        MyAccount.getInstance().loginWithFacebook(facebookID, facebookToken, new MyAccount.MyAccountLoginWithFacebookListener() {
            @Override
            public void didLoginWithFacebook(boolean success) {
                BSProgressHUD.dismissProgressHUD();

                if(success) {
                    showSettingsFragment();
                } else {

                    /**
                     *
                     * No content
                     * New register with facebook account information
                     *
                     * */
                    registerWithFacebook();
                 }
            }
        });
    }

    @UiThread
    protected void registerWithFacebook() {

        BSProgressHUD.showProgressHUD(this, "LOADING");
        MyAccount.getInstance().registerWithFacebook(mFacebookEmail, mFacebookName, mFacebookID, mFacebookToken, new MyAccount.MyAccountRegisterWithFacebookListener() {
            @Override
            public void didFinishRegisterWithFacebook(boolean success) {

                BSProgressHUD.dismissProgressHUD();
                if(success) {
                    registrarView.setVisibility(View.INVISIBLE);
                    mSettingsFragment = SettingsFragment.newInstance();
                    getFragmentManager().beginTransaction()
                            .replace(R.id.settings_fragment_container, mSettingsFragment)
                            .commit();
                } else {
                    BSAlertDialog.showEasyDialog(R.string.default_error_msg).show(getFragmentManager(), "register with facebook error");
                }
            }
        });
    }


}