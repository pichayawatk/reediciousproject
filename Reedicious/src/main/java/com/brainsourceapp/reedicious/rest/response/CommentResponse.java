package com.brainsourceapp.reedicious.rest.response;

import com.brainsourceapp.reedicious.rest.results.Header;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CommentResponse {

    @JsonProperty(value = "header", required = false)
    public Header header;

    @JsonProperty(value = "data", required = false)
    public Data data;

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class Data {

        @JsonProperty(value = "accesstoken", required = false)
        public String accesstoken;

    }
}
