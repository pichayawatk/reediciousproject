package com.brainsourceapp.reedicious.rest.utils;

import android.util.Log;

import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.message.BasicNameValuePair;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

public class BSHttpUtils {

    public BSHttpUtils() {

    }

    public static BSHttpUtils newInstance() {
        return new BSHttpUtils();
    }

    public String getfullURL(String url, String[] params) {
        String fullURL = url;
        for(int i=0, len=params.length; i<len; i+=2) {
            if(i == 0) {
                fullURL += "?";
            } else {
                fullURL += "&";
            }

            try {
                fullURL = fullURL + params[i] + "=" + URLEncoder.encode(params[i+1], "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }

        Log.d("test", "url : " + fullURL);
        return fullURL;
    }

    public String streamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        String result = "";

        String line;
        try {
            while ((line = reader.readLine()) != null) {
                result = result + line + "\n";
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    public List<NameValuePair> initPostParams(String... params) {
        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        for(int i=0, len=params.length; i<len; i+=2) {
            nameValuePairs.add(new BasicNameValuePair(params[i], params[i+1]));
        }
        return nameValuePairs;
    }

    public UrlEncodedFormEntity initGetParams(String[] params) {
        List<NameValuePair> pairs = new ArrayList<NameValuePair>();
        for(int i=0, len=params.length; i<len; i+=2) {
            pairs.add(new BasicNameValuePair(params[i], params[i+1]));
        }
        UrlEncodedFormEntity ret;
        try {
            ret = new UrlEncodedFormEntity(pairs);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            ret = null;
        }
        if(ret == null) {
            Log.d("test", "unsupported Encoding exception");
        }
        return ret;
    }

    // TODO: parser

}
