package com.brainsourceapp.reedicious.views;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.brainsourceapp.reedicious.R;
import com.brainsourceapp.reedicious.SettingsActivity_;
import com.brainsourceapp.reedicious.beans.MyAccount;
import com.squareup.picasso.Picasso;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

@EViewGroup(R.layout.comment_user_header_view)
public class CommentUserHeader extends RelativeLayout {

    public CommentUserHeader(Context context) {
        super(context);
    }

    public CommentUserHeader(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CommentUserHeader(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @ViewById(R.id.comment_login_btn)
    Button loginBtn;

    @Click(R.id.comment_login_btn)
    public void onLoginButtonClicked() {
        Context context = getContext();

        SettingsActivity_.intent(context).start();
    }

    @ViewById(R.id.comment_input_box)
    RDEditText commentBox;

    @Click(R.id.comment_input_box)
    public void onSendCommentBtnClicked() {

        assert commentBox.getText() != null;
        String comment = commentBox.getText().toString();

        assert comment != null;

        // TODO: Hide keyboard
        // TODO: Show progressdialog
        // TODO: Background Task
    }

    @Background
    public void sendCommentDataToBackend() {

    }

    @UiThread
    public void upateLocalDevices() {

    }

    @ViewById(R.id.comment_user_profile_picture)
    ImageView profileImage;

    @ViewById(R.id.comment_user_email_text)
    RDTextView useremailView;

    @ViewById(R.id.comment_user_name_text)
    RDTextView usernameView;

    @UiThread
    public void showLoggedInUser(boolean show) {
        if(show) {

            loginBtn.setVisibility(INVISIBLE);

            profileImage.setVisibility(VISIBLE);
            useremailView.setVisibility(VISIBLE);
            usernameView.setVisibility(VISIBLE);

            // update profile
            String profileImageURL = MyAccount.getInstance().getAccountAvatar();
            String username = MyAccount.getInstance().getAccountScreenName();
            String email = MyAccount.getInstance().getAccountEmail();

            if(profileImageURL == null || profileImageURL.trim().length() == 0) {
                Log.d("test", "cannot load image at empty URL");
            } else {
                try {
                    // TODO: load image here
                    Picasso.with(getContext())
                            .load(profileImageURL)
                            .into( profileImage );
                } catch (Exception e) {
                    Log.d("test", "error : " + e.getMessage());
                }
            }

            usernameView.setText( username );
            useremailView.setText( email );
        } else {
            loginBtn.setVisibility(VISIBLE);

            usernameView.setText( "" );
            useremailView.setText( "" );

            profileImage.setVisibility(INVISIBLE);
            useremailView.setVisibility(INVISIBLE);
            usernameView.setVisibility(INVISIBLE);
        }
    }


}
