package com.brainsourceapp.reedicious.fragments;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.app.Fragment;
import android.content.res.Resources;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.brainsourceapp.reedicious.R;
import com.brainsourceapp.reedicious.beans.MyAccount;
import com.brainsourceapp.reedicious.beans.MyRestBean;
import com.brainsourceapp.reedicious.rest.results.PlacePromotion;
import com.flurry.android.FlurryAgent;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.res.StringRes;
import org.springframework.web.client.RestClientException;

import java.util.HashMap;
import java.util.Map;

import uk.co.senab.photoview.PhotoViewAttacher;

@EFragment(R.layout.fragment_promotions)
public class PromotionsFragment extends Fragment {

    public static PromotionsFragment newInstance(int placeID) {
        PromotionsFragment fragment = new PromotionsFragment_();
        fragment.placeID = placeID;
        Log.d("test", "placeID : " + fragment.placeID);
        return fragment;
    }

    boolean isTextShown;

    int yAnimateDistance;

    int placeID;
    PhotoViewAttacher mAttacher;

    @StringRes(R.string.fallback_no_promotion)
    String fallbackString;

    @ViewById(R.id.place_promotion_stage)
    ImageView promotionStage;

    @ViewById(R.id.place_promotion_detail)
    TextView promotionDetailView;

    @ViewById(R.id.place_no_promotion_text)
    TextView promotionFallbackText;

    @ViewById(R.id.place_fallback_view)
    LinearLayout fallbackView;

    @Click(R.id.place_promotion_detail)
    void slideToShowText() {

        ObjectAnimator anim = ObjectAnimator.ofFloat(promotionDetailView, View.TRANSLATION_Y, 0f, -yAnimateDistance);
        anim.setDuration( 300 );
        anim.setInterpolator(new AccelerateDecelerateInterpolator());

        ObjectAnimator anim2 = ObjectAnimator.ofFloat(promotionStage, View.ALPHA, 0f, 0.5f);
        anim2.setDuration( 300 );
        anim2.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                promotionStage.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animator animation) {

            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });

        AnimatorSet set = new AnimatorSet();
        set.playTogether(anim, anim2);
        set.start();

        isTextShown = true;
        promotionDetailView.setEnabled( false );
        promotionStage.setEnabled( true );
    }

    @Click(R.id.place_promotion_stage)
    void slideToHideText() {

        ObjectAnimator anim = ObjectAnimator.ofFloat(promotionDetailView, View.TRANSLATION_Y, yAnimateDistance, 0f);
        anim.setDuration(300);
        anim.setInterpolator(new AccelerateDecelerateInterpolator());

        ObjectAnimator anim2 = ObjectAnimator.ofFloat(promotionStage, View.ALPHA, 0.5f, 0f);
        anim2.setDuration(300);
        anim2.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                promotionStage.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });

        AnimatorSet set = new AnimatorSet();
        set.playTogether(anim, anim2);
        set.start();

        isTextShown = false;
        promotionDetailView.setEnabled( true );
        promotionStage.setEnabled( false );
    }

    @Bean
    MyRestBean restBean;

    @ViewById(R.id.place_promotion_imageview)
    ImageView imageView;

    @AfterViews
    void afterPromotionsView() {

        Map<String, String> param = new HashMap<String, String>();
        param.put("screenName", MyAccount.getInstance().getAccountScreenName());
        param.put("placeID", placeID + "");
        FlurryAgent.logEvent("Read Promotion in Details Page", param);


        // Set title
        Activity activity = getActivity();
        //ActionBar actionBar = activity.getActionBar();
        //actionBar.setTitle("Promotions");

        // Get y-axis animate distance
        if(activity != null) {
            Resources resources = activity.getResources();
            yAnimateDistance = Math.abs( resources.getDimensionPixelSize( R.dimen.promotion_init_animation_margin ) );
        }

        // Disable stage view
        promotionStage.setEnabled( false );
        promotionStage.setVisibility(View.GONE);

        // Hide fallback text
        promotionFallbackText.setVisibility( View.GONE );

        // TODO: progress dialog
        getPromotionAsync();
    }

    @Background(id = "background_task")
    void getPromotionAsync() {

        PlacePromotion.Data result = null;
        try {

            //TODO: use promotionID instead of placeID
            result = restBean.getPromotionByID( placeID );
        } catch (RestClientException e) {

            Log.d("test", "rest error : " + e.getMessage());
        }

        updateUI(result);
    }

    @UiThread
    void updateUI(PlacePromotion.Data result) {
        // TODO: dismiss progress dialog

        // Have no promotion, load default image.
        if(result == null) {

            //BackgroundExecutor.cancelAll("background_task", true);

            // Fallback layout params
            RelativeLayout.LayoutParams fallbackParams = (RelativeLayout.LayoutParams) fallbackView.getLayoutParams();
            if(fallbackParams != null) {
                fallbackParams.addRule( RelativeLayout.CENTER_IN_PARENT );
            }

            // Image holder layout params
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT );
            params.gravity = Gravity.CENTER_HORIZONTAL;

            imageView.setLayoutParams( params );
            Picasso.with( getActivity() ).load( R.drawable.icon_no_promotion ).into(imageView);

            promotionDetailView.setVisibility(View.GONE);
            promotionFallbackText.setVisibility( View.VISIBLE );
            promotionFallbackText.setText( fallbackString );

        // Got it, bang bang bang
        } else {

            String imageURL = result.cover;

            String promotionDetailText = result.desc;

            promotionDetailView.setText( promotionDetailText );
            promotionDetailView.setVisibility( View.VISIBLE );

            promotionFallbackText.setVisibility( View.GONE );

            DisplayMetrics dm = new DisplayMetrics();
            Activity activity = getActivity();
            if(activity != null) {
                WindowManager wm = activity.getWindowManager();

                if(wm != null) {

                    // Get default display metrics
                    wm.getDefaultDisplay().getMetrics( dm );

                    // Get screenWidth and imageHeight
                    int w = dm.widthPixels;
                    int h = activity.getResources().getDimensionPixelOffset( R.dimen.promotion_detail_width );

                    Picasso.with( activity ).load( imageURL ).resize(w, h).centerInside().into( imageView, new Callback() {
                        @Override
                        public void onSuccess() {
                            if(imageView == null) {
                                Log.d("test", "promotion cover image is null");
                                return;
                            }

                            mAttacher = new PhotoViewAttacher( imageView );
                            mAttacher.setOnPhotoTapListener(new PhotoViewAttacher.OnPhotoTapListener() {
                                @Override
                                public void onPhotoTap(View view, float x, float y) {

                                    // TODO: go to share page
                                    Log.d("test", "image clicked, navigating to facebook share page");
                                }
                            });
                        }

                        @Override
                        public void onError() {
                            Log.d("test", "load place's promotion image failed");
                        }
                    });
                }
            }
        }
    }

}
