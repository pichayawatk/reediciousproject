package com.brainsourceapp.reedicious.constants;

public class BSTag {

    public static class Request {

        public static final int PhotoSelector = 4703;

        public static final int ImageCropper = 3524;
    }

}
