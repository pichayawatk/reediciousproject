package com.brainsourceapp.reedicious.rest.results;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MenuResult {

    public Header header;

    public Property property;

    @JsonProperty(value = "data")
    public ArrayList<Data> menues;


    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class Data {

        public int id;

        public String title;

        public int category;

        public String cover;

        public double latitude;

        public double longitude;

        public double distance;

        @JsonProperty(value = "gallery")
        public ArrayList<Gallery> galleries;

        public String recipes;
    }
}
