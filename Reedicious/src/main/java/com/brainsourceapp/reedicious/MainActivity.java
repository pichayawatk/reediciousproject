package com.brainsourceapp.reedicious;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.widget.FrameLayout;

import com.brainsourceapp.reedicious.beans.MyAccount;
import com.brainsourceapp.reedicious.fragments.RootIconMenuFragment;
import com.brainsourceapp.reedicious.fragments.SingleHotPromotionFragment;
import com.google.android.gms.maps.model.LatLng;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Fullscreen;
import org.androidannotations.annotations.InstanceState;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

@Fullscreen
@EActivity(R.layout.activity_main)
public class MainActivity extends Activity {

    /**
     |-------------------------------------------------
     | FRAGMENT TAG
     |-------------------------------------------------
     |
     |
     |
     */
    private static final String SINGLE_HOT_PROMOTION_FRAGMENT_TAG = "hotPromotion";
    private static final String ROOT_ICON_MENU_TAG = "rootIconMenu";

    RootIconMenuFragment rootIconMenuFragment;
    SingleHotPromotionFragment singleHotPromotionFragment;

    @ViewById(R.id.main_content_holder)
    FrameLayout contentHolder;

    @InstanceState
    boolean isViewBind;


    static boolean isPromotionShowed;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(savedInstanceState == null) {
            isPromotionShowed = false;
        }
    }

    /**
     |-------------------------------------------------
     | ON BACK PRESS
     |-------------------------------------------------
     |
     |
     |
     */
    @Override
    public void onBackPressed() {
        try {

            if(null != getFragmentManager().findFragmentByTag(SINGLE_HOT_PROMOTION_FRAGMENT_TAG)) {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        getFragmentManager().beginTransaction()
                                .replace(R.id.main_content_holder, RootIconMenuFragment.newInstance(), ROOT_ICON_MENU_TAG)
                                .commit();
                    }
                });

            } else {
                super.onBackPressed();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @AfterViews
    void afterMainActivityViews() {
        isViewBind = true;

        /*
        Intent intent = getIntent();
        boolean showPromotion = false;
        if(intent.hasExtra("promotion")) {
            showPromotion = intent.getBooleanExtra("promotion", false);
        }
        */

        // TODO: decision here
        if(!isPromotionShowed) {
            isPromotionShowed = true;

            Log.d("test", "add big promotion");

            addBigPromotionSafety();
        } else {

            addRootIconMenuSafety();
        }
    }

    /**
     *
     * Remember Even add root menu fragment, Picasso will not stop loading the image.
     *
     */
    @UiThread
    void addRootIconMenuSafety() {
        if(rootIconMenuFragment == null) {
            rootIconMenuFragment = RootIconMenuFragment.newInstance();
        }

        getFragmentManager().beginTransaction()
                .replace( R.id.main_content_holder, rootIconMenuFragment, ROOT_ICON_MENU_TAG)
                .commit();
    }

    @UiThread
    void addBigPromotionSafety() {
        LatLng latLng = MyAccount.getInstance().getLatestLatLng();
        singleHotPromotionFragment = SingleHotPromotionFragment.newInstance( latLng.latitude, latLng.longitude );

        getFragmentManager().beginTransaction()
                .replace( R.id.main_content_holder, singleHotPromotionFragment, SINGLE_HOT_PROMOTION_FRAGMENT_TAG)
                .commit();
    }

}
