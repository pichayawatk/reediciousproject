package com.brainsourceapp.reedicious;

import android.app.ActionBar;
import android.app.Activity;
import android.content.DialogInterface;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.widget.EditText;

import com.brainsourceapp.reedicious.beans.MyAccount;
import com.brainsourceapp.reedicious.utils.BSAlertDialog;
import com.brainsourceapp.reedicious.utils.BSProgressHUD;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.OptionsItem;
import org.androidannotations.annotations.OptionsMenu;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

@EActivity(R.layout.activity_registrar)
@OptionsMenu(R.menu.registrar)
public class RegistrarActivity extends Activity {

    @ViewById(R.id.regis_username_edittext)
    EditText usernameEditText;

    @ViewById(R.id.regis_email_edittext)
    EditText emailEditText;

    @ViewById(R.id.regis_password_edittext)
    EditText passwordEditText;

    @ViewById(R.id.regis_confirm_password_edittext)
    EditText confirmpasswordEditText;


    /**
     * --------------------------------------------------------------------------------------------------------
     *  Registrar Flow
     * --------------------------------------------------------------------------------------------------------
     */
    @Click(R.id.regis_ok_btn)
    void onOkBtnClicked() {
        assert usernameEditText.getText() != null && emailEditText.getText() != null && passwordEditText.getText() != null && confirmpasswordEditText.getText() != null;

        String username = usernameEditText.getText().toString();
        if("".equals(username)) {
            /** Empty screenName */
            BSAlertDialog.newInstance().easyDialog(R.string.error_code_506).show(getFragmentManager(), "error");
            return;
        }

        String email = emailEditText.getText().toString();
        if(email == null || email.trim().length() == 0) {
            /** No email */
            BSAlertDialog.newInstance().easyDialog(R.string.error_code_507).show(getFragmentManager(), "error");
            return;
        }
        if(!email.contains("@")) {
            /** Wrong format */
            BSAlertDialog.newInstance().easyDialog(R.string.error_code_508).show(getFragmentManager(), "error");
            return;
        }

        String password = passwordEditText.getText().toString();
        String confirmPass = confirmpasswordEditText.getText().toString();
        if(password == null || password.trim().length() == 0 || confirmPass == null || confirmPass.trim().length() == 0) {
            /** No password */
            BSAlertDialog.newInstance().easyDialog(R.string.error_code_509).show(getFragmentManager(), "error");
            return;
        }

        if(!password.equals(confirmPass)) {
            /** Mismatched password */
            BSAlertDialog.newInstance().easyDialog(R.string.q010_password_confirmation_failed).show(getFragmentManager(), "error");
            return;
        }

        String imageURL = MyAccount.getInstance().getAccountAvatar();
        Log.d("test", "registered member imageURL : " + imageURL);

        if(email.length() > 0 && password.length() > 0 && confirmPass.length() > 0 && username.length() > 0) {

            BSProgressHUD.showProgressHUD(this, "LOADING");
            MyAccount.getInstance().register(email, password, username, new MyAccount.MyAccountRegisterListener() {
                @Override
                public void didFinishRegister(boolean success) {
                    if(success) {
                        onSuccessRegister();
                    } else {
                        onFailedRegister();
                    }
                }
            });
        }
    }

    @UiThread
    public void onSuccessRegister() {
        // User Info should be updated

        String memberID = MyAccount.getInstance().getAccountID();
        String screenName = MyAccount.getInstance().getAccountScreenName();
        String email = MyAccount.getInstance().getAccountEmail();
        String accountPassword = MyAccount.getInstance().getAccountPassword();

        Log.d("test", "current memberID after registrar : " + memberID);

        String alertMsg = getResources().getString(R.string.p001_registeration_completed)
                + "\n" + getResources().getString(R.string.b_username) + " " + screenName
                + "\n" + getResources().getString(R.string.b_email) + " " + email
                + "\n" + getResources().getString(R.string.b_password) + " " + accountPassword;

        BSAlertDialog.newInstance(alertMsg, new BSAlertDialog.CallbackDialogListener() {
            @Override
            public void onBSDialogPosBtnClicked(DialogInterface dialog, String tag) {
                dialog.dismiss();
                setResult(RESULT_OK);
                finish();
            }
        }).show(getFragmentManager(), "alert");
    }

    @UiThread
    public void onFailedRegister() {
        MyAccount.getInstance().clearAccountData();
        BSAlertDialog.newInstance().easyDialog( MyAccount.getInstance().getCurrentResponseMsg() ).show(getFragmentManager(), "error");
    }
    /** -------------------------------------------------------------------------------------------------------- */

    @AfterViews
    void afterRegistrarViews() {

        setupActionBar();

    }

    private void setupActionBar() {
        ActionBar actionBar = getActionBar();
        if(actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    @OptionsItem(android.R.id.home)
    void didClickedHomeMenu() {
        try {
            NavUtils.navigateUpFromSameTask(this);
        } catch (Exception e) {
            finish();
        }
    }

}
