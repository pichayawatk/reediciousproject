package com.brainsourceapp.reedicious;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ListFragment;
import android.content.Context;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.brainsourceapp.reedicious.beans.MyAccount;
import com.brainsourceapp.reedicious.rest.ReedClient;
import com.brainsourceapp.reedicious.rest.results.CampaignResult;
import com.brainsourceapp.reedicious.utils.BSDialogIntentBuilder;
import com.brainsourceapp.reedicious.utils.BSProgressHUD;
import com.flurry.android.FlurryAgent;
import com.squareup.picasso.Picasso;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.FragmentById;
import org.androidannotations.annotations.OptionsItem;
import org.androidannotations.annotations.OptionsMenu;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.res.StringRes;
import org.androidannotations.annotations.rest.RestService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@EActivity(R.layout.activity_lucky_list)
@OptionsMenu(R.menu.lucky_list)
public class LuckyListActivity extends Activity {

    int page;
    int limit;

    LuckyAdapter mAdapter;

    @FragmentById(R.id.lucky_list_fragment)
    ListFragment luckyListFragment;

    @RestService
    ReedClient restClient;

    @AfterViews
    void afterLuckListViews() {

        setupActionBar();

        Map<String, String> param = new HashMap<String, String>();
        param.put("screenName", MyAccount.getInstance().getAccountScreenName());
        FlurryAgent.logEvent("View LuckyMan", param);

        // Init arguments
        page = 1;
        limit = 20;

        // Reference the instance
        luckyListFragment.setListAdapter( new LuckyAdapter(LuckyListActivity.this) );
        mAdapter = (LuckyAdapter) luckyListFragment.getListAdapter();

        // Remove divider
        assert luckyListFragment.getListView() != null;
        luckyListFragment.getListView().setDivider(null);
        luckyListFragment.getListView().setDividerHeight(0);

        // Start new Background Task
        //luckyListFragment.setListShown( false );
        BSProgressHUD.showProgressHUD(this, "LOADING...");
        getCampaignsAsync(page, limit);

        // Listener
        ListView listView = luckyListFragment.getListView();

        if(listView != null) {
            listView.setOnItemClickListener( new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                    CampaignResult.Data item = (CampaignResult.Data) parent.getItemAtPosition( position );

                    if(item != null) {

                        String url = item.url;
                        if(!url.startsWith("http://") && !url.startsWith("https://")) {
                            url = "http://" + url;
                        }

                        /** Start new WebViewActivity */
                        WebViewActivity_.intent(LuckyListActivity.this)
                                .url(  url )
                                .actionbarTitle( item.title )
                                .start();
                    }
                }
            });
        }
    }

    @Background
    void getCampaignsAsync(int page, int limit) {

        if(page == -1) {
            // EOF
            Log.d("test", "cannot get more data here.");
            return;
        }

        CampaignResult result = restClient.getCampaigns(MyAccount.getInstance().getApiToken(), page + "", limit + "");

        if(result == null || result.data == null) {

            updateUI( new ArrayList<CampaignResult.Data>() );
        } else {

            updateUI( result.data );
        }
    }

    @UiThread
    void updateUI(ArrayList<CampaignResult.Data> data) {

        BSProgressHUD.dismissProgressHUD();

        try {

            if(data == null) {

                Log.d("test", "Error...");
                return;
            }

            if(data.size() <= 0) {

                Log.d("test", "no more content.");
                page = -1;
                return;
            }

            // Safety doing on the UiThread here
            for(CampaignResult.Data item : data) {

                mAdapter.addItem( item );
            }

            // Notify
            mAdapter.notifyDataSetChanged();

        } catch (Exception e) {
            Log.d("test", "LuckyListActivity got me");
            // TODO: alert dialog here
        }
    }

    @StringRes(R.string.label_lucky_list)
    String luckyTitle;

    @StringRes(R.string.lucky_list_check)
    String showLuckyName;

    private void setupActionBar() {

        ActionBar actionBar = getActionBar();
        if(actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle( luckyTitle );
        }
    }

    @OptionsItem(android.R.id.home)
    void didClickedHomeMenu() {
        try {
            NavUtils.navigateUpFromSameTask(this);
        } catch (Exception e) {
            finish();
        }
    }

    /**
     *  ----------------------------------------------------------
     *
     *      Nested adapter class
     *
     *  ----------------------------------------------------------
     *
     */
    public class LuckyAdapter extends BaseAdapter {

        Context mContext;
        LayoutInflater mInflater;
        ArrayList<CampaignResult.Data> mData;
        MyOnClickListener myOnClickListener;

        public LuckyAdapter(Context mContext) {
            this.mContext = mContext;
            this.mInflater = ((Activity) mContext).getLayoutInflater();
            this.mData = new ArrayList<CampaignResult.Data>();
            this.myOnClickListener = new MyOnClickListener(mContext);
        }

        public void addItem(CampaignResult.Data item) {
            this.mData.add( item );
        }

        @Override
        public int getCount() {
            return mData.size();
        }

        @Override
        public CampaignResult.Data getItem(int position) {
            return mData.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View view, ViewGroup parent) {
            LuckyHolder holder;

            if(view == null) {
                view = mInflater.inflate(R.layout.cell_lucky_report, null);

                assert view != null;

                holder = new LuckyHolder();
                holder.image = (ImageView) view.findViewById(R.id.cell_lucky_list_icon);
                holder.text1 = (TextView) view.findViewById(R.id.cell_lucky_title);
                holder.text2 = (TextView) view.findViewById(R.id.cell_lucky_subtitle);
                holder.button = (Button) view.findViewById(R.id.cell_lucky_btn);

                view.setTag( holder );
            } else {

                holder = (LuckyHolder) view.getTag();
            }

            // Update content
            CampaignResult.Data item = mData.get( position );

            try {
                Context context = view.getContext();

                Log.d("test", "context : " + context + ", icon url : " + item.campaignIconURL);

                String path = item.campaignIconURL + "";
                if(!path.startsWith("http://") && !path.startsWith("https://")) {
                    path = "http://" + path;
                }

                // Rescale imageview (if needed)
                int width = holder.button.getWidth();
                int height = holder.button.getHeight();
                Picasso.with( mContext ).load( path ).resize(width, height).into( holder.image );

            } catch (Exception e) {
                e.printStackTrace();
            }

            holder.text1.setText( item.title );
            holder.text2.setText( ((LuckyListActivity) mContext).showLuckyName );
            holder.button.setTag(position);
            holder.button.setOnClickListener(myOnClickListener);

            return view;
        }

        public class MyOnClickListener implements View.OnClickListener {

            Context context;

            public MyOnClickListener(Context context) {
                this.context = context;
            }

            @Override
            public void onClick(View v) {
                try {

                    int pos = (Integer) v.getTag();
                    CampaignResult.Data item = mData.get( pos );

                    String url = item.url + "";

                    if(!url.startsWith("http://") && !url.startsWith("https://")) {
                        url = url + "http://";
                    }

                    Map<String, String> param = new HashMap<String, String>();
                    param.put("screenName", MyAccount.getInstance().getAccountScreenName());
                    param.put("campaignID", item.id + "");
                    param.put("campaignTitle", item.title);
                    FlurryAgent.logEvent("View LuckyMan's Info", param);

                    BSDialogIntentBuilder
                            .newInstance(context)
                            .openWebIntent(url);
                } catch (Exception e) {

                    e.printStackTrace();
                }
            }
        }
    }

    public static class LuckyHolder {
        ImageView image;
        TextView text1, text2;
        Button button;
    }
}
