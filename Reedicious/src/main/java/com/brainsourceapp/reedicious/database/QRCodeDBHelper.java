package com.brainsourceapp.reedicious.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.brainsourceapp.reedicious.database.data.MyQRItem;

import java.util.ArrayList;
import java.util.Collections;

public class QRCodeDBHelper {

    private SQLiteDatabase database;
    private MySQLiteHelper dbHelper;

    Context mContext;

    public QRCodeDBHelper(Context context) {
        this.mContext = context;
        dbHelper = MySQLiteHelper.getInstance(context);
    }

    public void open() {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    public void addQRCodeResponse(MyQRItem item,  String memberID) {

        // To Do

        if(dbHelper == null || dbHelper.getReadableDatabase() == null) {
            dbHelper = MySQLiteHelper.getInstance( mContext );
        }

        database = dbHelper.getWritableDatabase();

        item.setMemberID(memberID);

        ContentValues values = item.toContentValues();

        database.insert(MySQLiteHelper.TABLE_QRCODE, null, values);

        Log.d("test", "member : " + item.getMemberID() + " just saved the QRResponse : [ " + item.getResponseMsg() + " ].");
    }

    /**
     *
     * @param memberID String (null or empty mean getAll())
     * @return list
     */
    public ArrayList<MyQRItem> getMyQRItems(String memberID) {
        String queryString = "SELECT * FROM " + MySQLiteHelper.TABLE_QRCODE;

        if(memberID != null && memberID.trim().length() > 0) {
            queryString += " WHERE " + MySQLiteHelper.QR_MEMBER_ID + " LIKE " + memberID;
        }

        ArrayList<MyQRItem> list = new ArrayList<MyQRItem>();
        try {
            if(dbHelper.getReadableDatabase() == null) {
                return list;
            }

            Cursor cursor = dbHelper.getReadableDatabase().rawQuery(queryString, null);

            cursor.moveToFirst();
            while(!cursor.isAfterLast()) {
                MyQRItem item = new MyQRItem(cursor);
                list.add(item);
                cursor.moveToNext();
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return list;
    }

    public ArrayList<MyQRItem> getMyIndexedQRItems(String memberID) {
        String queryString = "SELECT * FROM " + MySQLiteHelper.TABLE_QRCODE;

        if(memberID != null && memberID.trim().length() > 0) {
            queryString += " WHERE " + MySQLiteHelper.QR_MEMBER_ID + " LIKE " + memberID;
        }

        ArrayList<MyQRItem> list = new ArrayList<MyQRItem>();
        if(dbHelper.getReadableDatabase() == null) {
            return list;
        }

        Cursor cursor = dbHelper.getReadableDatabase().rawQuery(queryString, null);

        try {
            long timestamp = -1;
            cursor.moveToFirst();
            while(!cursor.isAfterLast()) {
                MyQRItem item = new MyQRItem(cursor);

                if(timestamp == -1) {
                    timestamp = item.getTimestamp();
                } else {
                    if(Math.abs(timestamp - item.getTimestamp()) >=  24 * 3600) {
                        list.add(MyQRItem.newHeaderDate(timestamp));
                        timestamp = item.getTimestamp();
                    }
                }
                list.add(item);
                cursor.moveToNext();
            }
            /** Finally add the latest date Header*/
            if(timestamp != -1) {
                list.add(MyQRItem.newHeaderDate(timestamp));
            }

        } catch (Exception e) {

            e.printStackTrace();
        } finally {

            if(!cursor.isClosed()) {
                cursor.close();
            }
        }

        /** The magic is reverse here ! to get the nice list */
        Collections.reverse(list);

        return list;
    }
}
