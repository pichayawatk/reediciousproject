package com.brainsourceapp.reedicious.beans;

import org.androidannotations.annotations.EBean;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

@EBean
public class MyHelperBean {

    public MyHelperBean() {

    }

    public String timeStampToDateString(int epochInt) {

        java.util.Date date = new Date((long)epochInt * 1000);
        DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
        String dateStr = df.format( date );

        String arrStr[] = dateStr.split("/");

        return trimDay(arrStr[1]) + " " + castToThaiMonth(arrStr[0]) + " " + castToThaiYear(arrStr[2]);

    }

    private String trimDay(String dStr) {
        int dInt = Integer.parseInt(dStr);
        return dInt + "";
    }

    private String castToThaiMonth(String mStr) {
        // {{
        int mInt = Integer.valueOf(mStr);
        String ret = mStr;
        switch(mInt) {
            case 1 : {
                ret = "มกราคม";
                break;
            }
            case 2 : {
                ret = "กุมภาพันธ์";
                break;
            }
            case 3 : {
                ret = "มีนาคม";
                break;
            }
            case 4 : {
                ret = "เมษายน";
                break;
            }
            case 5 : {
                ret = "พฤษภาคม";
                break;
            }
            case 6 : {
                ret = "มิถุนายน";
                break;
            }
            case 7 : {
                ret = "กรกฎาคม";
                break;
            }
            case 8 : {
                ret = "สิงหาคม";
                break;
            }
            case 9 : {
                ret = "กันยายน";
                break;
            }
            case 10 : {
                ret = "ตุลาคม";
                break;
            }
            case 11 : {
                ret = "พฤศจิกายน";
                break;
            }
            case 12 : {
                ret = "ธันวาคม";
                break;
            }
        }

        return ret;
    } // }}

    private String castToThaiYear(String yStr) {
        int yInt = Integer.parseInt(yStr);
        yInt += 543;
        return yInt + "";
    }

}
