package com.brainsourceapp.reedicious.views;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.AttributeSet;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.brainsourceapp.reedicious.R;
import com.brainsourceapp.reedicious.SettingsActivity;
import com.brainsourceapp.reedicious.beans.MyAccount;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.res.StringRes;

@EViewGroup(R.layout.social_mapping)
public class FacebookMapperView extends LinearLayout {

    Context mContext;

    public FacebookMapperView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;
    }

    @ViewById(R.id.social_mapper_image)
    ImageView thumbImage;

    @ViewById(R.id.social_mapper_title)
    RDTextView email;

    @ViewById(R.id.social_mapper_status_btn)
    public Button statusBtn;

    @StringRes(R.string.error_code_601_a)
    String unmapSocial_601a;

    @StringRes(R.string.error_code_601_b)
    String unmapSocial_601b;

    @Click(R.id.social_mapper_status_btn)
    public void onMapperBtnClicked() {
        String msg;
        boolean mapped = MyAccount.getInstance().isFacebookStatusMapped();
        if(MyAccount.getInstance().isFacebookStatusMapped()) {
            msg = unmapSocial_601a + " Facebook " + MyAccount.getInstance().getAccountScreenName() + " " + unmapSocial_601b;
            ToggleStatusDialog.newInstance(msg, !mapped).show(((Activity) mContext).getFragmentManager(), "alert");
        } else {

            assert getContext() != null;
            ((SettingsActivity) getContext()).onMappingStatusChanged(true);
        }
    }

    @AfterViews
    public void afterMappingView() {
        thumbImage.setImageResource(R.drawable.profile_face);
    }

    public void updateEmail(String emailString) {
        email.setText(emailString);
    }
    public void updateButton(String buttonString) {
        statusBtn.setText(buttonString);
    }

    public static class ToggleStatusDialog extends DialogFragment {

        public interface SocialMappingStatusListener {
            public void onMappingStatusChanged(boolean willMap);
        }

        public static ToggleStatusDialog newInstance(String msg, boolean willMap) {
            ToggleStatusDialog fragment = new ToggleStatusDialog();
            Bundle bundle = new Bundle();
            bundle.putString("msg", msg);
            bundle.putBoolean("willMap", willMap);
            fragment.setArguments(bundle);
            return fragment;
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            assert getActivity() != null && getArguments() != null;
            return new AlertDialog.Builder(getActivity())
                    .setTitle(getActivity().getResources().getString(R.string.app_name))
                    .setMessage(getArguments().getString("msg"))
                    .setPositiveButton(R.string.btn_ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    ((SocialMappingStatusListener) getActivity()).onMappingStatusChanged(getArguments().getBoolean("willMap"));
                                }
                            });
                        }
                    })
                    .setNegativeButton(R.string.btn_cancel, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    }).create();
        }
    }
}
