package com.brainsourceapp.reedicious.rest.results;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import org.json.JSONException;
import org.json.JSONObject;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MyPromotion {

    public int id;

    public String title;

    public String desc;

    public int date;

    public String cover;

    public String relationship;

    public int videoid;

    public String promotion_link;

    public double latitude;

    public double longitude;

    public String distance;

    public MyPromotion() {

    }

    public static MyPromotion fromJSON(String str) {

        try {
            JSONObject jsonObject = new JSONObject(str);
            MyPromotion promotion = new MyPromotion();

            if(jsonObject.has("id"))                    promotion.id = jsonObject.getInt("id");
            if(jsonObject.has("title"))                 promotion.title = jsonObject.getString("title");
            if(jsonObject.has("desc"))                  promotion.desc = jsonObject.getString("desc");
            if(jsonObject.has("cover"))                 promotion.cover = jsonObject.getString("cover");
            if(jsonObject.has("relationship"))          promotion.relationship = jsonObject.getString("relationship");
            if(jsonObject.has("videoid"))               promotion.videoid = jsonObject.getInt("videoid");
            if(jsonObject.has("promotion_link"))        promotion.promotion_link = jsonObject.getString("promotion_link");
            if(jsonObject.has("latitude"))              promotion.latitude = jsonObject.getDouble("latitude");
            if(jsonObject.has("longitude"))             promotion.longitude = jsonObject.getDouble("longitude");
            if(jsonObject.has("distance"))              promotion.distance = jsonObject.getString("distance");

            return promotion;

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }
}
