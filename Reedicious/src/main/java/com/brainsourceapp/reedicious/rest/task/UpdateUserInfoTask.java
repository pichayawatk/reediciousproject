package com.brainsourceapp.reedicious.rest.task;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.util.Log;

import com.brainsourceapp.reedicious.beans.MyAccount;
import com.brainsourceapp.reedicious.rest.response.MemberUpdateResponse;
import com.brainsourceapp.reedicious.rest.results.Header;
import com.brainsourceapp.reedicious.rest.utils.BSHttpUtils;
import com.cloudinary.Cloudinary;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UpdateUserInfoTask extends AsyncTask<String, Void, String> {

    public interface UpdateUserCallback {
        void onFinishedUpdateUserInfoTask(boolean success, String imageURL);
    }

    UpdateUserCallback callback;
    Context context;
    String userAccessToken;
    String apiToken;
    String memberID;
    String email;
    String password;
    String screenname;
    String facebookID;
    String facebookToken;
    String facebookStatus;
    String avatar;
    Bitmap bitmap;

    Map<String, String> config;

    public UpdateUserInfoTask(Context context,
                              String apiToken, String userAccessToken,
                              String memberID, String email, String password, String screenname, String facebookID, String facebookToken, String facebookStatus, String avatar, Bitmap bitmap,
                              UpdateUserCallback callback) {

        this.context = context;
        this.userAccessToken = userAccessToken;
        this.apiToken = apiToken;
        this.memberID = memberID;
        this.email = email;
        this.password = password;
        this.screenname = screenname;
        this.facebookID = facebookID;
        this.facebookStatus = facebookStatus;
        this.facebookToken = facebookToken;
        this.avatar = avatar;
        this.bitmap = bitmap;
        this.callback = callback;

        this.config = new HashMap<String, String>();
    }

    @Override
    protected String doInBackground(String... params) {

        /**
         *
         *  Upload bitmap to cloudinary and get the imageURL
         *  and update the url to the database...
         *
         */
        String uploadedURL = null;
        if(bitmap != null) {
            uploadedURL = uploadBitmapToCloudinary();
        }

        if(uploadedURL != null && uploadedURL.trim().length() > 0) {
            avatar = uploadedURL;
        }

        /** Update user info */
        String baseURL = "http://api.reedicious.com/rest/member/update";
        String[] urlParams = new String[] {
                "appid", "1",
                "token", apiToken
        };

        String url = BSHttpUtils.newInstance().getfullURL(baseURL, urlParams);

        HttpClient client = new DefaultHttpClient();
        HttpPut put = new HttpPut(url);

        // Set Header
        put.setHeader("accesstoken", userAccessToken);

        // Get put params
        List<NameValuePair> pairs = new ArrayList<NameValuePair>();
        //if(email != null && email.trim().length() > 0)                      pairs.add(new BasicNameValuePair("email", email));
        //if(password != null && password.trim().length() > 0)                pairs.add(new BasicNameValuePair("password", password));
        if(screenname != null && screenname.trim().length() > 0)            pairs.add(new BasicNameValuePair("screenname", screenname));
        if(avatar != null && avatar.trim().length() > 0)                    pairs.add(new BasicNameValuePair("avatar", avatar));
        if(facebookID != null && facebookID.trim().length() > 0)            pairs.add(new BasicNameValuePair("fb_id", facebookID));
        if(facebookToken != null && facebookToken.trim().length() > 0)      pairs.add(new BasicNameValuePair("fb_token", facebookToken));
        if(facebookStatus != null && facebookStatus.trim().length() > 0)    pairs.add(new BasicNameValuePair("fb_status", facebookStatus));

        // Get response
        try {
            put.setEntity(new UrlEncodedFormEntity(pairs, "UTF-8"));
            HttpResponse response = client.execute(put);

            int statusCode = response.getStatusLine().getStatusCode();
            Log.d("test", "response status code : " + statusCode);

            HttpEntity responseEntity = response.getEntity();
            String responseString = BSHttpUtils.newInstance().streamToString( responseEntity.getContent() );

            Log.d("test", "string response : " + responseString);

            MemberUpdateResponse ret = new MemberUpdateResponse();

            JSONObject jsonObject = new JSONObject(responseString);
            if(!jsonObject.isNull("header")) {
                JSONObject header = jsonObject.getJSONObject("header");
                ret.header = new Header();
                if(!header.isNull("code"))              ret.header.code = header.getInt("code");
                if(!header.isNull("message"))           ret.header.message = header.getString("message");
            }

            if(!jsonObject.isNull("data")) {
                JSONObject data = jsonObject.getJSONObject("data");
                ret.data = new MemberUpdateResponse.Data();
                if(!data.isNull("accesstoken"))         ret.data.accesstoken = data.getString("accesstoken");
            }

            return uploadedURL;

        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return uploadedURL;
    }

    protected synchronized String uploadBitmapToCloudinary() {
        Log.d("test", "start upload image to cloudinary");
        // Image title
        String imageTitle = "" + (int)System.currentTimeMillis();
        String imageDesc = "Reedicious_Profile_Image_" + (int)System.currentTimeMillis();

        // Create url string from bitmap
        String imageUrl = MediaStore.Images.Media.insertImage(context.getContentResolver(), bitmap, imageTitle, imageDesc);

        if(imageUrl == null || imageUrl.length() == 0) {
            return null;
        }

        // Cloudinary configuration parameters
        String cloud_name = "brainsource";
        String api_key = "987674942549526";
        String api_secret = "O2qkWJMaU6UDYmuv9T7D85Z3joE";

        config.put("cloud_name", cloud_name);
        config.put("api_key", api_key);
        config.put("api_secret", api_secret);
        Cloudinary mobileCloudinary = new Cloudinary(config);

        // Bitmap to InputStream
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bos);
        byte[] bitmapdata = bos.toByteArray();
        ByteArrayInputStream bs = new ByteArrayInputStream(bitmapdata);

        try {
            JSONObject uploadResult = mobileCloudinary.uploader().upload(bs, Cloudinary.asMap(
                    "public_id", memberID
            ));

            Log.d("test", "upload result : " + uploadResult);

            String fullpath = uploadResult.getString("url");
            Log.d("test", "expected imageURL : " + fullpath);

            /** Save data after updated */
            MyAccount.getInstance().setAccountAvatar(fullpath);
            MyAccount.getInstance().setAccountScreenName(screenname);

            return fullpath;

        } catch (Exception e) {
            e.printStackTrace();

            Log.d("test", "error when upload bitmap to cloudinary : " + e.getLocalizedMessage());

            return null;
        }
    }

    @Override
    protected void onPostExecute(String imageURL) {
        super.onPostExecute(imageURL);

        boolean success = true;
        if(imageURL == null || imageURL.length() == 0) {
            Log.d("test", "error update user info, null response");
            success = false;
        }

        if(callback != null) {
            Log.d("test", "updated image URL : " + imageURL);
            callback.onFinishedUpdateUserInfoTask(success, imageURL);
        }
    }

}
