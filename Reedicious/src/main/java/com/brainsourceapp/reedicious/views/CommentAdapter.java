package com.brainsourceapp.reedicious.views;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.brainsourceapp.reedicious.rest.results.Commentator;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

import java.util.ArrayList;
import java.util.List;

@EBean
public class CommentAdapter extends BaseAdapter {

    public interface Callback {
        void OnLastViewShowedListener();
    }

    Callback mCallback;

    List<Commentator> commentators;

    int lastestPos;

    @RootContext
    Context context;

    @AfterInject
    void initAdapter() {
        try {
            mCallback = (Callback) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement CommentAdapter Callback Interface");
        }
        commentators = new ArrayList<Commentator>();
        lastestPos = 0;
    }

    public void addItem(Commentator item) {
        commentators.add( item );
    }

    public void clearData() {
        if(commentators.size() > 0) {
            commentators.clear();
        }
        if(commentators == null) {
            commentators = new ArrayList<Commentator>();
        }
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return commentators.size();
    }

    @Override
    public Commentator getItem(int position) {
        return commentators.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        // Callback
        if(position == commentators.size() - 1) {
            mCallback.OnLastViewShowedListener();
        }

        CommentatorItemView commentatorItemView;
        if(view == null) {
            commentatorItemView = CommentatorItemView_.build(context);
        } else {
            commentatorItemView = (CommentatorItemView) view;
        }

        commentatorItemView.bind( getItem(position) );

        // Animation
        /** Disable from p'Tong task, Jan 13, 2014 */
        /*
        Animation animation = null;

        if(lastestPos < position) {
            animation = AnimationUtils.loadAnimation(context, R.anim.push_up_in);
        }
        else if(lastestPos > position) {
            animation = AnimationUtils.loadAnimation(context, R.anim.push_down_in);
        }
        else {
            animation = AnimationUtils.loadAnimation(context, R.anim.fade_in);
        }
        lastestPos = position;


        if(animation != null) {
            commentatorItemView.startAnimation( animation );
            // TODO: should handle this by ourself or let the default gc handle.
            // animation = null;
        }
        */

        return commentatorItemView;
    }
}
