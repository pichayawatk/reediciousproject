package com.brainsourceapp.reedicious;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;

public class YoutubePlayerActivity extends YouTubeBaseActivity implements YouTubePlayer.OnInitializedListener {

    private static final String DEVELOPER_KEY = "AIzaSyAFg_mUft_IVFrT7zUnVRm3wEn-k-G0l5I";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_youtube_player);

        YouTubePlayerView youTubePlayer = (YouTubePlayerView) findViewById(R.id.youtube_video_view);
        youTubePlayer.initialize(DEVELOPER_KEY, this);
    }

    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {
        Intent intent = getIntent();

        if(intent != null) {

            String videoID = intent.getStringExtra("videoID");
            youTubePlayer.loadVideo( videoID );
        }
    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {
        Toast.makeText(this, "Oh no : " + youTubeInitializationResult.toString(), Toast.LENGTH_SHORT).show();
    }
}
