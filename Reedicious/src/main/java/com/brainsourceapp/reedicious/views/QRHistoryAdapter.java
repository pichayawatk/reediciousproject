package com.brainsourceapp.reedicious.views;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.brainsourceapp.reedicious.database.data.MyQRItem;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

import java.util.ArrayList;

@EBean
public class QRHistoryAdapter extends BaseAdapter {

    ArrayList<MyQRItem> mQRItems;

    @RootContext
    Context context;

    @AfterInject
    void initAdapter() {
        mQRItems = new ArrayList<MyQRItem>();
    }

    public void addItems(ArrayList<MyQRItem> items) {
        if(mQRItems == null) {
            mQRItems = new ArrayList<MyQRItem>();
        }
        mQRItems.addAll(items);
    }

    @Override
    public int getCount() {
        return mQRItems.size();
    }

    @Override
    public MyQRItem getItem(int position) {
        return mQRItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        QRHistoryItemView qritemView;
        if(convertView == null) {
            qritemView = QRHistoryItemView_.build(context);
        } else {
            qritemView = (QRHistoryItemView) convertView;
        }

        MyQRItem item = mQRItems.get(position);
        if(item.isHeaderCell()) {
            qritemView.bindHeader(item);
        } else {
            qritemView.bindData(item);
        }

        return qritemView;
    }
}
