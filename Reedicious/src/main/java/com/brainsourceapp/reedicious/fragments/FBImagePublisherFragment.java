package com.brainsourceapp.reedicious.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.util.LruCache;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.Volley;
import com.brainsourceapp.reedicious.R;

public class FBImagePublisherFragment extends Fragment implements View.OnClickListener {

    public interface FBImagePublishFragmentCallback {
        void willPublishImageToFacebook(boolean canShare,
                                   Bitmap bitmap,
                                   String description,
                                   String linkedURL);
    }

    FBImagePublishFragmentCallback mCallback;

    RequestQueue mRequestQueue;
    ImageLoader mImageLoader;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {
            mCallback = (FBImagePublishFragmentCallback) activity;

        } catch (ClassCastException e) {
            throw new ClassCastException( activity.getLocalClassName() + " must implement callback interfaces." );
        }
    }

    public static FBImagePublisherFragment newInstance(String imageURL,
                                                     String commentText,
                                                     String placeDescription,
                                                     String linkedURL,
                                                     String mPlaceTitle) {
        FBImagePublisherFragment fragment = new FBImagePublisherFragment();
        Bundle bundle = new Bundle();
        bundle.putString("imageURL", imageURL);
        bundle.putString("commentText", commentText);
        bundle.putString("placeDescription", placeDescription);
        bundle.putString("linkedURL", linkedURL);
        bundle.putString("mPlaceTitle", mPlaceTitle);

        fragment.setArguments(bundle);

        return fragment;
    }

    String imageURL;
    String commentText;
    String placeDescription;
    String linkedURL;
    String mPlaceTitle;

    EditText commentView;
    NetworkImageView publishedImage;
    Button okBtn;
    Button cancelBtn;

    Bitmap mBitmap;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mRequestQueue = Volley.newRequestQueue(getActivity());
        mImageLoader = new ImageLoader(mRequestQueue, new ImageLoader.ImageCache() {
            private final LruCache<String, Bitmap> mCache = new LruCache<String, Bitmap>(10);
            @Override
            public Bitmap getBitmap(String s) {
                return mCache.get(s);
            }

            @Override
            public void putBitmap(String s, Bitmap bitmap) {
                mBitmap = Bitmap.createBitmap( bitmap );
                mCache.put(s, bitmap);
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.publish_image_fb_fragment, container, false);
        assert getActivity() != null && getArguments() != null && view != null;

        // Try to get from saveInstanceState
        Bundle bundle = getArguments();
        if(bundle != null) {
            Log.d("test", "savedInstanceState");
            if(bundle.containsKey("imageURL"))              imageURL = bundle.getString("imageURL", "");
            if(bundle.containsKey("commentText"))           commentText = bundle.getString("commentText", "");
            if(bundle.containsKey("placeDescription"))      placeDescription = bundle.getString("placeDescription", "");
            if(bundle.containsKey("linkedURL"))             linkedURL = bundle.getString("linkedURL", "");
            if(bundle.containsKey("mPlaceTitle"))           mPlaceTitle = bundle.getString("mPlaceTitle", "");
        }

        Log.d("test", "published image url : " + imageURL);
        // View injection
        commentView = (EditText) view.findViewById(R.id.fb_image_dialog_comment_view);
        publishedImage = (NetworkImageView) view.findViewById(R.id.fb_image_dialog_image_view);
        okBtn = (Button) view.findViewById(R.id.fb_image_dialog_ok_btn);
        cancelBtn = (Button) view.findViewById(R.id.fb_image_dialog_cancel_btn);

        // Load Image
        publishedImage.setImageUrl(imageURL, mImageLoader);

        // Set initial information (comment)
        String comment = String.format("\n%s", placeDescription);
        commentView.setText( comment );

        // Listener
        okBtn.setOnClickListener(this);
        cancelBtn.setOnClickListener(this);

        return view;
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();

        assert commentView.getEditableText() != null;
        String description = commentView.getEditableText().toString();

        if(description == null) {
            description = "";
        }

        assert getActivity() != null;
        switch (id) {
            case R.id.fb_image_dialog_ok_btn:

                ((FBImagePublishFragmentCallback) getActivity()).
                        willPublishImageToFacebook(true, mBitmap, description, linkedURL);
                break;

            case R.id.fb_image_dialog_cancel_btn:
                ((FBImagePublishFragmentCallback) getActivity())
                        .willPublishImageToFacebook(false, null, description, linkedURL);
                break;

            default:
                break;
        }
    }
}
