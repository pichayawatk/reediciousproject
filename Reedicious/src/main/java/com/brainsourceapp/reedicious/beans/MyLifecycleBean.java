package com.brainsourceapp.reedicious.beans;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Bundle;

import com.brainsourceapp.reedicious.R;
import com.flurry.android.FlurryAgent;

public class MyLifecycleBean implements Application.ActivityLifecycleCallbacks {

    /**
     * Flurry embed here
     */

    private static int resumed;
    private static int stopped;

    public static boolean isApplicationInForeground() {
        return resumed > stopped;
    }

    public static boolean isApplicationInBackground() {
        return resumed == stopped;
    }

    public interface LifeBeanCallback {

        void onApplicationMovedToBackground(Activity activity);

        void onApplicationMovedToForeground(Activity activity);
    }

    LifeBeanCallback mCallback;
    private String flurryApiKey;
    public String getFlurryApiKey(Context context) {
        if(flurryApiKey == null) {
            flurryApiKey = context.getResources().getString(R.string.flurry_api_key);
        }
        return flurryApiKey;
    }

    public void setmCallback(MyRestBean bean) {

        try {
            mCallback = (LifeBeanCallback) bean;
        } catch (ClassCastException e) {
            throw new ClassCastException(bean.getClass() + " must implement interface");
        }
    }

    @Override
    public void onActivityCreated(Activity activity, Bundle savedInstanceState) {

    }

    @Override
    public void onActivityStarted(Activity activity) {
        FlurryAgent.onStartSession(activity, getFlurryApiKey(activity));
    }

    @Override
    public void onActivityResumed(Activity activity) {

            ++resumed;
            if( isApplicationInForeground() ) {

                if(mCallback instanceof MyRestBean) {

                    mCallback.onApplicationMovedToForeground( activity );
                }
            }

    }

    @Override
    public void onActivityPaused(Activity activity) {

    }

    @Override
    public void onActivityStopped(Activity activity) {
        FlurryAgent.onEndSession(activity);

        ++stopped;
        if( isApplicationInBackground() ) {
            // Cancel all background tasks created from aa when application is determined in background.
            if(mCallback instanceof MyRestBean) {

                mCallback.onApplicationMovedToBackground( activity );
            }

            // Save user information when application will go to background state.
            MyAccount.getInstance().saveInSharedPreferences(activity);
        }
    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle outState) {

    }

    @Override
    public void onActivityDestroyed(Activity activity) {

    }
}
