package com.brainsourceapp.reedicious.rest.results;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.ArrayList;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PlaceComment {

    public Header header;

    public Properties properties;

    public ArrayList<Commentator> data;

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class Properties {

        public int total_page;

        public int row_count;

        public int current_page;

        public int limit_per_page;
    }

}
