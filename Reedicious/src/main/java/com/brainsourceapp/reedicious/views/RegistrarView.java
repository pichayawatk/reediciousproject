package com.brainsourceapp.reedicious.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;

import com.brainsourceapp.reedicious.R;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EViewGroup;

@EViewGroup(R.layout.registrar_view)
public class RegistrarView extends LinearLayout {

    private static final String TAG = RegistrarView.class.getSimpleName();

    public interface RegistrarViewListener {
        void didClickRegistrarButton(View view);
    }

    protected RegistrarViewListener callback;

    @Click(R.id.registrar_view_registbtn)
    protected void onRegistrarClicked(View view) {
        if(callback != null)
            callback.didClickRegistrarButton(view);
    }

    public RegistrarView(Context context, AttributeSet attrs) {
        super(context,attrs);
        if(!isInEditMode()) {
            try {
                callback = (RegistrarViewListener)context;
            } catch (ClassCastException e) {
                throw new ClassCastException(TAG + ": must implement RegistrarViewInterface");
            }
        }
     }

    public RegistrarView(Context context, AttributeSet attrs, int style) {
        super(context, attrs, style);
        if(!isInEditMode()) {
            try {
                callback = (RegistrarViewListener)context;
            } catch (ClassCastException e) {
                throw new ClassCastException(TAG + ": must implement RegistrarViewInterface");
            }
        }
    }

}
