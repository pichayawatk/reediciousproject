package com.brainsourceapp.reedicious.rest.task;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.brainsourceapp.reedicious.rest.results.MyPromotion;
import com.brainsourceapp.reedicious.rest.utils.BSHttpUtils;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.IOException;
import java.util.ArrayList;

public class GetPromotionsTask extends AsyncTask <String, String, ArrayList<MyPromotion>> {

    public interface PromotionsCallback {
        void onSuccessGetPromotions(ArrayList<MyPromotion> promotions);
    }

    ProgressDialog pg;
    PromotionsCallback callback;

    public GetPromotionsTask(Context context, PromotionsCallback callback) {
        pg = new ProgressDialog(context);
        this.callback = callback;
    }

    @Override
    protected ArrayList<MyPromotion> doInBackground(String... params) {
        String fullURL = BSHttpUtils.newInstance().getfullURL("http://api.reedicious.com/", params);

        HttpClient httpClient = new DefaultHttpClient();
        HttpGet get = new HttpGet(fullURL);

        try {
            HttpResponse response = httpClient.execute(get);

            // Check status code
            int statusCode = response.getStatusLine().getStatusCode();
            Log.d("test", "response status code : " + statusCode);

            // Get entity
            HttpEntity entity = response.getEntity();

            if(entity != null) {
                String ret = BSHttpUtils.newInstance().streamToString( entity.getContent() );

                ArrayList<MyPromotion> promotions = new ArrayList<MyPromotion>();
                if(ret.length() > 0) {
                    MyPromotion item = MyPromotion.fromJSON( ret );
                    if(item != null) {
                        promotions.add( item );
                    }
                }

                return promotions;
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onPostExecute(ArrayList<MyPromotion> myPromotions) {
        super.onPostExecute(myPromotions);

        if(myPromotions == null) {
            myPromotions = new ArrayList<MyPromotion>();
        }

        this.callback.onSuccessGetPromotions( myPromotions );
    }
}
