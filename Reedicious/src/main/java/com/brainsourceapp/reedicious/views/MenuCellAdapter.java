package com.brainsourceapp.reedicious.views;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.brainsourceapp.reedicious.rest.results.MenuResult;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

import java.util.ArrayList;

@EBean
public class MenuCellAdapter extends BaseAdapter {

    public interface Callback {
        public void OnLastItemAppeared();
    }

    Callback mCallback;

    ArrayList<MenuResult.Data> mData;

    int last_pos;

    @RootContext
    Context mContext;

    @AfterInject
    void initAdapter() {
        try {
            mCallback = (Callback) mContext;
        } catch (ClassCastException e) {
            throw new ClassCastException(mContext.toString() + " must implement MenuCellAdapter.Callback interface");
        }
        mData = new ArrayList<MenuResult.Data>();
        last_pos = 0;
    }

    public void addItem(MenuResult.Data item) {
        mData.add(item);
    }

    public ArrayList<MenuResult.Data> getAllDatas() {
        ArrayList<MenuResult.Data> result = new ArrayList<MenuResult.Data>();
        result.addAll(this.mData);
        return result;
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public MenuResult.Data getItem(int position) {
        return mData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {

        // Callback
        if(position == mData.size() - 1) {
            mCallback.OnLastItemAppeared();
        }

        MenuCell menuCell;
        if(view == null) {
            menuCell = MenuCell_.build(mContext);
        } else {
            menuCell = (MenuCell) view;
        }

        menuCell.bind( mData.get( position ) );

        // Animation
        //Animation anim = AnimationUtils.loadAnimation(mContext, R.anim.fade_in);
        //menuCell.startAnimation(anim);

        last_pos = position;

        return menuCell;
    }
}
