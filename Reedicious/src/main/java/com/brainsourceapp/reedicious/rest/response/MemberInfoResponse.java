package com.brainsourceapp.reedicious.rest.response;

import com.brainsourceapp.reedicious.rest.results.Header;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MemberInfoResponse {

    public Header header;

    public MemberInfoResponseData data;

    public MemberInfoResponse() {
        header = new Header();
        data = new MemberInfoResponseData();
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class MemberInfoResponseData {

        public String member_id;
        public String screen_name;
        public String email;
        public String fb_id;
        public String fb_token;
        public String fb_status;
        public String instagram_status;
        public String avatar;
        public String accesstoken;

        public MemberInfoResponseData() {}
    }
}
