package com.brainsourceapp.reedicious;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Intent;
import android.content.IntentSender;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;

import com.brainsourceapp.reedicious.beans.MyAccount;
import com.brainsourceapp.reedicious.fragments.MyListFragment;
import com.brainsourceapp.reedicious.fragments.MyMapFragment;
import com.brainsourceapp.reedicious.rest.data.MyMinimart;
import com.brainsourceapp.reedicious.rest.data.RestaurantPageResponse;
import com.brainsourceapp.reedicious.rest.results.MyPlace;
import com.brainsourceapp.reedicious.rest.task.GetAllRestaurantsTask;
import com.brainsourceapp.reedicious.utils.BSAlertDialog;
import com.flurry.android.FlurryAgent;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.location.LocationClient;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.InstanceState;
import org.androidannotations.annotations.OptionsItem;
import org.androidannotations.annotations.OptionsMenu;
import org.androidannotations.annotations.OptionsMenuItem;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.List;

@EActivity(R.layout.activity_chick_restaurants)
@OptionsMenu(R.menu.chick_restaurants)
public class ChickRestaurantsActivity extends Activity
        implements GooglePlayServicesClient.ConnectionCallbacks,
                   GooglePlayServicesClient.OnConnectionFailedListener,
        MyListFragment.ListFragmentCallback
{
    private static final int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;

    static boolean isBackgroundTaskRunning;

    LocationClient mLocationClient;
    Location mLocation;
    boolean isViewInjected;

    @InstanceState
    int myPage;

    @InstanceState
    int maxPage;

    @InstanceState
    ArrayList<MyPlace> mPlaces;

    List<MyMinimart> mMarts;
    /*
    public void setmPlaces(ArrayList<MyPlace> places, boolean clear) {
        if(clear) {
            mPlaces.clear();
        }

        mPlaces.addAll(places);
    }
    */
    public ArrayList<MyPlace> getmPlaces() {

        ArrayList<MyPlace> list = new ArrayList<MyPlace>();
        for(MyPlace place : mPlaces) {
            list.add(place);
        }
        return list;
    }

    public ArrayList<MyMinimart> getmMinimarts() {

        ArrayList<MyMinimart> list = new ArrayList<MyMinimart>();
        for(MyMinimart minimart : mMarts) {
            list.add( minimart );
        }
        return list;
    }

    MyListFragment listFragment;
    MyMapFragment mapFragment;

    @ViewById(R.id.chick_res_list_holder)
    FrameLayout listView;

    @ViewById(R.id.chick_res_map_holder)
    FrameLayout mapView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mLocationClient = new LocationClient(this, this, this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mLocationClient.connect();
    }

    @Override
    protected void onStop() {
        mLocationClient.disconnect();
        super.onStop();
    }

    @Override
    protected void onResume() {
        super.onResume();

        boolean isGooglePlayServicesAvailable = servicesConnected();
        MyAccount.getInstance().setGooglePlayServicesAvailable(isGooglePlayServicesAvailable);
    }

    /**
     |--------------------------------------------------------
     | Option Menu Show Map
     |--------------------------------------------------------
     |
     |
     |
     */
    /*
    boolean isMapShowed() {
        return showmapMenuItem.isVisible();
    }
    */

    @OptionsMenuItem(R.id.action_show_map)
    MenuItem showmapMenuItem;

    @OptionsItem(R.id.action_show_map)
    void showmapOptionClicked() {

        if(MyAccount.getInstance().isGooglePlayServicesAvailable() 
               && MyAccount.getInstance().canGetGoogleLocation() ) {

            FlurryAgent.logEvent("View NearBy Restaurant Map");

            // hide menu
            showmapMenuItem.setVisible(false);

            mapView.setVisibility(View.VISIBLE);

            listView.setVisibility(View.INVISIBLE);

            addMapFragment();
        }
        /** Cannot show GoogleMap for some reason */
        else {


            BSAlertDialog.showEasyDialog(R.string.no_google_play_services).show(getFragmentManager(), "error");
        }
    }

    /**
     |--------------------------------------------------------
     |  After injected all views
     |--------------------------------------------------------
     |
     |
     |
     |
     |
     */
    @AfterViews
    void afterChickRestaurantsViews() {

        isViewInjected = true;

        isBackgroundTaskRunning = false;

        addListFragment();

        /** Add mapFragment on click icon */
        //addMapFragment();

        FlurryAgent.logEvent("View NearBy Restaurant List");

        if(myPage == 0) {
            myPage = 1;
            maxPage = 99;
        } else {
            Log.d("test", "resumed cache at current_page : " + myPage + ", max_page : " + maxPage);
        }

        listView.setVisibility(View.VISIBLE);

        mapView.setVisibility(View.INVISIBLE);

        setupActionBar();

        joinWork();
    }

    private void setupActionBar() {
        ActionBar actionBar = getActionBar();
        if(null != actionBar) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    private void addMapFragment() {
        if(null == mapFragment && getmPlaces() != null && !getmPlaces().isEmpty()) {

            mapFragment = MyMapFragment.newInstance( getmPlaces(), getmMinimarts(), mLocation);
            getFragmentManager().beginTransaction()
                    .add(R.id.chick_res_map_holder, mapFragment, "map")
                    .commit();
        }
    }

    private void addListFragment() {
        if(null == listFragment) {

            listFragment = MyListFragment.newInstance();
            getFragmentManager().beginTransaction()
                    .add( R.id.chick_res_list_holder, listFragment, "list")
                    .commit();
        }
    }

    @OptionsItem(android.R.id.home)
    void didClickedHomeMenu() {
        try {
            NavUtils.navigateUpFromSameTask(this);
        } catch (Exception e) {
            finish();
        }
    }

    /**
     |--------------------------------------------------------
     | Wrapped method for showing mapView and listView
     |--------------------------------------------------------
     |
     |
     |
     */

    /**
     |--------------------------------------------------------
     | On Back Press
     |--------------------------------------------------------
     |
     |
     |
     */
    @Override
    public void onBackPressed() {
        // Toggle back to list view if needed
        if(!showmapMenuItem.isVisible()) {

            showmapMenuItem.setVisible(true);

            mapView.setVisibility(View.INVISIBLE);

            listView.setVisibility(View.VISIBLE);
        } else {

            super.onBackPressed();
        }
    }

    /**
     |--------------------------------------------------------
     | GooglePlayServicesClient ConnectionCallbacks
     |--------------------------------------------------------
     |
     |
     |
     */
    @Override
    public void onConnected(Bundle bundle) {
        mLocation = mLocationClient.getLastLocation();

        joinWork();
    }

    @Override
    public void onDisconnected() {

    }

    /**
     |--------------------------------------------------------
     | GooglePlayServicesClient ConnectionFailed Listener
     |--------------------------------------------------------
     |
     |
     |
     */
    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

        if(connectionResult.hasResolution()) {

            try {
                connectionResult.startResolutionForResult(
                        this, CONNECTION_FAILURE_RESOLUTION_REQUEST
                );

                /**
                 *  Throw if Google Play services canceled the original Pending Intent !
                 *
                 */


            } catch (IntentSender.SendIntentException e) {
                e.printStackTrace();

                // Flag changed
                MyAccount.getInstance().setGooglePlayServicesAvailable(false);
            }
        } else {


            int errorCode = connectionResult.getErrorCode();

            Dialog errorDialog = GooglePlayServicesUtil.getErrorDialog(
                    errorCode,
                    this,
                    CONNECTION_FAILURE_RESOLUTION_REQUEST
            );

            if(errorDialog != null) {
                ErrorDialogFragment errorDialogFragment = new ErrorDialogFragment();

                errorDialogFragment.setDialog( errorDialog );

                errorDialogFragment.show(getFragmentManager(), "Geofence Detection");
            }
            // Flag changed
            MyAccount.getInstance().setGooglePlayServicesAvailable( false );
        }
    }


    /**
     |--------------------------------------------------------
     | Check GooglePlayServices is Available ?
     |--------------------------------------------------------
     |
     |
     |
     */
    private boolean servicesConnected() {

        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);

        if(ConnectionResult.SUCCESS == resultCode) {

            return true;
        } else {

            Log.d("test", "__GOOGLE PLAY SERVICES IS NOT AVAILABLE__");

            return false;
        }
    }

    // Error Dialog
    public static class ErrorDialogFragment extends DialogFragment {
        private Dialog mDialog;

        public ErrorDialogFragment() {
            super();
            mDialog = null;
        }

        public void setDialog(Dialog dialog) {
            mDialog = dialog;
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            return mDialog;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case CONNECTION_FAILURE_RESOLUTION_REQUEST:

                switch (resultCode) {

                    case Activity.RESULT_OK:

                        mLocationClient.connect();
                }
        }
    }

    /**
     |--------------------------------------------------------
     | Start the world HERE !
     |--------------------------------------------------------
     |
     |
     |
     */
    @UiThread
    void joinWork() {

        boolean canWork = (mLocation != null && isViewInjected && mLocationClient.isConnected());

        if(!canWork) {
            if(!mLocationClient.isConnected() && !mLocationClient.isConnecting()) {
                mLocationClient.connect();
            }

            MyAccount.getInstance().setCanGetGoogleLocation( false );
        }

        double lat, lng;

        try {
        // Views are injected, and location is updated
            lat = mLocation.getLatitude();
            lng = mLocation.getLongitude();

            MyAccount.getInstance().setLatestLatLng(lat, lng);
            MyAccount.getInstance().setCanGetGoogleLocation( true );

        } catch (Exception e) {
            MyAccount.getInstance().setCanGetGoogleLocation( false );
            Log.d("test", "get location error");
        }

        if(mPlaces != null && mPlaces.size() > 0) {
            Log.d("test", "cache !! no load data");

        } else {

            try {

                Log.d("test", "First load data !");
                myPage = 1;

                // Show progress dialog
                if(myPage <= maxPage) {

                    String apiToken = MyAccount.getInstance().getApiToken();
                    int limit = 20;

                    /** This should auto show the progressDialog */
                    if(!isBackgroundTaskRunning) {
                        isBackgroundTaskRunning = true;
                        new GetAllRestaurantsTask(this, MyAccount.getInstance().getLatestLatLng(), apiToken, myPage, limit, new GetAllRestaurantsTask.GetAllRestaurantCallback() {
                            @Override
                            public void onFinishedGetRestaurant(RestaurantPageResponse pageResponse) {
                                updateUI( pageResponse );
                            }
                        }).execute();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    @UiThread
    void updateUI(RestaurantPageResponse pageResponse) {
        /** And this should not show any dialog 2 */
        if(pageResponse == null || pageResponse.getmPlaces() == null || pageResponse.getmPlaces().isEmpty()) {
            // Error
            return;
        }

        maxPage = pageResponse.getTotalPage();
        myPage = pageResponse.getPage() + 1;

        // Add all
        boolean clear;
        if(pageResponse.getPage() == 1) {
            mPlaces = new ArrayList<MyPlace>();
            mPlaces.addAll( pageResponse.getmPlaces() );
            clear = true;
        } else {
            mPlaces.addAll( pageResponse.getmPlaces() );
            clear = false;
        }

        // Add minimarts
        if(pageResponse.getPage() == 1) {
            mMarts = new ArrayList<MyMinimart>();
            mMarts.addAll( pageResponse.getmMinimarts() );
        } else {
            mMarts.addAll( pageResponse.getmMinimarts() );
        }

        // Let's listView and mapView has a reference to mPlaces
        Log.d("test", "places : " + mPlaces.size() + ", minimarts : " + mMarts.size());
        listFragment.updatePlaces(mPlaces, (ArrayList<MyMinimart>) mMarts, clear);

        // If listview can show, map is 100% safe.
        MyAccount.getInstance().setGooglePlayServicesAvailable( true );

        // End of this, then can load new task.
        isBackgroundTaskRunning = false;
    }

    @Override
    public void onLastCellDidAppear() {
        try {
            if(myPage <= maxPage) {

                String apiToken = MyAccount.getInstance().getApiToken();
                int limit = 20;

                if(!isBackgroundTaskRunning) {
                    isBackgroundTaskRunning = true;
                    new GetAllRestaurantsTask(this, MyAccount.getInstance().getLatestLatLng(), apiToken, myPage, limit, new GetAllRestaurantsTask.GetAllRestaurantCallback() {
                        @Override
                        public void onFinishedGetRestaurant(RestaurantPageResponse pageResponse) {
                            updateUI( pageResponse );
                        }
                    }).execute();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
