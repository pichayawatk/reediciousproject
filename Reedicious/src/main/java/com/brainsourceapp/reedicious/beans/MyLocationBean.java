package com.brainsourceapp.reedicious.beans;

import android.app.Activity;
import android.app.Application;
import android.content.IntentSender;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.brainsourceapp.reedicious.R;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.location.LocationClient;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;
import org.androidannotations.annotations.res.StringRes;

@EBean
public class MyLocationBean
        implements  Application.ActivityLifecycleCallbacks,
                    GooglePlayServicesClient.ConnectionCallbacks,
                    GooglePlayServicesClient.OnConnectionFailedListener {

    public MyLocationBean() {

    }

    @StringRes(R.string.my_location_bean_tag)
    String tag;

    /** Only injected if the root context is an activity */
    @RootContext
    Activity activity;

    /**
     |-----------------------------------------------------
     |
     |-----------------------------------------------------
     |
     |
     */
    LocationBeanCallback mCallback;

    /**
     |-----------------------------------------------------
     |
     |-----------------------------------------------------
     |
     |
     */
    private LocationClient mLocationClient;

    /**
     |-----------------------------------------------------
     | Callback Interfaces
     |-----------------------------------------------------
     |
     |
     */
    public interface LocationBeanCallback {
        void OnFinishedGetLocation(boolean success, LocationClient locationClient);
    }

    @Override
    public void onActivityCreated(Activity activity, Bundle savedInstanceState) {

        /*
         * Attached the callback
         */
        mCallback = (LocationBeanCallback) activity;

        /*
         * Create a new location client, using the enclosing class to
         * handle callbacks
         */
        if( isGooglePlayServicesAvailable() ) {

            mLocationClient = new LocationClient(activity, this, this);
        }
    }

    @Override
    public void onActivityStarted(Activity activity) {

        /*
         * Connect the client.
         */
        if( isGooglePlayServicesAvailable() ) {

            mLocationClient.connect();
        }
    }

    @Override
    public void onActivityResumed(Activity activity) {

    }

    @Override
    public void onActivityPaused(Activity activity) {

    }

    @Override
    public void onActivityStopped(Activity activity) {

        /*
         * Disconnect the client invalidates it.
         */
        if( isGooglePlayServicesAvailable() ) {

            mLocationClient.disconnect();
        }
    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle outState) {

    }

    @Override
    public void onActivityDestroyed(Activity activity) {

    }

    /**
     |-----------------------------------------------------
     | Check Google Play Services
     |-----------------------------------------------------
     |
     |
     */
    private boolean isGooglePlayServicesAvailable() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(activity);

        if(ConnectionResult.SUCCESS == resultCode) {
            Log.i(tag, "Google Play services is available.");
            return true;
        } else {

            // TODO: Fallback handle implementation.
            Log.i(tag, "Google Play services is unavailable.");
            return false;
        }
    }

    /**
     |-----------------------------------------------------
     | ON CONNECTION CALLBACKS
     |-----------------------------------------------------
     |
     |
     */
    @Override
    public void onConnected(Bundle bundle) {

        Log.i(tag, "connected.");
        mCallback.OnFinishedGetLocation( true, mLocationClient );
    }

    @Override
    public void onDisconnected() {

        Log.i(tag, "disconnected.");
    }

    /**
     |-----------------------------------------------------
     | ON CONNECTION FAILED LISTENER
     |-----------------------------------------------------
     |
     |
     */
    private static final int CONNECTION_FIALURE_RESOLUTION_REQUEST = 9000;

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.i(tag, "Connection failed.");

        if(connectionResult.hasResolution()) {

            try {
                connectionResult.startResolutionForResult(activity, CONNECTION_FIALURE_RESOLUTION_REQUEST);
            } catch (IntentSender.SendIntentException e) {
                Log.i(tag, "ConnectionResult Error.");
                e.printStackTrace();
            }
        } else {

            // TODO: show dialog instead of Toast
            Toast.makeText(activity, connectionResult.getErrorCode(), Toast.LENGTH_SHORT).show();
        }
    }
}
