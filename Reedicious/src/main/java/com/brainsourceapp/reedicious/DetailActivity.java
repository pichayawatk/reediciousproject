package com.brainsourceapp.reedicious;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v13.app.FragmentPagerAdapter;
import android.support.v4.app.NavUtils;
import android.support.v4.view.PagerTabStrip;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.brainsourceapp.reedicious.beans.MyAccount;
import com.brainsourceapp.reedicious.beans.MyRestBean;
import com.brainsourceapp.reedicious.fragments.CommentFragment;
import com.brainsourceapp.reedicious.fragments.DescriptionFragment;
import com.brainsourceapp.reedicious.fragments.FBImagePublisherFragment;
import com.brainsourceapp.reedicious.fragments.PromotionsFragment;
import com.brainsourceapp.reedicious.fragments.RouteFragment;
import com.brainsourceapp.reedicious.fragments.VideoFragment;
import com.brainsourceapp.reedicious.rest.results.Gallery;
import com.brainsourceapp.reedicious.rest.results.PlaceDetail;
import com.brainsourceapp.reedicious.utils.BSProgressHUD;
import com.brainsourceapp.reedicious.utils.Connectivity;
import com.brainsourceapp.reedicious.views.CommentAdapter;
import com.facebook.FacebookRequestError;
import com.facebook.HttpMethod;
import com.facebook.Request;
import com.facebook.RequestAsyncTask;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.model.GraphUser;
import com.flurry.android.FlurryAgent;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.InstanceState;
import org.androidannotations.annotations.OptionsItem;
import org.androidannotations.annotations.OptionsMenu;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.res.StringArrayRes;
import org.jetbrains.annotations.NotNull;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@EActivity(R.layout.activity_detail)
@OptionsMenu(R.menu.detail)
public class DetailActivity extends Activity implements CommentAdapter.Callback,
        DescriptionFragment.DescriptionFragmentListener,
        FBImagePublisherFragment.FBImagePublishFragmentCallback {

    public static final int REQUEST_LOGIN_CODE_FROM_COMMENT = 1423;
    public static final int REQUEST_LOGIN_CODE_FROM_DESCRIPTION = 1424;

    PlaceDetail.Data mData;

    ArrayList<Gallery> galleryList;

    // Reference
    CommentFragment commentFragment;
    RouteFragment routeFragment;

    private int currentPage;

    /**
     *
     * placeID for getting detail from restClient
     *
     */
    @Extra
    String placeID;

    @InstanceState
    String mPlaceTitle;

    /**
     *
     * PagerTitleStrip
     *
     */
    @ViewById(R.id.detail_pager_tab_strip)
    PagerTabStrip mPagerTabStrip;

    /**
     * Page Title Array from resources
     */
    @StringArrayRes(R.array.title_detail_pages)
    String[] pagesTitle;

    @ViewById(R.id.detail_view_pager)
    ViewPager mViewPager;

    @Bean
    MyRestBean restBean;

    UiLifecycleHelper uiHelper;
    private Session.StatusCallback callback = new Session.StatusCallback() {
        @Override
        public void call(Session session, SessionState state, Exception exception) {
            onSessionStateChange(session, state, exception);
        }
    };

    /** ---------------------- */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState != null) {
            pendingPublishReauthorization =
                    savedInstanceState.getBoolean(PENDING_PUBLISH_KEY, false);
        }

        uiHelper = new UiLifecycleHelper(this, callback);
        uiHelper.onCreate(savedInstanceState);
    }

    @AfterViews
    protected void afterDetailViews() {

        if(MyAccount.getInstance().isGooglePlayServicesAvailable()
                && MyAccount.getInstance().canGetGoogleLocation()) {

            Map<String, String> map = new HashMap<String, String>();
            map.put("screenName", MyAccount.getInstance().getAccountScreenName());
            map.put("placeTitle", placeID);
            FlurryAgent.logEvent("View Restaurant's Info on Details page", map, true);

            initDefaultNavigation();
            BSProgressHUD.showProgressHUD(DetailActivity.this, "LOADING");
            loadDataAsync();
        } else {

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(R.string.app_name);
            builder.setMessage(R.string.no_google_play_services);
            builder.setPositiveButton(R.string.ok_th, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    Intent myIntent = new Intent( Settings.ACTION_LOCATION_SOURCE_SETTINGS );
                    startActivity(myIntent);
                    finish();
                }
            });
            builder.setCancelable(false);
            AlertDialog alertDialog = builder.create();
            alertDialog.show();
        }
    }

    @Background
    void loadDataAsync() {

        int id = Integer.parseInt( placeID );

        mData = restBean.getPlaceDetail( id );

        updateUI();
    }

    @UiThread
    void updateUI() {
        // dismiss dialog
        BSProgressHUD.dismissProgressHUD();

        if(mData == null) {

            Log.d("test", "error null");
        }
        else if(mData.equals( new PlaceDetail.Data())) {

            Log.d("test", "empty no content");
        }
        else {

            // Keep ArrayList of Galleries
            galleryList = new ArrayList<Gallery>();
            for(Gallery item : mData.galleries) {
                galleryList.add( item );
            }

            // Create the adapter that will return a fragment for each of the three
            // primary sections of the app.
            MyAdapter adapter = new MyAdapter( getFragmentManager() );

            // Set up the ViewPager with the sections adapter.
            mViewPager.setAdapter( adapter );

            // Set listener (for start getting direction at the right time
            mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int i, float v, int i2) {

                }

                @Override
                public void onPageSelected(int i) {
                    currentPage = i;
                    Log.d("test", "__onPageSelected : " + currentPage);

                    /** Pager is showing the comment */
                    if(commentFragment != null) {
                        if(i == 4) {
                            commentFragment.setOnScreen( true );
                            commentFragment.joinWork();
                        } else {
                            if(commentFragment.isOnScreen())    commentFragment.setOnScreen( false );
                        }
                    }


                    /** Pager is showing the MapFragment */
                    if(i == 1) {
                        if(routeFragment == null) {
                            routeFragment = RouteFragment.newInstance(mData, MyAccount.getInstance().getLatestLatLng(), true);
                        }

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                routeFragment.updateLayout();
                            }
                        });

                    }
                }

                @Override
                public void onPageScrollStateChanged(int i) {

                }
            });
        }
    }

    private void initDefaultNavigation() {
        // Show the Up button in the action bar.
        ActionBar actionBar = getActionBar();
        if(actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        // Custom the PagerTabStrip
        mPagerTabStrip.setDrawFullUnderline( true );
        mPagerTabStrip.setTabIndicatorColorResource(R.color.pressed_reedecious);
    }

    @OptionsItem(android.R.id.home)
    void didClickedHomeMenu() {
        try {
            NavUtils.navigateUpFromSameTask(this);
        } catch (Exception e) {
            finish();
        }
    }

    /**
     *  Callback from {@link com.brainsourceapp.reedicious.views.CommentAdapter}
     *  when last cell is show on screen.
     */
    @Override
    public void OnLastViewShowedListener() {
        if(commentFragment != null) {
            commentFragment.onLastViewShowed();
        }
    }


    /**
     *
     *  Callback from DescriptionFragment when facebook share button is clicked
     *
     */
    @Override
    public void onFBShareButtonClicked() {

        /** Check main condition */
        if ( !MyAccount.getInstance().isLoggedIn() || !MyAccount.getInstance().isFacebookStatusMapped() ) {

            SettingsActivity_.intent( this ).startForResult( REQUEST_LOGIN_CODE_FROM_DESCRIPTION );
            return;
        }

        /** User should logged in with facebook now,
         *  Check only the permission. */
        //safety check
        if ( !Connectivity.isConnected( this )) {

            return;
        }

        Session.openActiveSession(this, true, new Session.StatusCallback() {

            @Override
            public void call(Session session, SessionState state, Exception exception) {
                if(session.isOpened()) {

                    Request.newMeRequest(session, new Request.GraphUserCallback() {
                        @Override
                        public void onCompleted(GraphUser user, Response response) {
                            if(user != null) {

                                publishStory();
                            }
                        }
                    }).executeAsync();
                }
            }
        });
    }

    private synchronized String getSmartDescription() {

        String description = "";

        if( textNotNull( mData.title ) )      description = description + mData.title + mData.getsmartTagIG() + "\n";
        if( textNotNull( mData.desc ))        description = description + mData.desc + "\n";
        if( textNotNull( mData.address ))       description = description + mData.address + "\n";
        if( textNotNull( mData.openTime ))      description = description + mData.openTime + "\n";
        if( textNotNull( mData.phone ))         description = description + mData.phone;

        return description;
    }

    /**
     * Check input string if not null
     */
    public boolean textNotNull(String text) {

        return (text != null && text.trim().length() > 0);
    }

    @UiThread
    void publishStory() {

        Log.d("test", "creating the publish fragment view");
        Session session = Session.getActiveSession();

        if( session != null ) {

            // Check for publish permissions
            List<String> permissions = session.getPermissions();
            if (!isSubsetOf(Arrays.asList("publish_actions"), permissions)) {
                pendingPublishReauthorization = true;
                Session.NewPermissionsRequest newPermissionsRequest = new Session
                        .NewPermissionsRequest(this, Arrays.asList("publish_actions"));
                session.requestNewPublishPermissions(newPermissionsRequest);
                return;
            }

            String imageURL = galleryList.get(0).url;
            String description = this.getSmartDescription();
            String linkedURL = mData.website;
            mPlaceTitle = mData.title;

            showFBPublishFragment(imageURL, mData.title, description, linkedURL);
        }
    }

    @ViewById(R.id.detail_fragment_view_holder)
    View publishView;
    FBImagePublisherFragment mPublishFragment;

    @UiThread
    protected void showFBPublishFragment(String imageURL, String title, String description, String linkedURL) {

        publishView.setVisibility( View.VISIBLE );
        mPublishFragment =  FBImagePublisherFragment.newInstance(imageURL, "", description, linkedURL, title);
        getFragmentManager().beginTransaction()
                .replace(R.id.detail_fragment_container, mPublishFragment)
                .commit();
    }

    private static final String PENDING_PUBLISH_KEY = "pendingPublishReauthorization";
    private boolean pendingPublishReauthorization = false;

    private boolean isSubsetOf(Collection<String> subset, Collection<String> superset) {
        for (String string : subset) {
            if (!superset.contains(string)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public void willPublishImageToFacebook(boolean canShare, Bitmap bitmap, String description, String linkedURL) {

        if(mPublishFragment != null) {
            getFragmentManager().beginTransaction()
                    .remove( mPublishFragment )
                    .commit();
        }
        publishView.setVisibility(View.GONE);

        if(canShare) {

            willPublishPictureToFeed(bitmap, description, linkedURL);
        }
    }

    public void willPublishPictureToFeed(Bitmap bitmap, String description, String linkedURL) {

        BSProgressHUD.showProgressHUD(DetailActivity.this, "PUBLISHING");

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
        byte[] data = byteArrayOutputStream.toByteArray();

        Bundle postParams = new Bundle();
        postParams.putByteArray("picture", data);
        postParams.putString("name", description);
        postParams.putString("link", linkedURL);

        Request.Callback callback = new Request.Callback() {
            @Override
            public void onCompleted(Response response) {
                BSProgressHUD.dismissProgressHUD();
                FacebookRequestError error = response.getError();
                String result = "สำเร็จ";
                if(error != null) {
                    result = error.toString();
                }
                assert getApplicationContext() != null;
                Toast.makeText(getApplicationContext(), result, Toast.LENGTH_LONG).show();

                Map<String, String> param = new HashMap<String, String>();
                param.put("screenName", MyAccount.getInstance().getAccountScreenName());
                param.put("placeTitle", mPlaceTitle);
                FlurryAgent.logEvent("Share Restaurant on Facebook", param);
            }
        };

        Request request = new Request(Session.getActiveSession(), "me/photos", postParams, HttpMethod.POST, callback);
        RequestAsyncTask task = new RequestAsyncTask(request);
        task.execute();
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class MyAdapter extends FragmentPagerAdapter {

        public MyAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int i) {
            Fragment fragment;

            Log.d("test", "Adapter Get Item : " + i + " when the current page is : " + currentPage);

            if(i == 0) {
                fragment = DescriptionFragment.newInstance( mData );

            } else if(i == 1) {

                routeFragment = RouteFragment.newInstance( mData, MyAccount.getInstance().getLatestLatLng(), true);
                fragment = routeFragment;

            } else if(i == 2) {
                int placeID = mData.id;
                fragment = PromotionsFragment.newInstance( placeID );

            } else if(i == 3) {
                fragment = VideoFragment.newInstance( mData );

            } else if(i == 4) {
                commentFragment = CommentFragment.newInstance( mData );
                fragment = commentFragment;

            } else {
                Log.d("test", "description index out off bound");
                return null;
            }

            return fragment;
        }

        @Override
        public int getCount() {
            return 5;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return pagesTitle[position];
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            super.destroyItem(container, position, object);
        }
    }


    /**
     *
     *
     *  Facebook Zone
     *
     */
    private void onSessionStateChange(Session session, SessionState state, Exception exception) {

        if (state.isOpened()) {

            if (pendingPublishReauthorization &&
                    state.equals(SessionState.OPENED_TOKEN_UPDATED)) {
                pendingPublishReauthorization = false;
                publishStory();
            }
        } else if (state.isClosed()) {

            Log.d("test", "status is close");
        }
    }

    boolean isResumed;

    @Override
    public void onSaveInstanceState(@NotNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(PENDING_PUBLISH_KEY, pendingPublishReauthorization);
        uiHelper.onSaveInstanceState(outState);
    }

    @Override
    protected void onResume() {
        super.onResume();
        uiHelper.onResume();
        isResumed = true;
    }

    @Override
    protected void onPause() {
        super.onPause();
        uiHelper.onResume();
        isResumed = false;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        uiHelper.onDestroy();
        FlurryAgent.endTimedEvent("View Restaurant's Info on Details page");
    }

    @Override
    public void onBackPressed() {

        if(mPublishFragment != null) {
            getFragmentManager().beginTransaction()
                    .remove(mPublishFragment)
                    .commit();
            mPublishFragment = null;
            publishView.setVisibility(View.GONE);
        } else {

            super.onBackPressed();
        }
    }
}