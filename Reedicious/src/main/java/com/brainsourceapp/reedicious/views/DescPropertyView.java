package com.brainsourceapp.reedicious.views;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.brainsourceapp.reedicious.R;
import com.brainsourceapp.reedicious.fragments.ShopDesc;
import com.brainsourceapp.reedicious.utils.BSDialogIntentBuilder;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

@EViewGroup(R.layout.layout_desc_with_icon)
public class DescPropertyView extends LinearLayout {

    private int type;
    public void settype(int type) {
        this.type = type;
    }
    public int gettype() {
        return type;
    }

    @ViewById(R.id.descwi_image)
    ImageView image;

    @ViewById(R.id.descwi_text)
    RDTextView text;

    public void setTitleText(String text) {
        if(text == null || text.trim().length() == 0) {
            text = "N/A";
        }
        this.text.setText( text );
    }

    public void setDynamicHeight(boolean enabled) {
        if(enabled) {
            text.setMaxLines(999);
        } else {
            text.setMaxLines(1);
        }
    }

    @Click(R.id.descwi_image)
    void onIconClicked() {
        onCellClicked();
    }

    @Click(R.id.descwi_text)
    void onTextClicked() {
        onCellClicked();
    }

    public DescPropertyView(Context context) {
        super(context);
    }

    public DescPropertyView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public DescPropertyView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public void setImageWithResourceID(int resID) {
        this.image.setImageResource( resID );
    }

    @UiThread
    public void onCellClicked() {

        if(type == ShopDesc.LOCATION_TAB) {

            Log.d("test", "address click");
        }
        else if(type == ShopDesc.OFFICETIME_TAB) {

            Log.d("test", "open time click");
        }
        else if(type == ShopDesc.TELEPHONE_TAB) {

            Log.d("test", "telephone click");
            callPhone();
        }
        else if(type == ShopDesc.WEB_TAB) {
            assert text.getText() != null;

            if(!"N/A".equals( text.getText().toString()) ) {
                callWebURL( text.getText().toString() );
            }
        }
        else if(type == ShopDesc.FACEBOOK_TAB) {

            Log.d("test", "facebook click");

            CharSequence fburl = text.getText();
            if(fburl != null && !"N/A".equals( fburl.toString() )) {

                callWebURL( fburl.toString() );
                Log.d("test", "facebook web bun clicked");
            }
        }
    }

    void callPhone() {
        // Get all telephone number
        CharSequence cs = text.getText();

        if(cs == null) {
            return;
        }

        String[] phoneNumbers = cs.toString().split("[,]");

        for(int i=0, len=phoneNumbers.length; i<len; i++) {
            phoneNumbers[i] = phoneNumbers[i].replaceAll("[^0-9]", "");
        }

        BSDialogIntentBuilder.newInstance(getContext()).openPhoneNumbersIntent(phoneNumbers);
    }

    void callWebURL(String url) {
        if(url != null && url.trim().length() > 0 && !url.equals("N/A")) {

            if(!url.startsWith("http://") && !url.startsWith("https://")) {
                url = "http://" + url;
            }

            BSDialogIntentBuilder.newInstance(getContext()).openWebIntent(url);
        }
    }
}
