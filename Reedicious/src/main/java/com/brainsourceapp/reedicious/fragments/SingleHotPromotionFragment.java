package com.brainsourceapp.reedicious.fragments;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.brainsourceapp.reedicious.R;
import com.brainsourceapp.reedicious.WebViewActivity_;
import com.brainsourceapp.reedicious.beans.MyAccount;
import com.brainsourceapp.reedicious.rest.ReedClient;
import com.brainsourceapp.reedicious.rest.results.SingleHotPromotionResult;
import com.brainsourceapp.reedicious.transformers.image.RoundedTransformation;
import com.brainsourceapp.reedicious.utils.BSProgressHUD;
import com.flurry.android.FlurryAgent;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.res.StringRes;
import org.androidannotations.annotations.rest.RestService;

import java.util.HashMap;
import java.util.Map;

@EFragment(R.layout.single_hot_promotion)
public class SingleHotPromotionFragment extends Fragment implements Callback {

    public static SingleHotPromotionFragment newInstance(double lat, double lng) {
        SingleHotPromotionFragment fragment = new SingleHotPromotionFragment_();
        Bundle bundle = new Bundle();
        bundle.putDouble("lat", lat);
        bundle.putDouble("lng", lng);
        fragment.setArguments(bundle);
        return fragment;
    }

    double lat;
    double lng;

    String promotionLink;

    @StringRes(R.string.alert_promotion_clicked)
    String alertMsg;

    @RestService
    ReedClient reedClient;

    @ViewById(R.id.single_hot_promotion_imageview)
    ImageView promotionImage;

    @ViewById(R.id.single_hot_promotion_closebtn)
    Button closeButton;

    @Click(R.id.single_hot_promotion_closebtn)
    void onCloseButtonClicked() {
        assert getActivity() != null;
        getActivity().onBackPressed();
    }

    @AfterViews
    void afterSingleHotPromotionViews() {

        promotionImage.setAlpha(0f);
        closeButton.setAlpha(0f);

        if(getArguments() != null) {
            Bundle bundle = getArguments();
            lat = bundle.getDouble("lat");
            lng = bundle.getDouble("lng");
        }

        // Show progress dialog here
        String tokenApi = MyAccount.getInstance().getApiToken();
        BSProgressHUD.showProgressHUD(getActivity(), "LOADING");
        loadDataAsync(tokenApi, lat + "", lng + "", "");
    }

    AnimatorSet fadeInAnim;
    ObjectAnimator proimgAnimator;
    ObjectAnimator closebtnAnimator;

    void initAnim() {
        proimgAnimator = ObjectAnimator.ofFloat(promotionImage, "alpha", 0f, 1f);
        proimgAnimator.setDuration(1000);
        proimgAnimator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                promotionImage.setClickable(false);
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                promotionImage.setClickable(true);
            }

            @Override
            public void onAnimationCancel(Animator animation) {
                promotionImage.setClickable(true);
            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });

        closebtnAnimator = ObjectAnimator.ofFloat(closeButton, "alpha", 0f, 1f);
        closebtnAnimator.setDuration(1000);
        closebtnAnimator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                closeButton.setClickable(false);
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                closeButton.setClickable(true);
            }

            @Override
            public void onAnimationCancel(Animator animation) {
                closeButton.setClickable(true);
            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });

        fadeInAnim = new AnimatorSet();
        fadeInAnim.playTogether(proimgAnimator, closebtnAnimator);
    }

    @Background
    void loadDataAsync(String apiToken, String lat, String lng, String categoryID) {

        try {
            SingleHotPromotionResult result = reedClient.getPromotion(apiToken, "1", "1", lat, lng, categoryID);

            updateViews( result.datas.get(0) );
        } catch (Exception e) {
            e.printStackTrace();

            Log.d("test", "error : " + e.getLocalizedMessage());

            updateViews(null);
        }
    }

    @UiThread
    void updateViews( SingleHotPromotionResult.Data data ) {

        BSProgressHUD.dismissProgressHUD();

        if(data == null) {
            assert getActivity() != null;
            getActivity().onBackPressed();
            return;
        }

        Log.d("test", "data :" + data.cover);

        // Set up promotionLink
        promotionLink = data.promotionLink;

        // Load image
        Activity activity = getActivity();
        if(activity != null) {
            Picasso.with(activity)
                    .load(data.cover)
                    .transform(new RoundedTransformation(20, 0))
                    .into(promotionImage, this);
        }

        Map<String, String> param = new HashMap<String, String>();
        param.put("screenName", MyAccount.getInstance().getAccountScreenName());
        param.put("lat", lat + "");
        param.put("lng", lng + "");
        param.put("campaignID", data.ID);
        FlurryAgent.logEvent("View Big Campaign", param);
    }

    @Override
    public void onSuccess() {

        BSProgressHUD.dismissProgressHUD();

        /**
         * Remove auto-size functionality
         *
         */
        if(promotionLink != null && !promotionLink.equals("")) {

            promotionImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(promotionLink != null && !promotionLink.isEmpty() && !promotionLink.equals("")) {
                        assert getActivity() != null;
                        PromotionAlertDialog.newInstance(alertMsg, promotionLink).show(getActivity().getFragmentManager(), "alert");
                    }
                }
            });
        }

        /**
         * Remove old scaling animation, use fade animation instead
         *
         */
        fadeInAnim.start();

        /*
        mAttacher = new PhotoViewAttacher(promotionImage);

        // If this promotion have a urlLink
        // So set it some listener
        if(promotionLink != null && !promotionLink.equals("")) {

            mAttacher.setOnPhotoTapListener(new PhotoViewAttacher.OnPhotoTapListener() {
                @Override
                public void onPhotoTap(View view, float x, float y) {
                    if(promotionLink != null && !promotionLink.isEmpty() && !promotionLink.equals("")) {
                        PromotionAlertDialog.newInstance(alertMsg, promotionLink).show(getFragmentManager(), "alert");
                    }
                }
            });
        }

        // Animation
        promotionImage.startAnimation( fadeInAnim );
        */
    }

    @Override
    public void onError() {
        BSProgressHUD.dismissProgressHUD();
    }

    /**
     |-----------------------------------------------------------------
     |  Static nested alert dialog fragment class
     |-----------------------------------------------------------------
     |
     |
     */
    public static class PromotionAlertDialog extends DialogFragment {
        public static PromotionAlertDialog newInstance(String message, String url) {
            PromotionAlertDialog fragment = new PromotionAlertDialog();
            Bundle extras = new Bundle();
            extras.putString("message", message);
            extras.putString("url", url);
            fragment.setArguments(extras);
            return  fragment;
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            Activity activity = getActivity();
            if(activity != null && getArguments() != null) {

                return new AlertDialog.Builder(getActivity())
                        .setMessage( getArguments().getString("message") )
                        .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                String url = getArguments().getString("url");
                                Log.d("test", "before url : " + url);

                                Map<String, String> param = new HashMap<String, String>();
                                param.put("screenName", MyAccount.getInstance().getAccountScreenName());
                                param.put("campaignURL", url);
                                FlurryAgent.logEvent("View Big Campaign's Info", param);

                                Intent intent = new Intent(getActivity(), WebViewActivity_.class);
                                intent.putExtra("url", url);
                                getActivity().startActivity( intent );
                            }
                        })
                        .create();
            } else {

                return null;
            }
        }
    }
}
