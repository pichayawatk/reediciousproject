package com.brainsourceapp.reedicious.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.Button;

import com.brainsourceapp.reedicious.R;

public class RDButton extends Button {


    public RDButton(Context context) {
        super(context);

    }

    public RDButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        if(!isInEditMode()) {
            parseAttributes(context, attrs);
        }
    }

    public RDButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        if(!isInEditMode()) {
            parseAttributes(context, attrs);
        }
    }

    private void parseAttributes(Context context, AttributeSet attrs) {
        TypedArray values = context.obtainStyledAttributes(attrs, R.styleable.RDButton);

        assert values != null;
        int typeface = values.getInt(R.styleable.RDButton_typeface, 0);

        switch (typeface) {
            case 0:
                setTypeface(RDTextView.getFontRegular(context));
                break;
            case 1:
                setTypeface(RDTextView.getFontBold(context));
                break;
            case 2:
                setTypeface(RDTextView.getFontBoldItalic(context));
                break;
            case 3:
                setTypeface(RDTextView.getFontItalic(context));
                break;
        }
    }
}
