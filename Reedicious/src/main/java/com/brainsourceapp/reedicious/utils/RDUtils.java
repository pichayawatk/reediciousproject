package com.brainsourceapp.reedicious.utils;

public class RDUtils {

    private static final String baseURL = "http://api.graph.facebook.com/";
    private static final String tailURL =  "/picture?type=large";

    public static String geProfileImageUrlByFacebookID(String facebookID) {

        return baseURL + facebookID + tailURL;
    }

}
