package com.brainsourceapp.reedicious.rest.data;

import com.brainsourceapp.reedicious.rest.results.MyPlace;

import java.util.ArrayList;

public class RestaurantPageResponse {

    private int code;
    private String message;

    private int totalRecord;
    private int totalPage;
    private int page;
    private int limit;

    private ArrayList<MyPlace> mPlaces;

    private ArrayList<MyMinimart> mMinimarts;

    public RestaurantPageResponse() {

    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getTotalRecord() {
        return totalRecord;
    }

    public void setTotalRecord(int totalRecord) {
        this.totalRecord = totalRecord;
    }

    public int getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(int totalPage) {
        this.totalPage = totalPage;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public ArrayList<MyPlace> getmPlaces() {
        ArrayList<MyPlace> list = new ArrayList<MyPlace>();
        for(MyPlace place : this.mPlaces) {
            list.add( place );
        }
        return list;
    }

    public void setmPlaces(ArrayList<MyPlace> mPlaces) {
        this.mPlaces = new ArrayList<MyPlace>();
        for(MyPlace place : mPlaces) {
            this.mPlaces.add( place );
        }
    }

    public ArrayList<MyMinimart> getmMinimarts() {
        ArrayList<MyMinimart> list = new ArrayList<MyMinimart>();
        for(MyMinimart minimart : mMinimarts) {
            list.add( minimart );
        }
        return list;
    }

    public void setmMinimarts(ArrayList<MyMinimart> mMinimarts) {
        this.mMinimarts = new ArrayList<MyMinimart>();
        for(MyMinimart minimart : mMinimarts) {
            this.mMinimarts.add( minimart );
        }
    }
}
