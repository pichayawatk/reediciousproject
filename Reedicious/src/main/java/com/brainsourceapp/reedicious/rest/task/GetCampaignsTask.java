package com.brainsourceapp.reedicious.rest.task;

import android.os.AsyncTask;
import android.util.Log;

import com.brainsourceapp.reedicious.rest.response.Campaign;
import com.brainsourceapp.reedicious.rest.response.CampaignsResponse;
import com.brainsourceapp.reedicious.rest.utils.BSHttpUtils;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

public class GetCampaignsTask extends AsyncTask<String, Void, CampaignsResponse> {

    String appid;
    String apiToken;
    CampaignTaskCallback callback;

    public interface CampaignTaskCallback {
        void onFinishGetCampaign(CampaignsResponse response);
    }

    public GetCampaignsTask(String appid, String apiToken, CampaignTaskCallback callback) {
        this.appid = appid;
        this.apiToken = apiToken;
        this.callback = callback;
    }

    @Override
    protected CampaignsResponse doInBackground(String... params) {

        String baseURL = "http://api.reedicious.com/rest/campaign";
        String url = BSHttpUtils.newInstance().getfullURL(baseURL, new  String[] {
                "appid", appid,
                "token", apiToken
        });

        HttpClient client = new DefaultHttpClient();
        HttpGet get = new HttpGet(url);

        try {
            HttpResponse httpResponse = client.execute(get);

            // Get status Code
            int statusCode = httpResponse.getStatusLine().getStatusCode();
            Log.d("test", "status code : " + statusCode);

            String responseString = BSHttpUtils.newInstance().streamToString( httpResponse.getEntity().getContent() );

            JSONObject json = new JSONObject(responseString);
            CampaignsResponse response = new CampaignsResponse();

            if(json.has("header")) {
                JSONObject header = json.getJSONObject("header");
                if(header.has("code"))                      response.setCode( header.getInt("code") );
                if(header.has("message"))                   response.setMessage( header.getString("message") );
            }

            if(json.has("property")) {
                JSONObject property = json.getJSONObject("property");
                if(property.has("totalrecord"))             response.setTotalRecord( property.getInt("totalrecord") );
                if(property.has("totalpage"))               response.setTotalPage(property.getInt("totalpage"));
                if(property.has("page"))                    response.setPage(property.getInt("page"));
                if(property.has("limit"))                   response.setLimit(property.getInt("limit"));
            }
             
            if(json.has("data")) {
                ArrayList<Campaign> campaigns = new ArrayList<Campaign>();
                JSONArray items = json.getJSONArray("data");
                for(int i=0, len=items.length(); i<len; i++) {
                    JSONObject item = items.getJSONObject(i);
                    Campaign campaign = new Campaign();
                    if(item.has("id"))                      campaign.setId( item.getString("id") );
                    if(item.has("join_campaign"))           campaign.setJoin_campaign( item.getString("join_campaign") );
                    if(item.has("campaign_icon"))           campaign.setCampaign_icon( item.getString("campaign_icon") );
                    if(item.has("campaign_url"))            campaign.setCampaign_url(item.getString("campaign_url"));
                    if(item.has("title"))                   campaign.setTitle( item.getString("title") );
                    if(item.has("cover"))                   campaign.setCover( item.getString("cover"));
                    if(item.has("start"))                   campaign.setStart( item.getInt("start"));
                    if(item.has("end"))                     campaign.setEnd( item.getInt("end"));
                    if(item.has("url"))                     campaign.setUrl( item.getString("url"));
                    campaigns.add( campaign );
                }
                response.setCampaigns( campaigns );
            }

            return response;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onPostExecute(CampaignsResponse campaignsResponse) {
        super.onPostExecute(campaignsResponse);

        if(callback != null) {
            callback.onFinishGetCampaign( campaignsResponse );
        }
    }
}