package com.brainsourceapp.reedicious.fragments;

import android.graphics.Point;
import android.location.Location;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.brainsourceapp.reedicious.DetailActivity_;
import com.brainsourceapp.reedicious.R;
import com.brainsourceapp.reedicious.beans.MyAccount;
import com.brainsourceapp.reedicious.rest.data.MyMinimart;
import com.brainsourceapp.reedicious.rest.results.MyPlace;
import com.brainsourceapp.reedicious.utils.RDMapHelper;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.res.StringRes;

import java.util.ArrayList;

@EFragment
public class MyMapFragment extends MapFragment
    implements GooglePlayServicesClient.ConnectionCallbacks,
               GooglePlayServicesClient.OnConnectionFailedListener {

    public MyMapFragment() {
        super();
    }

    public static MyMapFragment newInstance(ArrayList<MyPlace> places, ArrayList<MyMinimart> marts, Location location) {
        MyMapFragment fragment = new MyMapFragment_();
        fragment.setPlaces( places );
        fragment.setMinimarts( marts );
        fragment.setmLocation( location );
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        initMap();
        return view;
    }

    private void initMap() {
        getMap().setMyLocationEnabled( true );
        getMap().getUiSettings().setMyLocationButtonEnabled( true );
        getMap().getUiSettings().setCompassEnabled( true );
    }

    private ArrayList<MyPlace> places;
    public void setPlaces(ArrayList<MyPlace> data) {
        places = new ArrayList<MyPlace>();
        for(MyPlace place : data) {
            places.add( place );
        }
    }

    private ArrayList<MyMinimart> marts;
    public void setMinimarts(ArrayList<MyMinimart> data) {
        marts = new ArrayList<MyMinimart>();
        for(MyMinimart minimart : data) {
            marts.add( minimart );
        }
    }

    private LocationClient mLocationClient;
    private Location mLocation;
    public void setmLocation(Location location) {
        mLocation = location;
    }

    @StringRes(R.string.label_distance)
    String distancePrefix;

    @StringRes(R.string.unit_distance)
    String distancePostfix;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // TODO: BUG this method hit 3 times by ??
        if(null == mLocationClient) {
            mLocationClient = new LocationClient(getActivity(), this, MyMapFragment.this);
            Log.d("test", "create");
        }
    }

    @AfterViews
    void afterMyMapViews() {

        updateMarker();

        updateCamera();

        initMapEventListener();
    }

    @UiThread
    public void updateMarker(ArrayList<MyPlace> places, ArrayList<MyMinimart> marts) {
        setPlaces( places );

        setMinimarts( marts );

        updateMarker();
    }

    @UiThread
    public void updateMarker() {
        // Add my location first
        /*
        getMap().addMarker( new MarkerOptions()
                .position(new LatLng(mLocation.getLatitude(), mLocation.getLongitude()))
                .title("me"));
                //.icon(BitmapDescriptorFactory.fromResource(R.drawable.pin_me)));
                */

        // Iteration
        for(MyPlace place : places) {

            Log.d("test", "place category : " + place.category);

            int respinID = RDMapHelper.getPinResID(place.category);

            String placeID = String.valueOf( place.id );
            String detail = place.title + "|" + place.address + "|" + String.format( "%.2f", Float.valueOf( place.distance ) / 1000 );

            getMap().addMarker(new MarkerOptions()
                    .position(new LatLng( place.latitude, place.longitude) )
                    .title( placeID )                                                   /** Set marker's title as a place's ID */
                    .snippet(detail)                                                    /** Set marker's snippet as it's detail */
                    .icon(BitmapDescriptorFactory.fromResource(respinID)));
        }

        // Add Minimarts
        for(MyMinimart mart : marts) {

            Log.d("test", "minimart name : " + mart.getMart_name());

            getMap().addMarker(new MarkerOptions()
                    .position(new LatLng(Double.valueOf(mart.getMart_lat()), Double.valueOf(mart.getMart_lng())))
                    .title(mart.getMart_name())
                    .snippet("7-11")
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_minimart)));

        }
    }

    @UiThread
    void updateCamera() {
        // Move camera instantly to Sydney with a zoom of 10
        getMap().moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(mLocation.getLatitude(), mLocation.getLongitude()), 10));

        // Zoom in, animating the camera.
        getMap().animateCamera(CameraUpdateFactory.zoomIn());

        // Zoom out to zoom level 13, animating with a duration of 2 seconds.
        getMap().animateCamera(CameraUpdateFactory.zoomTo(13), 2000, null);

        // Get zoom level try
        try {
            Location source = getMap().getMyLocation();
            float zoom = this.getZoomLevelFromBound( getMap(), source,  mLocation);
        } catch (Exception e) {

            /** Cannot get device location */
        }
    }


    private float getZoomLevelFromBound(GoogleMap map, Location source, Location dest) throws Exception {
        float maxZoom = map.getMaxZoomLevel();
        float minZoom = map.getMinZoomLevel();

        Point northEast = map.getProjection().toScreenLocation( new LatLng(source.getLatitude(), source.getLongitude()) );
        Point southWest = map.getProjection().toScreenLocation( new LatLng(dest.getLatitude(), dest.getLongitude()) );

        float worldCoorWidth = Math.abs( northEast.x - southWest.x );
        float worldCoorHeight = Math.abs( northEast.y - southWest.y );

        // Fit Padding in pixel
        // TODO: screenSize
        assert getActivity() != null;
        DisplayMetrics dm = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics( dm );
        float fitPad = dm.widthPixels * 9/10;

        // Fit padding in pixel
        for(float zoom = maxZoom; zoom >= minZoom; zoom--) {
            if(worldCoorWidth*zoom + fitPad < dm.widthPixels &&
                    worldCoorHeight*zoom + fitPad < dm.heightPixels) {
                return zoom;
            }
        }

        return ( minZoom + maxZoom ) / 2;
    }

    @UiThread
    void initMapEventListener() {

        // Set info window adapter
        getMap().setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
            @Override
            public View getInfoWindow(Marker marker) {
                // Inflate new view
                assert getActivity() != null;
                View view = getActivity().getLayoutInflater().inflate(R.layout.info_window_layout, null);

                String detail = marker.getSnippet();
                String[] details = detail.split("\\|");

                // Update UI
                assert view != null;
                ((TextView) view.findViewById(R.id.info_window_title)).setText( details[0] );
                ((TextView) view.findViewById(R.id.info_window_address)).setText( details[1] );
                ((TextView) view.findViewById(R.id.info_window_distance)).setText( distancePrefix + " " + details[2] + " " + distancePostfix );

                return view;
            }

            @Override
            public View getInfoContents(Marker marker) {
                return null;
            }
        });

        // Set marker onClicked event
        getMap().setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                if(!"7-11".equals(marker.getSnippet())) {
                    marker.showInfoWindow();
                    return false;
                } else {
                    return true;
                }
            }
        });

        // Set info window click event
        getMap().setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {

                // Get marker title (actually it is placeID)
                String id = marker.getTitle();

                // Start new activity
                DetailActivity_.intent(getActivity()).placeID( id ).start();
            }
        });
    }

    /**
     |---------------------------------------------------
     | CONNECTION CALLBACK
     |---------------------------------------------------
     |
     |
     */
    @Override
    public void onConnected(Bundle bundle) {
        mLocation = mLocationClient.getLastLocation();

        if(mLocation != null) {

            double lat = mLocation.getLatitude();
            double lng = mLocation.getLongitude();

            LatLng latestLatLng = MyAccount.getInstance().getLatestLatLng();

            // Check if location is the same as our previous one
            if(lat == latestLatLng.latitude &&
                    lng == latestLatLng.longitude ) {

                // Do nothing
            } else {

                // Update location to myPref
                MyAccount.getInstance().setLatestLatLng(lat, lng);

                // TODO: Update markers
            }
        }
    }

    @Override
    public void onDisconnected() {
        // TODO: code here
    }

    /**
     |---------------------------------------------------
     | CONNECTION FAILURE LISTENER
     |---------------------------------------------------
     |
     |
     */
    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        // TODO: code here
    }
}


/**
 |----------------------------------------------------------------------
 |  Flow
 |----------------------------------------------------------------------
 |
 |  1. Inject dependencies of location from root context.
 |  2. get last location on background task.
 |  3. update ui if needed.
 |
 |
 */