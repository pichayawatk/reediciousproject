package com.brainsourceapp.reedicious.views;

import android.content.Context;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.brainsourceapp.reedicious.R;
import com.brainsourceapp.reedicious.database.data.MyQRItem;
import com.brainsourceapp.reedicious.utils.BSDateHelper;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

@EViewGroup(R.layout.qrhistory_itemview)
public class QRHistoryItemView extends RelativeLayout {

    boolean isHeader;

    int cellType;

    int resiconID;

    String intentData;

    String dateStr;

    //@ViewById(R.id.qrhistory_cell_bg)
    //ImageView cellBG;
    @ViewById(R.id.qrhistory_mainlayout)
    RelativeLayout mainLayout;


    @ViewById(R.id.qrhistory_itemview_text)
    TextView cellTextView;

    @ViewById(R.id.qrhistory_itemview_icon)
    ImageView cellIcon;

    //@ViewById(R.id.qrhistory_cell_btn)
    //Button cellmainBtn;

    //@Click(R.id.qrhistory_cell_btn)
    //void oncellItemClicked() {
    //    // TODO: show alert dialog from type
    //}

    public QRHistoryItemView(Context context) {
        super(context);
    }

    public void bindHeader(MyQRItem item) {
        isHeader = true;

        // Hide some views
        cellIcon.setVisibility(INVISIBLE);
        //cellmainBtn.setVisibility(INVISIBLE);

        //cellBG.setImageResource(R.drawable.history_tab_pink);
        mainLayout.setBackgroundResource(R.drawable.history_tab_pink);

        long timestamp = item.getTimestamp();
        String dateFormat = "dd/MM/yyyy";
        dateStr = BSDateHelper.timestampToThaiDateFormat(timestamp, dateFormat);

        cellTextView.setText(dateStr);
    }

    public void bindData(MyQRItem item) {
        isHeader = false;

        // Show some views
        cellIcon.setVisibility(VISIBLE);
        //cellmainBtn.setVisibility(VISIBLE);

        String typeString = "";
        cellType = item.gettype();

        switch (cellType) {
            case MyQRItem.TYPE_MOBILE:
                typeString = "หมายเลข";
                resiconID = R.drawable.icon_phone;
                break;
            case MyQRItem.TYPE_SMS:
                typeString = "ข้อความ";
                resiconID = R.drawable.icon_mail;
                break;
            case MyQRItem.TYPE_WEB:
                resiconID = R.drawable.icon_web;
                break;
        }

        intentData = item.getIntentData();

        long timestamp = item.getTimestamp();
        String dateFormat = "dd/MM/yyyy";
        dateStr = BSDateHelper.timestampToThaiDateFormat(timestamp, dateFormat);

        // update UI
        cellTextView.setText( typeString + " " + intentData);
        cellIcon.setImageResource(resiconID);
        //cellBG.setImageResource(R.drawable.history_tab_white);
        mainLayout.setBackgroundResource(R.drawable.history_tab_white);
    }

}
