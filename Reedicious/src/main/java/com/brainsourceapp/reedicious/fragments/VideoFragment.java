package com.brainsourceapp.reedicious.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.widget.ImageView;
import android.widget.TextView;

import com.brainsourceapp.reedicious.R;
import com.brainsourceapp.reedicious.YoutubePlayerActivity;
import com.brainsourceapp.reedicious.beans.MyAccount;
import com.brainsourceapp.reedicious.beans.MyHelperBean;
import com.brainsourceapp.reedicious.rest.results.PlaceDetail;
import com.brainsourceapp.reedicious.views.RDTextView;
import com.flurry.android.FlurryAgent;
import com.squareup.picasso.Picasso;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.res.StringRes;

import java.util.HashMap;
import java.util.Map;

@EFragment(R.layout.fragment_video)
public class VideoFragment extends Fragment {

    public static VideoFragment newInstance(PlaceDetail.Data mData) {
        VideoFragment fragment = new VideoFragment_();
        fragment.youtubeID = mData.youtubeID;
        fragment.youtubeURL = mData.youtubeURL;
        fragment.videoDesc = mData.videoDesc;

        fragment.onAirTime = mData.onAirDate;
        fragment.coverURL = mData.cover;
        fragment.placeTitle = mData.title;
        return fragment;
    }

    String youtubeID;
    String youtubeURL;
    String videoDesc;
    int onAirTime;
    String coverURL;
    String placeTitle;

    @Bean
    MyHelperBean helperBean;

    @ViewById(R.id.video_cover_image)
    ImageView videoCoverImage;

    @ViewById(R.id.video_title)
    RDTextView mTitle;

    @ViewById(R.id.video_description)
    TextView mDesc;

    @ViewById(R.id.video_text_overlay)
    TextView textOverlay;

    @StringRes(R.string.label_on_air_time)
    String labelOnAirTime;

    @StringRes(R.string.label_play_clip)
    String labelPlayClip;

    @StringRes(R.string.label_by_reedecious_reed)
    String byReed;

    @Click(R.id.video_play_button)
    void onVideoButtonClicked() {

        Activity activity = getActivity();

        if(activity != null) {

            Intent intent = new Intent(activity, YoutubePlayerActivity.class);
            intent.putExtra("videoID", youtubeID);
            activity.startActivity( intent );
        }
    }

    @AfterViews
    void afterVideoViews() {

        // Set title
        //Activity activity = getActivity();
        //ActionBar actionBar = activity.getActionBar();
        //actionBar.setTitle("Video");

        Map<String, String> param = new HashMap<String, String>();
        param.put("screenName", MyAccount.getInstance().getAccountScreenName());
        param.put("placeTitle", placeTitle);
        FlurryAgent.logEvent("Read Video in Details Page", param);

        textOverlay.setText(String.format("%s\n%s\n%s", labelPlayClip, placeTitle, byReed));

        String title = helperBean.timeStampToDateString( onAirTime );

        mTitle.setText( labelOnAirTime + " " + title );
        //mTitle.setVisibility(View.GONE);

        //mDesc.setText( videoDesc );
        mDesc.setText("");

        Picasso.with(getActivity()).load(coverURL).into( videoCoverImage );
    }

}
