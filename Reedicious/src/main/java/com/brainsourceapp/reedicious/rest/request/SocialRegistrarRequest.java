package com.brainsourceapp.reedicious.rest.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SocialRegistrarRequest {

    @JsonProperty(value = "email")
    public String email;

    @JsonProperty(value = "fb_token")
    public String fb_token;

    @JsonProperty(value =  "fb_id")
    public String fb_id;

    @JsonProperty(value = "screenname")
    public String screenname;

    @JsonProperty(value = "avatar")
    public String avatar;

    public SocialRegistrarRequest(String email, String fb_id, String fb_token, String screenname) {
        this.email = email;
        this.fb_id = fb_id;
        this.fb_token = fb_token;
        this.screenname = screenname;
        this.avatar = "http://images2.fanpop.com/image/photos/10000000/House-Avatar-Crossover-dr-gregory-house-10012748-713-713.jpg";
    }
}
