package com.brainsourceapp.reedicious.rdtools;

import android.content.Context;

import com.brainsourceapp.reedicious.R;

public class FriendlyMsgMapper {

    public static String getMsg(Context context, int errorCode) {
        // TODO: code here
        switch (errorCode) {

            case 402: // Missing Parameter
                return context.getResources().getString(R.string.missing_parameter);
            case 506: // Duplicated Email
                return context.getResources().getString(R.string.duplicated_email);
            default:
                return context.getResources().getString(R.string.q002_connection_time_out);
        }
    }

}
