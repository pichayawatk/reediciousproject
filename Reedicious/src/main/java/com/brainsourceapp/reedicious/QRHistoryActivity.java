package com.brainsourceapp.reedicious;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;

import com.brainsourceapp.reedicious.beans.MyAccount;
import com.brainsourceapp.reedicious.database.QRCodeDBHelper;
import com.brainsourceapp.reedicious.database.data.MyQRItem;
import com.brainsourceapp.reedicious.utils.BSAlertDialog;
import com.brainsourceapp.reedicious.utils.BSDialogIntentBuilder;
import com.brainsourceapp.reedicious.views.QRHistoryAdapter;
import com.flurry.android.FlurryAgent;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ItemClick;
import org.androidannotations.annotations.OptionsItem;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;

@EActivity(R.layout.activity_qrhistory)
public class QRHistoryActivity extends Activity {

    QRCodeDBHelper qrCodeDBHelper;

    @ViewById(R.id.qr_history_listview)
    ListView qrListView;

    @Bean
    QRHistoryAdapter qrAdapter;

    MyQRItem selectedItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(qrCodeDBHelper == null) {
            qrCodeDBHelper = new QRCodeDBHelper(this);
        }
        qrCodeDBHelper.open();

        FlurryAgent.logEvent("View QR Scan History Page");
    }

    @Override
    protected void onResume() {
        qrCodeDBHelper.open();
        super.onResume();
    }

    @Override
    protected void onPause() {
        qrCodeDBHelper.close();
        super.onPause();
    }

    @AfterViews
    public void afterQRHistoryViews() {
        setupActionBar();
        String memberID = MyAccount.getInstance().getAccountID();
        ArrayList<MyQRItem> items = qrCodeDBHelper.getMyIndexedQRItems(memberID);
        qrAdapter.addItems(items);
        Log.d("test", "All items in database : " + items.size());
        qrListView.setAdapter( qrAdapter );
    }

    @ItemClick(R.id.qr_history_listview)
    public void qritemListItemClicked(MyQRItem item) {
        selectedItem = item;

        String alertMsg = item.getAlertMsg();

        int type = item.gettype();

        if(type == MyQRItem.TYPE_SMS) {
            BSAlertDialog.newInstance(alertMsg, new BSAlertDialog.CallbackDialogListener() {
                @Override
                public void onBSDialogPosBtnClicked(DialogInterface dialog, String tag) {
                    dialog.dismiss();
                    Context context = QRHistoryActivity.this;
                    BSDialogIntentBuilder.newInstance(context).openSMSIntent(selectedItem.getIntentData(), selectedItem.getResponseMsg());
                }
            }).show(getFragmentManager(), "alert");
        }
        else if(type == MyQRItem.TYPE_MOBILE) {

            BSAlertDialog.newInstance(alertMsg, new BSAlertDialog.CallbackDialogListener() {
                @Override
                public void onBSDialogPosBtnClicked(DialogInterface dialog, String tag) {
                    dialog.dismiss();
                    Context context = QRHistoryActivity.this;
                    BSDialogIntentBuilder.newInstance(context).openPhoneNumbersIntent(new String[]{selectedItem.getIntentData()});
                }
            }).show(getFragmentManager(), "alert");
        }
        else if(type == MyQRItem.TYPE_WEB) {

            BSAlertDialog.newInstance(alertMsg, new BSAlertDialog.CallbackDialogListener() {
                @Override
                public void onBSDialogPosBtnClicked(DialogInterface dialog, String tag) {
                    dialog.dismiss();
                    Context context = QRHistoryActivity.this;

                    WebViewActivity_.intent(context)
                            .url(selectedItem.getIntentData())
                            .actionbarTitle( "" )
                            .start();

                }
            }).show(getFragmentManager(), "alert");
        }
    }

    private void setupActionBar() {

        ActionBar actionBar = getActionBar();
        if(actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    @OptionsItem(android.R.id.home)
    void onHomeMenuClicked() {
        finish();
    }

}