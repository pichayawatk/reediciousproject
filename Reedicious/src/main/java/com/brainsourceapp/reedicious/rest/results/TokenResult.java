package com.brainsourceapp.reedicious.rest.results;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TokenResult {

    public Data data;

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class Data {

        @JsonProperty(value = "appid")
        public String appID;

        public String token;
    }

}
