package com.brainsourceapp.reedicious.rest.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MemberUpdateRequest {

    public MemberUpdateRequest(String fb_id, String fb_token, String fb_status) {
        this.fb_id = fb_id;
        this.fb_token = fb_token;
        this.fb_status = fb_status;
    }

    @JsonProperty(value = "fb_id")
    public String fb_id;

    @JsonProperty(value = "fb_token")
    public String fb_token;

    @JsonProperty(value = "fb_status")
    public String fb_status;

}
