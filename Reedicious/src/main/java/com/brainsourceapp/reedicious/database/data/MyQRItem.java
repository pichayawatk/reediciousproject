package com.brainsourceapp.reedicious.database.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.brainsourceapp.reedicious.R;
import com.brainsourceapp.reedicious.database.MySQLiteHelper;

import java.util.Calendar;
import java.util.Locale;

public class MyQRItem  {

    public static final int TYPE_HEADER = 0;
    public static final int TYPE_MOBILE = 1;
    public static final int TYPE_SMS = 2;
    public static final int TYPE_WEB = 3;

    private int type;
    private String responseMsg;
    private String alertMsg;
    private String intentData;
    private String memberID;
    private long timestamp;

    public boolean isHeaderCell() {
        return this.type == TYPE_HEADER;
    }

    private MyQRItem(long timestamp) {
        this.type = TYPE_HEADER;
        this.timestamp = timestamp;
        Log.d("test", "type HEADER");
    }

    public static MyQRItem newHeaderDate(long timestamp) {
        return new MyQRItem(timestamp);
    }

    public MyQRItem(String memberID, String rawtext) {
        // Classify the rawtex to
        this.memberID = memberID;
        this.timestamp = Calendar.getInstance(Locale.ENGLISH).getTimeInMillis() / 1000;

        if(rawtext.startsWith("http://") || rawtext.startsWith("https://")) {
            this.type = TYPE_WEB;
            this.intentData = rawtext;
            this.responseMsg = rawtext;
            Log.d("test", "item type WEB");
        } else {

            // SMSTO:0896837453:ร่วมสนุกร้านอร่อย ความคิดเห็น
            String strs[] = rawtext.split("[:]");

            if(strs.length == 2) {
                if(strs[0].toUpperCase(Locale.ENGLISH).contains("TEL")) {
                    this.type = TYPE_MOBILE;
                    this.intentData = strs[1];
                    this.responseMsg = rawtext;
                    Log.d("test", "item type TEL");
                }
            }
            else if(strs.length == 3) {
                if(strs[0].toUpperCase(Locale.ENGLISH).contains("SMS")) {
                    this.type = TYPE_SMS;
                    this.intentData = strs[1];
                    this.responseMsg = strs[2];
                    Log.d("test", "item type SMS");
                    //return;
                }
        } else {

            this.type = 7;
            this.intentData = "";
            this.responseMsg = "";
        }
        }
    }

    public MyQRItem(Cursor cursor) {
        int index = cursor.getColumnIndex(MySQLiteHelper.QR_TYPE);
        if(!cursor.isNull(index))   this.type = cursor.getInt(index);
        else                        this.type = 0;

        index = cursor.getColumnIndex(MySQLiteHelper.QR_RESPONSE_MESSAGE);
        if(!cursor.isNull(index))   this.responseMsg = cursor.getString(index);
        else                        this.responseMsg = "";

        index = cursor.getColumnIndex(MySQLiteHelper.QR_ALERT_MESSAGE);
        if(!cursor.isNull(index))   this.alertMsg = cursor.getString(index);
        else                        this.alertMsg = "";

        index = cursor.getColumnIndex(MySQLiteHelper.QR_INTENT_DATA);
        if(!cursor.isNull(index))   this.intentData = cursor.getString(index);
        else                        this.intentData = "";

        index = cursor.getColumnIndex(MySQLiteHelper.QR_MEMBER_ID);
        if(!cursor.isNull(index))   this.memberID = cursor.getString(index);
        else                        this.memberID = "";

        index = cursor.getColumnIndex(MySQLiteHelper.QR_CREATED_AT);
        if(!cursor.isNull(index))   this.timestamp = cursor.getLong(index);
        else                        this.timestamp = Calendar.getInstance(Locale.ENGLISH).getTimeInMillis() / 1000;
    }

    public ContentValues toContentValues() {
        ContentValues c = new ContentValues();
        c.put(MySQLiteHelper.QR_TYPE, this.type);
        c.put(MySQLiteHelper.QR_RESPONSE_MESSAGE, this.responseMsg);
        c.put(MySQLiteHelper.QR_ALERT_MESSAGE, this.alertMsg);
        c.put(MySQLiteHelper.QR_INTENT_DATA, this.intentData);
        c.put(MySQLiteHelper.QR_MEMBER_ID, this.memberID);
        c.put(MySQLiteHelper.QR_CREATED_AT, this.timestamp);
        return c;
    }

    /** GETTER - SETTER */
    public int gettype() {
        return type;
    }

    public String getIntentData() { return intentData; }

    public String getAlertMsg() { return alertMsg; }
    public void setAlertMsg(Context context) {
        if(this.type == MyQRItem.TYPE_MOBILE) {
            this.alertMsg = context.getResources().getString(R.string.qrcode_alert_mobile) + " " + this.intentData;
        }
        else if(this.type == MyQRItem.TYPE_SMS) {
            this.alertMsg = context.getResources().getString(R.string.qrcode_alert_will_send_msg_name) + " \"" + this.responseMsg + "\" "
                    + context.getResources().getString(R.string.qrcode_alert_sendmsg_to_number) + " " + this.intentData;
        }
        else if(this.type == MyQRItem.TYPE_WEB) {
            this.alertMsg = context.getResources().getString(R.string.qrcode_alert_web) + " " + this.intentData;
        }
    }

    public String getResponseMsg() {
        return responseMsg;
    }

    public String getMemberID() {
        return memberID;
    }

    public void setMemberID(String memberID) {
        this.memberID = memberID;
    }

    public long getTimestamp() {
        return this.timestamp;
    }
}
