package com.brainsourceapp.reedicious.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.graphics.Bitmap;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.Volley;
import com.brainsourceapp.reedicious.R;
import com.brainsourceapp.reedicious.SettingsActivity;
import com.brainsourceapp.reedicious.UpdateProfileActivity_;
import com.brainsourceapp.reedicious.beans.MyAccount;
import com.brainsourceapp.reedicious.utils.BSProgressHUD;
import com.brainsourceapp.reedicious.views.FacebookMapperView;
import com.brainsourceapp.reedicious.views.RDTextView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

@EFragment(R.layout.settings_fragment)
public class SettingsFragment extends Fragment {

    private static final String TAG = SettingsFragment.class.getSimpleName();

    public static SettingsFragment newInstance() {
        return new SettingsFragment_();
    }

    public interface SettingsFragmentListener {
        void didClickLogOutButton();
    }

    SettingsFragmentListener mListener;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (SettingsFragmentListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(TAG + " : must implement SettingsFragmentListener Interface.");
        }
    }

    ImageLoader mImageLoader;
    RequestQueue mRequestQueue;

    @Click(R.id.settings_logout_btn)
    public void didClickLogOutButton() {
        mListener.didClickLogOutButton();
    }

    @Click(R.id.setting_edit_profile_btn)
    public void didClickUpdateProfileButton() {
        UpdateProfileActivity_.intent(getActivity()).startForResult(SettingsActivity.UPDATE_PROFILE_ACTIVITY_CODE);
    }

    @ViewById(R.id.settings_profile_picture)
    NetworkImageView profileImageView;

    @ViewById(R.id.settings_username)
    RDTextView userNameTextView;

    @ViewById(R.id.settings_email)
    RDTextView userEmailTextView;

    @ViewById(R.id.settings_facebook_mapper)
    FacebookMapperView facebookMapperView;
    public FacebookMapperView getFacebookMapperView() {
        return facebookMapperView;
    }

    @AfterViews
    protected void afterSettingsFragmentView() {

        mRequestQueue = Volley.newRequestQueue(getActivity());
        mImageLoader = new ImageLoader(mRequestQueue, new ImageLoader.ImageCache() {
            @Override
            public Bitmap getBitmap(String s) {
                // No cache
                return null;
            }

            @Override
            public void putBitmap(String s, Bitmap bitmap) {
                // No cache
            }
        });

        BSProgressHUD.showProgressHUD(getActivity(), "LOADING");
        MyAccount.getInstance().fetchInfo(new MyAccount.MyAccountFetchInfoListener() {
            @Override
            public void didFinishFetchInfo(boolean success) {
                BSProgressHUD.dismissProgressHUD();
                if(success) {
                    onUpdateUI();
                }
            }
        });
    }

    @UiThread
    public void onUpdateUI() {
        MyAccount account = MyAccount.getInstance();
        /** API BUG */
        String avatarURL = account.getAccountAvatar();
        if(avatarURL != null && avatarURL.endsWith(".")) {
            MyAccount.getInstance().setAccountAvatar( avatarURL + "png" );
            account = MyAccount.getInstance();
        }
        profileImageView.setImageUrl(account.getAccountAvatar(), mImageLoader);
        userNameTextView.setText( account.getAccountScreenName() );
        userEmailTextView.setText( account.getAccountEmail() );

        // TODO: extract to res
        if(account.isFacebookStatusMapped()) {
            facebookMapperView.updateEmail(account.getAccountEmail());
            facebookMapperView.updateButton("ยกเลิก");
        }
        else {
            facebookMapperView.updateEmail("คุณยังไม่ได้เชื่อมต่อ");
            facebookMapperView.updateButton("เพิ่ม");
        }
    }

    /**
     *
     * Implement Methods
     *
     */
    @UiThread
    public void updateUserInfo(String username, String email) {
        userNameTextView.setText(username);
        userEmailTextView.setText(email);
    }

    @UiThread
    public void updateUserInfo() {
        String screenName = MyAccount.getInstance().getAccountScreenName();
        String avatarURL = MyAccount.getInstance().getAccountAvatar();

        if(screenName != null && screenName.length() > 0) {
            userNameTextView.setText( screenName );
        }
        if(avatarURL != null && avatarURL.length() > 0) {
            profileImageView.setImageUrl(avatarURL, mImageLoader);
        }
    }
}
