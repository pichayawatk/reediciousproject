package com.brainsourceapp.reedicious.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.util.Log;
import android.widget.EditText;

import com.brainsourceapp.reedicious.R;
import com.brainsourceapp.reedicious.beans.MyAccount;
import com.brainsourceapp.reedicious.rest.task.ResetPasswordRequestTask;
import com.brainsourceapp.reedicious.utils.BSAlertDialog;
import com.brainsourceapp.reedicious.utils.BSProgressHUD;
import com.brainsourceapp.reedicious.views.ButtonWithText;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.res.StringRes;

@EFragment(R.layout.login_fragment)
public class LoginFragment extends Fragment {

    private static final String TAG = LoginFragment.class.getSimpleName();

    public static LoginFragment newInstance() {
        return new LoginFragment_();
    }

    public interface LoginFragmentFBLoginListener {
        void didClickFBLoginButton();
    }
    public interface LoginFragmentAccountLoginListener {
        void didSuccessAccountLogin();
    }

    LoginFragmentFBLoginListener mListener;
    LoginFragmentAccountLoginListener mAccListener;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (LoginFragmentFBLoginListener) activity;
            mAccListener = (LoginFragmentAccountLoginListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(TAG + " : must implement LoginFragmentInterface." );
        }
    }

    @Click(R.id.login_with_facebook_button)
    public void onFacebookLoginButtonClicked() {
        mListener.didClickFBLoginButton();
    }

    @ViewById(R.id.login_email_edittext)
    EditText emailEditText;

    @Click(R.id.login_forget_password_text)
    public void onForgetPasswordClicked() {
        assert getActivity() != null;

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.app_name);
        builder.setMessage(R.string.please_input_your_email_to_receive_your_password);
        // Set an edittext view to get user input
        final EditText email = new EditText(getActivity());
        builder.setView(email);

        builder.setPositiveButton(R.string.reset_password, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // TODO: API HERE
                dialog.dismiss();
                if(email.getText() != null) {
                    String emailStr = email.getText().toString();
                    Log.d("test", "reset password email : " + emailStr + " sent");
                    requestResetPassword(emailStr);
                }
            }
        });
        builder.setNegativeButton(R.string.btn_cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public void requestResetPassword(String email) {
        String token = MyAccount.getInstance().getApiToken();
        Log.d("test", "on prepare request for reset password with token : " + token);
        new ResetPasswordRequestTask(getActivity(), token, email).execute();
    }

    @ViewById(R.id.login_password_edittext)
    EditText passwordEditText;

    @ViewById(R.id.login_login_btn)
    ButtonWithText loginBtn;

    @StringRes(R.string.login_email_label)
    String emailLabel;

    @StringRes(R.string.login_password_label)
    String passwordLabel;

    @Click(R.id.login_login_btn)
    public void clickDefaultLoginButton() {
        // TODO: default authen
        attempLogin();
    }

    public void attempLogin(String username, String password) {

        emailEditText.setText(username);
        passwordEditText.setText( password );

        attempLogin();
    }

    @UiThread
    public void attempLogin() {
        assert emailEditText.getText() != null && passwordEditText.getText() != null && getActivity() != null;

        String userEmail = emailEditText.getText().toString();
        String password = passwordEditText.getText().toString();

        boolean error = false;

        String errorMsg = "Error : unmapped friendly message :(";
        if(userEmail == null || userEmail.trim().length() == 0) {
            error = true;
            errorMsg = getActivity().getResources().getString(R.string.error_code_403);
        }

        else if(password == null || password.trim().length() == 0) {
            error = true;
            errorMsg = getActivity().getResources().getString(R.string.error_code_405);
        }

        assert getActivity() != null;
        if(error) {
            BSAlertDialog.newInstance( errorMsg, null ).show(getActivity().getFragmentManager(), "login_error");
        } else {

            // default login with no social
            BSProgressHUD.showProgressHUD(getActivity(), "LOADING");

            MyAccount.getInstance().login(userEmail, password, new MyAccount.MyAccountLoginListener() {
                @Override
                public void didLoginWithAccount(boolean success, String errorMsg) {
                    BSProgressHUD.dismissProgressHUD();

                    if(success) {
                       mAccListener.didSuccessAccountLogin();
                    } else {
                        BSAlertDialog.showEasyDialog(errorMsg).show(getActivity().getFragmentManager(), "error");
                    }
                }
            });
        }
    }

    @AfterViews
    void afterLoginViews() {
        loginBtn.bind("ตกลง", "");
    }


}
