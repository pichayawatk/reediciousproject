package com.brainsourceapp.reedicious.utils;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;

public class BSError {

    public static class UselessDialog extends DialogFragment {

        public static UselessDialog newInstance(String message, String btnTitle) {
            UselessDialog dialog = new UselessDialog();
            Bundle args = new Bundle();
            args.putString("message", message);
            args.putString("btnTitle", btnTitle);
            dialog.setArguments(args);
            return dialog;
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            String message = getArguments().getString("message", "");
            String btnTitle = getArguments().getString("btnTitle", "");

            return new AlertDialog.Builder(getActivity())
                    .setMessage( message )
                    .setPositiveButton(btnTitle, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    }).create();

        }
    }

}
