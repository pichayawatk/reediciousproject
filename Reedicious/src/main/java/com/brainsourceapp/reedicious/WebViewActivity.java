package com.brainsourceapp.reedicious;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Window;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.OptionsItem;
import org.androidannotations.annotations.OptionsMenu;
import org.androidannotations.annotations.ViewById;

@EActivity(R.layout.activity_web_view)
@OptionsMenu(R.menu.web_view)
public class WebViewActivity extends Activity {

    @Extra
    String url;

    @Extra
    String actionbarTitle;

    @ViewById(R.id.webview_webview)
    WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Adds Progress bar support
        this.getWindow().requestFeature(Window.FEATURE_PROGRESS);

        // Make progressBar Visible
        getWindow().setFeatureInt(Window.FEATURE_PROGRESS, Window.PROGRESS_VISIBILITY_ON);
    }

    @SuppressLint("SetJavaScriptEnabled")
    @AfterViews
    void afterWebViews() {

        Log.d("test", "webview url : " + url);

        setupActionBar();

        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setSupportZoom(true);
        webView.getSettings().setBuiltInZoomControls(true);

        webView.setWebViewClient(new MyWebClient(this));
        webView.setWebChromeClient(new MyWebChromeClient(this));

        webView.loadUrl( url );
    }

    @Override
    protected void onStop() {

        // Cancel webview for loading everything
        webView.stopLoading();
        super.onStop();
    }

    private void setupActionBar() {

        ActionBar actionBar = getActionBar();
        if(actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle( actionbarTitle );
        }
    }

    @OptionsItem(android.R.id.home)
    void didClickedHomeMenu() {
        try {
            NavUtils.navigateUpFromSameTask(this);
        } catch (Exception e) {
            finish();
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // Check if the key event was the Back button and if there's history
        if ((keyCode == KeyEvent.KEYCODE_BACK) && webView.canGoBack()) {
            webView.goBack();
            return true;
        }
        // If it wasn't the Back key or there's no web page history, bubble up to the default
        // system behavior (probably exit the activity)
        return super.onKeyDown(keyCode, event);
    }

    /**
     * MyWebClient class
     */
    private class MyWebClient extends WebViewClient {

        Context context;

        public MyWebClient(Context context) {
            this.context = context;
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            return false;
        }
    }

    /**
     * MyWebChrome class
     */
    private class MyWebChromeClient extends WebChromeClient {

        Context context;

        public MyWebChromeClient(Context context) {
            this.context = context;
        }

        @Override
        public void onProgressChanged(WebView view, int newProgress) {
            Activity activity = (Activity) context;
            ActionBar actionBar = activity.getActionBar();

            assert actionBar != null;
            actionBar.setTitle("LOADING");
            activity.setProgress(newProgress * 100);

            if(newProgress == 100) {
                actionBar.setTitle(getResources().getString(R.string.title_activity_lucky_list));
            }
        }
    }
}
