package com.brainsourceapp.reedicious.utils;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.os.CountDownTimer;
import android.support.v4.app.NavUtils;
import android.util.Log;

import com.brainsourceapp.reedicious.R;
import com.walnutlabs.android.ProgressHUD;

public class BSProgressHUD {

    public interface BSProgressHUDListener {
        void onHUDTimeOutListener();
    }

    private static ProgressHUD instance;
    private static CountDownTimer mTimer;
    private static Context mContext;
    private static BSProgressHUDListener mListener;

    public static void showProgressHUD(Context context, String message) {
        showProgressHUD(context, message, 15000);
    }

    public static void showProgressHUD(Context context, String message, int milliSec) {
        showProgressHUD(context, message, milliSec, null);
    }

    public static void showProgressHUD(Context context, String message, int milliSec, BSProgressHUDListener listener) {
        mContext = context;
        if(instance != null) {
            if(instance.isShowing()) {
                instance.dismiss();
            }
            instance = null;
        }

        if(mTimer != null) {
            mTimer.cancel();
            mTimer = null;
        }

        mListener = listener;

        instance = ProgressHUD.show(context, message, true, true, null);
        instance.setCanceledOnTouchOutside(false);
        mTimer = new CountDownTimer(milliSec, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                Log.d("test", "time elapsed : " + millisUntilFinished);
            }

            @Override
            public void onFinish() {
                try {
                    instance.dismiss();

                    if(mListener != null) {
                        mListener.onHUDTimeOutListener();
                        return;
                    }

                    BSAlertDialog.newInstance(mContext.getResources().getString(R.string.error_when_retrieving_data), "ยืนยัน", new BSAlertDialog.CallbackDialogListener() {
                        @Override
                        public void onBSDialogPosBtnClicked(DialogInterface dialog, String tag) {
                            try {
                                NavUtils.navigateUpFromSameTask( ((Activity) mContext) );
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }).show(((Activity) mContext).getFragmentManager(), "timeout");

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
    }

    public static void dismissProgressHUD() {
        if(instance != null && instance.isShowing()) {
            instance.dismiss();
        }
    }

    public static boolean isHUDShowing() {
        return (instance != null && instance.isShowing());
    }
}
