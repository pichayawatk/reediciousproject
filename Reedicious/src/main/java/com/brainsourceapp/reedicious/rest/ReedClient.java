package com.brainsourceapp.reedicious.rest;

import com.brainsourceapp.reedicious.rest.request.MemberUpdateRequest;
import com.brainsourceapp.reedicious.rest.request.SocialRegistrarRequest;
import com.brainsourceapp.reedicious.rest.response.MemberInfoResponse;
import com.brainsourceapp.reedicious.rest.response.MemberUpdateResponse;
import com.brainsourceapp.reedicious.rest.results.CampaignResult;
import com.brainsourceapp.reedicious.rest.results.ChickRestaurantResult;
import com.brainsourceapp.reedicious.rest.results.MenuResult;
import com.brainsourceapp.reedicious.rest.results.PlaceComment;
import com.brainsourceapp.reedicious.rest.results.PlaceDetail;
import com.brainsourceapp.reedicious.rest.results.PlacePromotion;
import com.brainsourceapp.reedicious.rest.results.SingleHotPromotionResult;
import com.brainsourceapp.reedicious.rest.results.SocialLoginResponse;
import com.brainsourceapp.reedicious.rest.results.SocialRegistrarResponse;
import com.brainsourceapp.reedicious.rest.results.TokenResult;

import org.androidannotations.annotations.rest.Get;
import org.androidannotations.annotations.rest.Post;
import org.androidannotations.annotations.rest.Put;
import org.androidannotations.annotations.rest.RequiresHeader;
import org.androidannotations.annotations.rest.Rest;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;

@Rest(rootUrl = "http://api.reedicious.com/", converters = {MappingJackson2HttpMessageConverter.class})
public interface ReedClient {

    void setHeader(String name, String value);
    String getHeader(String name);

    @Get("rest/auth/gettoken?appid=1&apikey=574936")
    TokenResult getToken();

    @Get("rest/search/promotion?appid=1&token={token}&page={page}&limit={limit}&latitude={lat}&longitude={lng}&cateid={categoryID}")
    SingleHotPromotionResult getPromotion(String token, String page, String limit, String lat, String lng, String categoryID);

    @Get("rest/promotion/{placeID}?appid=1&token={token}")
    PlacePromotion getPromotionByID(String placeID, String token);

    @Get("rest/search/place?appid=1&token={token}&latitude={latitude}&longitude={longitude}&distance={distance}&cateid={categoryID}&page={page}&limit={limit}")
    ChickRestaurantResult getPlaces(String token, String latitude, String longitude, String distance, String categoryID, String page, String limit);

    @Get("rest/place/{placeID}?appid=1&token={token}")
    PlaceDetail getPlaceDetail(String placeID, String token);

    @Get("rest/comment/place/{placeID}?appid=1&token={token}&page={page}&limit={limit}")
    PlaceComment getPlaceComments(String placeID, String token, String page, String limit);

    @Get("rest/campaign?appid=1&token={token}&page={page}&limit={limit}")
    CampaignResult getCampaigns(String token, String page, String limit);

    @Get("rest/search/menu?appid={appid}&token={token}&latitude={latitude}&longitude={longitude}&distance={distance}&cateid={cateid}&page={page}&limit={limit}")
    MenuResult getMenues(String appid, String token, double latitude, double longitude, String distance, int cateid, int page, int limit);

    @Get("rest/auth/memberlogin?appid=1&token={token}&social={social}")
    @RequiresHeader(value = {"username", "password"})
    SocialLoginResponse getSocialAccountLogin(String token, String social);

    @Post("rest/member/register?appid=1&token={token}")
    SocialRegistrarResponse postNewAccount(String token, SocialRegistrarRequest request);

    @Get("rest/member/getprofile?appid=1&token={token}")
    @RequiresHeader(value = {"accesstoken"})
    MemberInfoResponse getuserInfoByToken(String token);

    @Get("rest/auth/memberlogin?appid=1&token={token}")
    @RequiresHeader(value = {"username", "password"})
    SocialLoginResponse getdefaultLogin(String token);

    @Put("rest/member/update?appid=1&token={token}&social={social}")
    @RequiresHeader(value = {"accesstoken"})
    MemberUpdateResponse putFBMappingStatus(String token, String social, MemberUpdateRequest request);
}
