package com.brainsourceapp.reedicious.beans;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import com.brainsourceapp.reedicious.rest.ReedClient;
import com.brainsourceapp.reedicious.rest.results.CampaignResult;
import com.brainsourceapp.reedicious.rest.results.Commentator;
import com.brainsourceapp.reedicious.rest.results.PlaceComment;
import com.brainsourceapp.reedicious.rest.results.PlaceDetail;
import com.brainsourceapp.reedicious.rest.results.PlacePromotion;
import com.brainsourceapp.reedicious.rest.results.SingleHotPromotionResult;
import com.brainsourceapp.reedicious.rest.results.SocialLoginResponse;
import com.brainsourceapp.reedicious.rest.results.TokenResult;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;
import org.androidannotations.annotations.rest.RestService;
import org.springframework.web.client.RestClientException;

import java.util.ArrayList;

@EBean(scope = EBean.Scope.Singleton)
public class MyRestBean implements MyLifecycleBean.LifeBeanCallback {

    public MyRestBean() {

    }

    @RestService
    ReedClient reedClient;

    @RootContext
    Context context;

    public void updateApiToken() {
        TokenResult result = reedClient.getToken();

        MyAccount.getInstance().setApiToken( result.data.token );
    }

    /**
     *  GET_PROMOTIONS_BY_LOCATION
     */
    public ArrayList<SingleHotPromotionResult.Data> getPromotionsByLocation(double _lat, double _lng, int _page, int _limit) {
        String limit = String.valueOf(_limit);
        String page = String.valueOf(_page);
        String categoryID = "";
        String lat = String.valueOf( _lat );
        String lng = String.valueOf( _lng );

        try {
            SingleHotPromotionResult results = reedClient.getPromotion(MyAccount.getInstance().getApiToken(), page, limit, lat, lng, categoryID);

            /** Successful */
            if(results.header.code == 200 && results.datas != null) {

                Log.d("test", "successful");

                return results.datas;
            }

            /** Can get error code */
            if(results.header != null) {

                // Token Timeout
                if(results.header.code == 405) {

                    Log.d("test", "Token Timeout");
                    updateApiToken();

                    results = reedClient.getPromotion(MyAccount.getInstance().getApiToken(), page, limit, lat, lng, categoryID);

                    return results.datas;
                }

                // No Content
                if(results.header.code == 204) {

                    Log.d("test", "No Content");

                    return null;
                }

            }

            /** Cannot get error code, default as no content */
            return null;

        } catch (Exception e) {
            Log.d("test", "get single hot promotion error : " + e.getMessage());

            return null;
        }
    }

    /**
     *  GET_PROMOTIONS_BY_ID
     */
    public PlacePromotion.Data getPromotionByID(int _placeID) {
        String placeID = String.valueOf( _placeID );

        try {
            String log = String.format("\n\ngetPromotionByID\n%s\nappid : 1\ntoken : %s", placeID, MyAccount.getInstance().getApiToken());
            Log.d("test", log);

            PlacePromotion result = reedClient.getPromotionByID(placeID, MyAccount.getInstance().getApiToken());

            /** Successful */
            if(result.header.code == 200 && result.data != null) {

                Log.d("test", "successful");

                return result.data;
            }

            /** Can get error code */
            if(result.header != null) {

                // Token Timeout
                if(result.header.code == 405) {

                    Log.d("test", "Token Timeout");
                    updateApiToken();

                    result = reedClient.getPromotionByID(placeID, MyAccount.getInstance().getApiToken());

                    return result.data;
                }

                // No Content
                if(result.header.code == 204) {

                    Log.d("test", "No Content");

                    return null;
                }
            }

            /** Cannot get error code, default as no content */
            return null;

        } catch (RestClientException e) {

            Log.d("test", "getPromotionByID : " + e.getMessage());
        }


        return null;
    }

    /**
     *  GET_PLACE_DETAIL
     */
    public PlaceDetail.Data getPlaceDetail(int _placeID) {

        String placeID = String.valueOf( _placeID );

            try {
                String log = String.format("\n\nappid : 1\n\ntoken : %s\n\n: placeID : %s", MyAccount.getInstance().getApiToken(), placeID);
                Log.d("test", "getPlaceDetail : " + log);

                PlaceDetail place = reedClient.getPlaceDetail( placeID, MyAccount.getInstance().getApiToken() );

                /** Successful get data */
                if(place.header.code == 200 && place.data != null) {

                    return place.data;
                }

                /** Can get error code */
                if(place.header != null) {

                    // Token Timeout
                    if(place.header.code == 405) {

                        Log.d("test", "Token Timeout");
                        updateApiToken();

                        place = reedClient.getPlaceDetail( placeID, MyAccount.getInstance().getApiToken() );

                        return place.data;
                    }

                    // No Content
                    if(place.header.code == 204) {

                        Log.d("test", "No Content");

                        return new PlaceDetail.Data();
                    }
                }
            } catch (RestClientException e) {

                Log.d("test", "RestClientException : " + e.getMessage());
            } catch (Exception e) {

                Log.d("test", "Exception : " + e.getMessage());
            }

        return null;
    }

    /**
     *  GET_COMMENTATORS
     */
    public ArrayList<Commentator> getCommentators(String placeID, int _page, int _limit) {

        String page = String.valueOf( _page );
        String limit = String.valueOf( _limit );

        try {

            PlaceComment comment = reedClient.getPlaceComments(placeID, MyAccount.getInstance().getApiToken(), page, limit);

            if(comment.header.code == 200) {

                return comment.data;
            }

            /** Can get error code */
            if(comment.header != null) {

                // Token Timeout
                if(comment.header.code == 405) {

                    Log.d("test", "Token Timeout");
                    updateApiToken();

                    comment = reedClient.getPlaceComments(placeID, MyAccount.getInstance().getApiToken(), page, limit);

                    return comment.data;
                }

                // No Content
                if(comment.header.code == 204) {

                    Log.d("test", "No Content");

                    return new ArrayList<Commentator>();
                }

                // EOF
                if(comment.properties.current_page > comment.properties.total_page) {

                    Log.d("test", "Empty data");

                    return new ArrayList<Commentator>();
                }
            }

        } catch (RestClientException e) {

            Log.d("test", "RestClientException : " + e.getMessage());
        } catch (Exception e) {

            Log.d("test", "Exception : " + e.getMessage());
        }

        return null;
    }

    /**
     *  GET_CAMPAIGNS
     */
    public CampaignResult getCampaigns(int _page, int _limit) {

        String appid = "1";

        String page = String.valueOf(_page);

        String limit = String.valueOf(_limit);

        try {
            String log = String.format("\n\nappid : %s\n\ntoken : %s\n\npage : %s\n\nlimit : %s\n\n", MyAccount.getInstance().getApiToken(), page, limit);
            Log.d("test", "getMenus : " + log);

            CampaignResult result = reedClient.getCampaigns(MyAccount.getInstance().getApiToken(), page, limit);

            /** Successful get data */
            if(result.header.code == 200 && result.data != null) {

                return result;
            }

            /** Can get error code */
            if(result.header != null) {

                // Token Timeout
                if(result.header.code == 405) {

                    Log.d("test", "Token Timeout");
                    updateApiToken();

                    result = reedClient.getCampaigns(MyAccount.getInstance().getApiToken(), page, limit);

                    return result;
                }

                // No Content
                if(result.header.code == 204) {

                    Log.d("test", "No Content");

                    return new CampaignResult();
                }
            }
        } catch (RestClientException e) {

            Log.d("test", "RestClientException : " + e.getMessage());
        } catch (Exception e) {

            Log.d("test", "Exception : " + e.getMessage());
        }

        return null;
    }

    /**
     *  GET_DEFAULT_LOGIN
     */
    public SocialLoginResponse getdefaultLogin(String email, String password) {
        reedClient.setHeader("username", email);
        reedClient.setHeader("password", password);

        try {

            SocialLoginResponse response = reedClient.getdefaultLogin(MyAccount.getInstance().getApiToken());
            SocialLoginResponse.SocialLoginResponseData item = response.data.get(0);

            if(response != null && response.header != null && response.header.code == 200 && response.data != null && item.accesstoken != null && item.accesstoken.trim().length() > 0) {

                return response;
            } else {

                updateApiToken();

                return reedClient.getdefaultLogin(MyAccount.getInstance().getApiToken());
            }

        } catch (Exception e) {

            updateApiToken();

            return reedClient.getdefaultLogin(MyAccount.getInstance().getApiToken());

        }
    }

    /**
     *
     * RESET_PASSWORD_REQUEST
     *
     */

    @Override
    public void onApplicationMovedToBackground(Activity activity) {
        Log.d("test", "application moved to background with activity : " + activity.getLocalClassName());
    }

    @Override
    public void onApplicationMovedToForeground(Activity activity) {
        Log.d("test", "application moved to foreground with activity : " + activity.getLocalClassName());
    }
}