package com.brainsourceapp.reedicious.utils;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.content.DialogInterface;
import android.os.Bundle;

import com.brainsourceapp.reedicious.R;

/**
 *
 *  Available only on Activity ( Will crash the app if you use on fragment )
 *
 *  Author.
 *  fChopin.
 */
public class BSAlertDialog {

    public interface BSAlertCallback {
        void onPositiveButtonClicked(DialogInterface dialog);
        void positiveCallback();
    }

    public interface CallbackDialogListener {
        void onBSDialogPosBtnClicked(DialogInterface dialog, String tag);
    }

    public static BSAlertDialog newInstance() {
        return new BSAlertDialog();
    }

    public static SimpleAlertDialogFragment showEasyDialog(String msg) {
        return SimpleAlertDialogFragment.newInstance(msg);
    }

    public static SimpleAlertDialogFragment showEasyDialog(int resmsgID) {
        return SimpleAlertDialogFragment.newInstance(resmsgID);
    }

    public static CallbackDialogFragment newInstance(String msg, CallbackDialogListener callback) {
        return CallbackDialogFragment.newInstance(msg, callback);
    }

    public static CallbackDialogFragment newInstance(int resID, CallbackDialogListener callback) {
        return CallbackDialogFragment.newInstance(resID, callback);
    }

    public static CallbackDialogFragment newInstance(String msg, CallbackDialogListener callback, String tag) {
        return CallbackDialogFragment.newInstance(msg, callback, tag);
    }

    public static CallbackDialogFragment newInstance(String msg, String btnMsg, CallbackDialogListener callback) {
        return CallbackDialogFragment.newInstance(msg, btnMsg, callback);
    }

    public static CallbackDialogFragment newInstance(String msg, String positiveBtn, String negativeBtn, CallbackDialogListener callback) {
        return CallbackDialogFragment.newInstance(msg, positiveBtn, negativeBtn, callback);
    }

    public ForceExitDialogFragment forceexitDialog() {
        return ForceExitDialogFragment.newInstance();
    }

    public BaseAlertDialogFragment simpleDialog(String msg, String okMsg, String cancelMsg, boolean callback, Fragment callbackFragment) {
        return BaseAlertDialogFragment.newInstance(msg, okMsg, cancelMsg, callback, callbackFragment);
    }

    public SimpleAlertDialogFragment easyDialog(int resID) {
        return SimpleAlertDialogFragment.newInstance(resID);
    }

    public SimpleAlertDialogFragment easyDialog(String msg) {
        return SimpleAlertDialogFragment.newInstance(msg);
    }

    public static class CallbackDialogFragment extends DialogFragment {

        public static CallbackDialogFragment newInstance(int msgID, CallbackDialogListener callback) {
            return new CallbackDialogFragment(msgID, R.string.ok_th, callback);
        }

        public static CallbackDialogFragment newInstance(int msgID, int okbtnID, CallbackDialogListener callback) {
            return new CallbackDialogFragment(msgID, okbtnID, callback);
        }

        public static CallbackDialogFragment newInstance(String msg, CallbackDialogListener callback) {
            return new CallbackDialogFragment(msg, callback, null);
        }

        public static CallbackDialogFragment newInstance(String msg, CallbackDialogListener callback, String tag) {
            return new CallbackDialogFragment(msg, callback, tag);
        }

        public static CallbackDialogFragment newInstance(String msg, String positiveBtnMsg, CallbackDialogListener callback) {
            return new CallbackDialogFragment(msg, positiveBtnMsg, callback);
        }

        public static CallbackDialogFragment newInstance(String msg, String okMsg, String cancelMsg, CallbackDialogListener callback) {
            return new CallbackDialogFragment(msg, okMsg, cancelMsg, callback);
        }

        CallbackDialogListener callback;
        int msgID;
        int okbtnID;
        int cancelbtnID;
        String tag;
        String msg;
        String positiveBtnMsg;
        String negativeBtnMsg;

        private CallbackDialogFragment(String msg, int okBtnID, CallbackDialogListener callback) {
            this.callback = callback;
            this.okbtnID = okBtnID;
            this.msg = msg;
        }

        private CallbackDialogFragment(String msg, String positiveBtnMsg, CallbackDialogListener callback) {
            this.callback = callback;
            this.positiveBtnMsg = positiveBtnMsg;
            this.msg = msg;
        }

        private CallbackDialogFragment(String msg, CallbackDialogListener callback, String tag) {
            this.callback = callback;
            this.msg = msg;
            this.okbtnID = R.string.btn_ok;
            this.cancelbtnID = R.string.btn_cancel;
            this.tag = tag;
        }

        private CallbackDialogFragment(int msgID, int okbtnID, CallbackDialogListener callback) {
            this.callback = callback;
            this.msgID = msgID;
            this.okbtnID = okbtnID;
            this.cancelbtnID = R.string.btn_cancel;
        }

        private CallbackDialogFragment(String msg, int okbtnID, int cancelbtnID, CallbackDialogListener callback) {
            this.callback = callback;
            this.msg = msg;
            this.okbtnID = okbtnID;
            this.cancelbtnID = cancelbtnID;
        }

        private CallbackDialogFragment(String msg, String positiveBtn, String negativeBtn, CallbackDialogListener callback) {
            this.callback = callback;
            this.msg = msg;
            this.positiveBtnMsg = positiveBtn;
            this.negativeBtnMsg = negativeBtn;
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            assert getActivity() != null && getArguments() != null;
            if(this.msg == null || this.msg.trim().length() == 0) {
                this.msg = getActivity().getResources().getString(this.msgID);
            }
            String okMsg = this.positiveBtnMsg;
            if(okMsg == null) {
                okMsg = getActivity().getResources().getString(okbtnID);
            }
            String cancelMsg = this.negativeBtnMsg;
            if(cancelMsg == null) {
                cancelMsg = getActivity().getResources().getString(cancelbtnID);
            }
            return new AlertDialog.Builder(getActivity())
                    .setTitle(R.string.app_name)
                    .setMessage(this.msg)
                    .setPositiveButton(okMsg, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if(callback == null) {
                                dialog.dismiss();
                            } else {
                                callback.onBSDialogPosBtnClicked(dialog, tag);
                            }
                        }
                    })
                    .setNegativeButton(cancelMsg, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    }).create();
        }
    }

    public static class SimpleAlertDialogFragment extends DialogFragment {
        public static SimpleAlertDialogFragment newInstance(int resID) {
            SimpleAlertDialogFragment fragment = new SimpleAlertDialogFragment();
            Bundle bundle = new Bundle();
            bundle.putInt("resID", resID);
            fragment.setArguments(bundle);
            return fragment;
        }

        public static SimpleAlertDialogFragment newInstance(String msg) {
            SimpleAlertDialogFragment fragment = new SimpleAlertDialogFragment();
            Bundle bundle = new Bundle();
            bundle.putString("msg", msg);
            fragment.setArguments(bundle);
            return fragment;
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            assert getActivity() != null && getArguments() != null;
            String msg = "";
            if(getArguments().containsKey("msg")) {
                msg = getArguments().getString("msg");
            }
            else if(getArguments().containsKey("resID")) {
                msg = getActivity().getResources().getString( getArguments().getInt("resID") );
            }
            return new AlertDialog.Builder(getActivity())
                    .setTitle(getActivity().getResources().getString(R.string.app_name))
                    .setMessage( msg )
                    .setNegativeButton(getActivity().getResources().getString(R.string.btn_ok), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    })
                    .create();
        }
    }

    public static class BaseAlertDialogFragment extends DialogFragment {

        Object callback;

        public static BaseAlertDialogFragment newInstance(String msg, String okMsg, String cancelMsg, boolean willexit, Object callback) {
            BaseAlertDialogFragment fragment = new BaseAlertDialogFragment();
            Bundle bundle = new Bundle();
            if(msg != null)                     bundle.putString("msg", msg);
            if(okMsg != null)                   bundle.putString("okMsg", okMsg);
            if(cancelMsg != null)               bundle.putString("cancelMsg", cancelMsg);
            bundle.putBoolean("willexit", willexit);
            if(callback != null)                fragment.callback = callback;

            fragment.setArguments( bundle );
            return fragment;
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            assert getActivity() != null && getArguments() != null;
            boolean posBtn = false;
            boolean negBtn = false;
            String msg = null;
            if(getArguments().containsKey("okMsg"))         posBtn = true;
            if(getArguments().containsKey("cancelMsg"))     negBtn = true;
            if(getArguments().containsKey("msg"))           msg = getArguments().getString("msg");

            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

            builder.setTitle(getActivity().getResources().getString(R.string.app_name));

            if(msg == null || msg.trim().length() == 0)         msg = "LOADING";
            builder.setMessage( msg );

            if(posBtn)  builder.setPositiveButton(getArguments().getString("okMsg"), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if(callback != null) {
                        ((BSAlertCallback) callback).onPositiveButtonClicked( dialog );
                    } else {
                        dialog.dismiss();

                        if(getArguments().getBoolean("willexit")) {
                            getActivity().finish();
                        }
                    }
                }
            });

            if(negBtn)  builder.setNegativeButton(getArguments().getString("cancelMsg"), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });

            return builder.create();
        }
    }

    public static class ForceExitDialogFragment extends DialogFragment {
        public static ForceExitDialogFragment newInstance() {
            return new ForceExitDialogFragment();
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            assert getActivity() != null;
            return new AlertDialog.Builder(getActivity())
                    .setTitle(getActivity().getResources().getString(R.string.app_name))
                    .setMessage(getActivity().getResources().getString(R.string.null_response_error))
                    .setPositiveButton("ปิด", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            getActivity().finish();
                        }
                    })
                    .setCancelable(false)
                    .create();
        }
    }
}
