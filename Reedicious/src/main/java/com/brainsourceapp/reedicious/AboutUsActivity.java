package com.brainsourceapp.reedicious;

import android.app.Activity;
import android.support.v4.app.NavUtils;
import android.widget.TextView;

import com.brainsourceapp.reedicious.beans.MyAccount;
import com.brainsourceapp.reedicious.utils.BSDialogIntentBuilder;
import com.flurry.android.FlurryAgent;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.OptionsItem;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.res.StringRes;

import java.util.HashMap;
import java.util.Map;

@EActivity(R.layout.activity_about_us)
public class AboutUsActivity extends Activity {

    @StringRes(R.string.app_version_name)
    String versionName;

    @ViewById(R.id.about_version)
    TextView aboutVersionTextView;

    @AfterViews
    void afterAboutUsViews() {

        assert getActionBar() != null;
        getActionBar().setTitle("About");

        aboutVersionTextView.setText("Version : " + versionName);

        Map<String, String> param = new HashMap<String, String>();
        param.put("screenName", MyAccount.getInstance().getAccountScreenName());
        FlurryAgent.logEvent("View About Page", param);
    }

    @Click(R.id.phone_number_link)
    public void onPhoneNumberClicked() {
        BSDialogIntentBuilder.newInstance(this).openPhoneNumbersIntent(new CharSequence[]{"025026767"});
    }

    @OptionsItem(android.R.id.home)
    void didClickedHomeMenu() {
        try {
            NavUtils.navigateUpFromSameTask(this);
        } catch (Exception e) {
            finish();
        }
    }
}
