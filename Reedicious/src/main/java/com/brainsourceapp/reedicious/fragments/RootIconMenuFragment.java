package com.brainsourceapp.reedicious.fragments;

import android.app.Fragment;
import android.os.Build;
import android.util.Log;
import android.widget.ImageView;

import com.brainsourceapp.reedicious.AboutUsActivity_;
import com.brainsourceapp.reedicious.ChickRestaurantsActivity_;
import com.brainsourceapp.reedicious.LuckyListActivity_;
import com.brainsourceapp.reedicious.PromotionsPagerActivity_;
import com.brainsourceapp.reedicious.QRCodeActivity_;
import com.brainsourceapp.reedicious.R;
import com.brainsourceapp.reedicious.RecommendedActivity_;
import com.brainsourceapp.reedicious.SettingsActivity_;
import com.brainsourceapp.reedicious.TermAndConditionActivity_;
import com.brainsourceapp.reedicious.beans.MyAccount;
import com.brainsourceapp.reedicious.beans.MyRestBean;
import com.brainsourceapp.reedicious.rest.response.Campaign;
import com.brainsourceapp.reedicious.rest.response.CampaignsResponse;
import com.brainsourceapp.reedicious.rest.task.GetCampaignsTask;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import java.util.Calendar;

@EFragment(R.layout.root_icon_menu)
public class RootIconMenuFragment extends Fragment {

    public static RootIconMenuFragment newInstance() {
        return new RootIconMenuFragment_();
    }

    public ImageView myImage;

    @Bean
    MyRestBean myRestBean;

    @Click(R.id.root_icon_menu_chick_recipe_button)
    void didClickChickRecipeButton() {
        ChickRestaurantsActivity_.intent(getActivity()).start();
    }

    @Click(R.id.root_icon_menu_hot_promotion_button)
    void didClickHotPromotionsButton() {
        PromotionsPagerActivity_.intent(getActivity()).start();
    }

    @Click(R.id.root_icon_menu_recommended_button)
    void didClickRecommendedButton() {
        RecommendedActivity_.intent(getActivity()).start();
    }

    @ViewById(R.id.root_icon_menu_current_campaign_icon)
    ImageView campaignImgView;

    @Click(R.id.root_icon_menu_current_campaign_icon)
    void didClickFreeButton() {
        TermAndConditionActivity_.intent(getActivity()).start();
    }

    @Click(R.id.root_icon_menu_winner_list_button)
    void didClickWinnerListButton() {
        LuckyListActivity_.intent(getActivity()).start();
    }

    @Click(R.id.root_icon_menu_settings_button)
    void didClickSettingsButton() {
        SettingsActivity_.intent(getActivity()).start();
    }

    @Click(R.id.root_icon_menu_i_button)
    void didClickIButton() {

        AboutUsActivity_.intent(getActivity()).start();
    }

    @Click(R.id.root_icon_menu_qr)
    void didClickScanQRButton() {

        QRCodeActivity_.intent(getActivity()).start();
    }

    @AfterViews
    void afterRootIconMenuViews() {;

        String apiToken = MyAccount.getInstance().getApiToken();

        GetCampaignsTask task = new GetCampaignsTask( "1", apiToken, new GetCampaignsTask.CampaignTaskCallback() {
            @Override
            public void onFinishGetCampaign(CampaignsResponse response) {
                if(response != null) {
                    updateUI(response);
                }
            }
        });
        task.execute();
    }

    @UiThread
    void updateUI(CampaignsResponse response) {

        if(response != null && response.getCampaigns() != null && response.getCampaigns().size() > 0) {

            Campaign campaign = response.getCampaigns().get(0);

            int campaignID = Integer.parseInt(campaign.getId());
            String title = campaign.getTitle();
            String url = campaign.getCampaign_icon();
            String imageLink = campaign.getCampaign_url();
            String desc = campaign.getJoin_campaign();
            String cover = campaign.getCover();

            // Check is this new ?
            if(MyAccount.getInstance().getCampaignID().equals(campaignID + "")) {

                Log.d("test", "This is the current version of K.Reed Campaign... nothing to do here...");

                /** Check is expired */
                int current = (int)(Calendar.getInstance().getTimeInMillis() / 1000);
                Log.d("test", "campaign expired : " + campaign.getEnd() + ", current is : " + current);
                if(current > campaign.getEnd()) {
                    updateExpiredCampaign();
                }
                return;
            }

            /** Check is expired */
            int current = (int)(Calendar.getInstance().getTimeInMillis() / 1000);
            Log.d("test", "campaign expired : " + campaign.getEnd() + ", current is : " + current);
            if(current > campaign.getEnd()) {
                updateExpiredCampaign();
                return;
            }

            // Save data to sharedPref
            MyAccount.getInstance().saveCampaign(campaign);

            myImage = new ImageView(getActivity());

            int w = campaignImgView.getWidth();
            int h = campaignImgView.getHeight();

            Picasso.with(getActivity()).load(url).resize(w, h).centerCrop().into(myImage, new Callback() {
                @Override
                public void onSuccess() {

                    updateCampaignButton();
                }

                @Override
                public void onError() {

                }
            });
        }
    }

    @UiThread
    public void updateExpiredCampaign() {
        Log.d("test", "update expired campaign");

        campaignImgView.setImageResource( R.drawable.menu_button_comingsoon );
        campaignImgView.setEnabled( false );
    }

    @SuppressWarnings("deprecation")
    @UiThread
    void updateCampaignButton() {

        Log.d("test", "updating button");
        if(android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
            campaignImgView.setBackgroundDrawable(myImage.getDrawable());
        } else {
            campaignImgView.setBackground(myImage.getDrawable());
        }
        campaignImgView.setEnabled(true);
    }

    @Override
    public void onDestroy() {
        Log.d("test", "RootIconMenu : cancel update campaign...");
        super.onDestroy();
    }
}