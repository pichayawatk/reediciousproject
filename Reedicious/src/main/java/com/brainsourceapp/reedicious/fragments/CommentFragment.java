package com.brainsourceapp.reedicious.fragments;

import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.Toast;

import com.brainsourceapp.reedicious.DetailActivity;
import com.brainsourceapp.reedicious.R;
import com.brainsourceapp.reedicious.SettingsActivity_;
import com.brainsourceapp.reedicious.beans.MyAccount;
import com.brainsourceapp.reedicious.beans.MyRestBean;
import com.brainsourceapp.reedicious.rest.response.CommentResponse;
import com.brainsourceapp.reedicious.rest.results.Commentator;
import com.brainsourceapp.reedicious.rest.results.PlaceDetail;
import com.brainsourceapp.reedicious.rest.task.CommentTask;
import com.brainsourceapp.reedicious.utils.BSAlertDialog;
import com.brainsourceapp.reedicious.utils.BSProgressHUD;
import com.brainsourceapp.reedicious.views.CommentAdapter;
import com.brainsourceapp.reedicious.views.CommentListView;
import com.brainsourceapp.reedicious.views.CommentUserHeader;
import com.brainsourceapp.reedicious.views.RDEditText;
import com.flurry.android.FlurryAgent;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.OnActivityResult;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@EFragment(R.layout.fragment_comment)
public class CommentFragment extends Fragment {

    public static CommentFragment newInstance(PlaceDetail.Data mData) {
        CommentFragment fragment = new CommentFragment_();
        fragment.placeID = String.valueOf( mData.id );
        fragment.page = 1;
        fragment.limit = 10;
        return fragment;
    }

    public boolean isOnScreen() {
        return isOnScreen;
    }

    public void setOnScreen(boolean onScreen) {
        isOnScreen = onScreen;
    }

    private boolean isOnScreen;
    private boolean canLoadDataAsync;
    public boolean canLoadDataAsync() {
        return canLoadDataAsync;
    }
    public void setCanLoadDataAsync(boolean load) {
        canLoadDataAsync = load;
    }

    @OnActivityResult(DetailActivity.REQUEST_LOGIN_CODE_FROM_COMMENT)
    public void activityResultFromLogin() {
        Log.d("test", "Result from login.");
        if(MyAccount.getInstance().isLoggedIn()) {
            commentHeader.showLoggedInUser( true );
            unloginButton.setVisibility(View.GONE);
        } else {
            commentHeader.showLoggedInUser( false );
            unloginButton.setVisibility(View.VISIBLE);
        }
    }

    String placeID;
    int page;
    int limit;

    @Bean
    MyRestBean restBean;

    @ViewById(R.id.comment_commentators_list)
    CommentListView commentList;

    @Bean
    CommentAdapter adapter;

    @ViewById(R.id.comment_userHeader)
    CommentUserHeader commentHeader;

    // Buffered data
    ArrayList<Commentator> bufferedCommentatorList;

    boolean isBackgroundRunning;

    @ViewById(R.id.comment_input_box)
    RDEditText commentInputBox;

    @ViewById(R.id.comment_unlogin_button)
    Button unloginButton;

    @Click(R.id.comment_unlogin_button)
    public void unloginTyped() {

        if(!MyAccount.getInstance().isLoggedIn()) {
            commentInputBox.clearFocus();
            assert getActivity() != null;
            String loginMsg = getActivity().getResources().getString(R.string.error_require_login_to_use_feature);
            BSAlertDialog.newInstance(loginMsg, new BSAlertDialog.CallbackDialogListener() {
                @Override
                public void onBSDialogPosBtnClicked(DialogInterface dialog, String tag) {
                    dialog.dismiss();

                    SettingsActivity_.intent(getActivity()).startForResult( DetailActivity.REQUEST_LOGIN_CODE_FROM_COMMENT );
                }
            }).show(getActivity().getFragmentManager(), "unauthen");
        }
    }

    @Click(R.id.comment_add_button)
    public void willPostForComment() {
        // TODO :
        /**
         * 1. close keyboard
         * 2. prepare data for api
         * 3. show progress dialog
         * 4. start background thread
         */
        assert getActivity() != null;
        InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow( commentInputBox.getWindowToken(), 0 );

        assert commentInputBox.getText() != null;
        String text = commentInputBox.getText().toString();

        String userToken = MyAccount.getInstance().getAccountToken();

        String apiToken = MyAccount.getInstance().getApiToken();

        // TODO: Alert No Input
        if(text == null || text.trim().length() == 0) {

            Log.d("test", "type something...");
            return;
        }

        // TODO: Alert no userToken!
        if(userToken == null || userToken.trim().length() == 0) {

            assert getActivity() != null;

            BSAlertDialog.newInstance(getActivity().getResources().getString(R.string.error_require_login_to_use_feature), getActivity().getResources().getString(R.string.ok_th), new BSAlertDialog.CallbackDialogListener() {
                @Override
                public void onBSDialogPosBtnClicked(DialogInterface dialog, String tag) {
                    dialog.dismiss();

                    SettingsActivity_.intent(getActivity()).startForResult( DetailActivity.REQUEST_LOGIN_CODE_FROM_COMMENT );
                }
            }).show(getActivity().getFragmentManager(), "alert");
            return;
        }

        // TODO: Yes man, can send comment now
        BSProgressHUD.showProgressHUD(getActivity(), "LOADING");

        new CommentTask(text, new CommentTask.CommentListener() {
            @Override
            public void onCommentResponse(CommentResponse response) {

                onSendCommentResponse( response );
            }
        }).execute(placeID, "1", apiToken, userToken);
    }

    @UiThread
    public void onSendCommentResponse(CommentResponse response) {
        BSProgressHUD.dismissProgressHUD();

        if(response != null) {

            assert commentInputBox.getEditableText() != null;
            String data = commentInputBox.getEditableText().toString();
            commentInputBox.setText("");
            commentInputBox.clearFocus();

            Map<String, String> param = new HashMap<String, String>();
            param.put("screenName", MyAccount.getInstance().getAccountScreenName());
            param.put("data", data);
            FlurryAgent.logEvent("Post Comment in Details Page", param);

            reloadPage();
        } else {

            assert getActivity() != null;
            Toast.makeText(getActivity(), "Comment failed", Toast.LENGTH_LONG).show();
        }
    }

    @UiThread
    public void reloadPage() {

        // Reset all values
        page = 1;
        limit = 10;

        if(adapter != null && adapter.getCount() > 0) {
            adapter.clearData();
        }

        loadBufferedData();
    }

    @AfterViews
    public void afterInjectedCommentViews() {

        // Set title
        //Activity activity = getActivity();
        //ActionBar actionBar = activity.getActionBar();
        //actionBar.setTitle("Comments");

        if(MyAccount.getInstance().isLoggedIn()) {
            commentHeader.showLoggedInUser( true );
            unloginButton.setVisibility(View.GONE);
        } else {
            commentHeader.showLoggedInUser( false );
            unloginButton.setVisibility(View.VISIBLE);
        }

        // Init params
        page = 1;

        // Init object
        bufferedCommentatorList = new ArrayList<Commentator>();

        // Note: new empty adapter
        commentList.setAdapter( adapter );

        loadBufferedData();
    }

    @Override
    public void onResume() {
        super.onResume();

        if(MyAccount.getInstance().isLoggedIn()) {
            commentHeader.showLoggedInUser( true );
            unloginButton.setVisibility(View.GONE);
        } else {
            commentHeader.showLoggedInUser( false );
            unloginButton.setVisibility(View.VISIBLE);
        }
    }

    @Background
    void loadBufferedData() {
        isBackgroundRunning = true;
        bufferedCommentatorList = restBean.getCommentators(placeID, page, limit);
        isBackgroundRunning = false;

        if(this.isOnScreen) {
            updateAdapter();
        }
    }

    @UiThread
    void updateAdapter() {
        Log.d("test", "page count : " + page);

        if(bufferedCommentatorList.size() > 0) {
            page++;
            setCanLoadDataAsync(true);

            for(Commentator item : bufferedCommentatorList) {
                adapter.addItem(item);
            }
            adapter.notifyDataSetChanged();

            clearBuffer();

            Map<String, String> param = new HashMap<String, String>();
            param.put("screenName", MyAccount.getInstance().getAccountScreenName());
            param.put("placeTitle", placeID + "");
            param.put("page", (page - 1) + "");
            FlurryAgent.logEvent("Read Comment in Details Page", param);
        }
    }

    @UiThread
    public void joinWork() {

        Log.d("test", "join work");

        if(!isBackgroundRunning && bufferedCommentatorList != null) {
            updateAdapter();
        }
    }

    void clearBuffer() {
        bufferedCommentatorList.clear();
    }

    public void onLastViewShowed() {
        Log.d("test", "OnLastViewShowed heard!");
        if(canLoadDataAsync()) {
            loadBufferedData();
        }
    }
}
