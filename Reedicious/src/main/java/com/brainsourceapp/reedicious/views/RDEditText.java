package com.brainsourceapp.reedicious.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.EditText;

import com.brainsourceapp.reedicious.R;

public class RDEditText extends EditText {


    public RDEditText(Context context) {
        super(context);
    }

    public RDEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        if(!isInEditMode()) {
            parseAttributes(context, attrs);
        }
    }

    public RDEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        if(!isInEditMode()) {
            parseAttributes(context, attrs);
        }
    }

    private void parseAttributes(Context context, AttributeSet attrs) {
        TypedArray values = context.obtainStyledAttributes(attrs, R.styleable.RDEditText);

        assert values != null;
        int typeface = values.getInt(R.styleable.RDEditText_typeface, 0);

        switch (typeface) {
            case 0:
                setTypeface(RDTextView.getFontRegular(context));
                break;
            case 1:
                setTypeface(RDTextView.getFontBold(context));
                break;
            case 2:
                setTypeface(RDTextView.getFontBoldItalic(context));
                break;
            case 3:
                setTypeface(RDTextView.getFontItalic(context));
                break;
        }
    }
}
