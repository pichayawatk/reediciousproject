package com.brainsourceapp.reedicious.rest.task;

import android.os.AsyncTask;
import android.util.Log;

import com.brainsourceapp.reedicious.rest.response.CommentResponse;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.net.URLEncoder;

public class CommentTask extends AsyncTask<String, String, String> {

    public static final String TAG = CommentTask.class.getSimpleName();

    public interface CommentListener {
        public void onCommentResponse(CommentResponse response);
    }

    private MultiValueMap<String, Object> formData;
    private CommentListener listener;

    public CommentTask(String comment, CommentListener listener) {

        this.listener = listener;
        formData = new LinkedMultiValueMap<String, Object>();
        formData.add("comment", comment);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();


    }

    @Override
    protected String doInBackground(String... params) {

        try {

            String placeID = params[0];
            String appid = params[1];
            String token = params[2];
            String userToken = params[3];

            try {
                placeID = URLEncoder.encode(params[0], "UTF-8");
                appid = URLEncoder.encode(params[1], "UTF-8");
                token = URLEncoder.encode(params[2], "UTF-8");
            } catch (Exception e) {
                Log.d("test", "cannot encode params");
            }

           // Init base URL
            final String url = "http://api.reedicious.com/rest/comment/place/" + placeID + "?appid=" + appid + "&token=" + token;

            Log.d("test", "request url : " + url);

            // Sending multipart/form-data
            HttpHeaders requestHeaders = new HttpHeaders();
            requestHeaders.add("accesstoken", userToken);

            // Populate the MultiValueMap being serialized and headers in an HttpEntity object to use for the request
            HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<MultiValueMap<String, Object>>(
                    formData, requestHeaders
            );

            // Create new restTemplate
            RestTemplate restTemplate = new RestTemplate(true);

            // Make the network request
            ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.POST, requestEntity, String.class);

            // Return the response
            return responseEntity.getBody();

        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        }

        return null;
    }

    @Override
    protected void onPostExecute(String responseString) {
        super.onPostExecute(responseString);

        Log.d(TAG, "response : " + responseString);

        boolean success = false;
        try {

            ObjectMapper mapper = new ObjectMapper();

            CommentResponse response = mapper.readValue(responseString, CommentResponse.class);

            listener.onCommentResponse( response );

            success = true;

        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (JsonParseException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        if(!success) {

            listener.onCommentResponse( null );
        }
    }
}
