package com.brainsourceapp.reedicious.views;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.brainsourceapp.reedicious.R;
import com.brainsourceapp.reedicious.rest.results.MenuResult;
import com.brainsourceapp.reedicious.transformers.image.RoundedTransformation;
import com.squareup.picasso.Picasso;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.res.DimensionPixelSizeRes;
import org.androidannotations.annotations.res.StringRes;

@EViewGroup(R.layout.cell_menu_list_fragment)
public class MenuCell extends RelativeLayout {

    /**
     * --------------------------------------------
     * PRIVATE METHOD
     * --------------------------------------------
     *
     */
    @DimensionPixelSizeRes(R.dimen.margin_05_)
    int moreBtnTopMargin;
    @DimensionPixelSizeRes(R.dimen.margin_10)
    int moreBtnRightMargin;

    private LayoutParams lpref;
    private void setDefaultMargin() {
        lpref = (RelativeLayout.LayoutParams) moreButton.getLayoutParams();
        assert getContext() != null;
        lpref.setMargins(0, 0, moreBtnRightMargin, 0);
    }
    private void setTopMargonAfterHideDetailView() {
        lpref = (RelativeLayout.LayoutParams) moreButton.getLayoutParams();
        assert getContext() != null;
        lpref.setMargins(0, moreBtnTopMargin, moreBtnRightMargin, 0);
    }
    private void showDetailView() {
        slideDetailView.setVisibility(VISIBLE);
        this.setDefaultMargin();
    }
    private void hideDetailView() {
        slideDetailView.setVisibility(GONE);
        this.setTopMargonAfterHideDetailView();
    }

    /**
     * --------------------------------------------
     * INTERFACES
     * --------------------------------------------
     *
     */
    public interface Callback {
        void OnMenuTitleButtonClicked(int placeID, String recipeTitle);
    }

    /**
     * --------------------------------------------
     * ATTRIBUTES
     * --------------------------------------------
     *
     */
    Callback mCallback;

    private int placeID;

    @ViewById(R.id.cell_menu_image_src)
    ImageView menuImage;

    @ViewById(R.id.cell_menu_title)
    Button titleButton;

    @ViewById(R.id.cell_menu_owner_name)
    Button ownerButton;

    @ViewById(R.id.cell_menu_slide_detail_view)
    View slideDetailView;

    @ViewById(R.id.cell_menu_more_button)
    Button moreButton;

    @ViewById(R.id.cell_menu_buy_button)
    Button buyButton;

    @ViewById(R.id.cell_menu_recipe_title)
    TextView recipeTitle;

    @ViewById(R.id.cell_menu_recipe_detail)
    TextView recipeDetail;

    @StringRes(R.string.cell_menu_title)
    String menuTitleLabel;
    @StringRes(R.string.cell_menu_author)
    String menuAuthorLabel;

    @Click(R.id.cell_menu_more_button)
    void onMoreButtonClicked() {
        toogleRecipe();
    }

    @Click(R.id.cell_menu_title)
    void OnMenuTitleClicked() {
        assert recipeTitle.getText() != null;
        mCallback.OnMenuTitleButtonClicked(placeID, recipeTitle.getText().toString());
    }

    @Click(R.id.cell_menu_image)
    void onMenuImageClicked() {
        assert recipeTitle.getText() != null;
        mCallback.OnMenuTitleButtonClicked(placeID, recipeTitle.getText().toString());
    }

    @Click(R.id.cell_menu_owner_name)
    void onMenuOwnerName() {
        assert recipeTitle.getText() != null;
        mCallback.OnMenuTitleButtonClicked(placeID, recipeTitle.getText().toString());
    }

    /**
     * --------------------------------------------
     * CONSTRUCTOR
     * --------------------------------------------
     *
     */
    public MenuCell(Context context) {
        super(context);

        try {
            mCallback = (Callback) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement MenuCellCallback");
        }
    }

    /**
     * --------------------------------------------
     * LIFE CYCLE
     * --------------------------------------------
     *
     */
    public void bind(MenuResult.Data menu) {

        this.placeID = menu.id;

        // Picasso
        try {
            Picasso.with(getContext())
                .load(menu.cover)
                .placeholder(R.drawable.default_700x500)
                .transform(new RoundedTransformation(10, 0))
                .into(menuImage);
        } catch (Exception e) {
            Log.d("test", e.getLocalizedMessage());
        }

        titleButton.setText( menuAuthorLabel + menu.title );
        ownerButton.setText( menuTitleLabel + menu.galleries.get(0).imageName );

        if(menu.recipes == null || menu.recipes.isEmpty() || menu.recipes.equals("")) {

            recipeDetail.setText("-");
        } else {

            recipeDetail.setText( menu.recipes );
        }

        /*
        Phase 1 : hide DetailView and MoreButton
         */
        this.hideDetailView();
        moreButton.setVisibility(GONE);
    }

    void toogleRecipe() {
        if(slideDetailView.getVisibility() == GONE) {
            this.showDetailView();
        } else {
            this.hideDetailView();
        }
    }


}
