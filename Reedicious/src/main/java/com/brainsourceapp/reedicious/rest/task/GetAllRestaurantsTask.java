package com.brainsourceapp.reedicious.rest.task;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.brainsourceapp.reedicious.rest.data.MyMinimart;
import com.brainsourceapp.reedicious.rest.data.RestaurantPageResponse;
import com.brainsourceapp.reedicious.rest.results.MyPlace;
import com.brainsourceapp.reedicious.rest.utils.BSHttpUtils;
import com.brainsourceapp.reedicious.utils.BSProgressHUD;
import com.google.android.gms.maps.model.LatLng;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

public class GetAllRestaurantsTask extends AsyncTask<String, Integer, String> {

    Context context;
    LatLng latLng;
    RestaurantPageResponse pageResponse;
    GetAllRestaurantCallback listener;
    String apiToken;
    int page, limit;

    public interface GetAllRestaurantCallback {
        void onFinishedGetRestaurant(RestaurantPageResponse pageResponse);
    }

    public GetAllRestaurantsTask(Context context,
                                 LatLng latLng, String apiToken,
                                 int page, int limit,
                                 GetAllRestaurantCallback listener) {
        this.context = context;
        this.latLng = latLng;
        this.apiToken = apiToken;
        this.listener = listener;
        this.page = page;
        this.limit = limit;
        this.pageResponse = new RestaurantPageResponse();
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        BSProgressHUD.showProgressHUD(context, "LOADING...");
    }

    @Override
    protected void onCancelled() {
        BSProgressHUD.dismissProgressHUD();
        super.onCancelled();
    }

    @Override
    protected String doInBackground(String... params) {

        String baseURL;

        if(this.latLng != null) {

            baseURL = BSHttpUtils.newInstance().getfullURL("http://api.reedicious.com/rest/search/place", new String[] {
                    "appid", "1",
                    "token", this.apiToken,
                    "latitude", latLng.latitude + "",
                    "longitude", latLng.longitude + "",
                    "distance", "all",
                    "cateid", "",
                    "page", page + "",
                    "limit", limit + "",
                    "minimart", "1"
            });
        } else {

            baseURL = BSHttpUtils.newInstance().getfullURL("http://api.reedicious.com/rest/search/place", new String[] {
                    "appid", "1",
                    "token", this.apiToken,
                    "distance", "all",
                    "cateid", "",
                    "page", page + "",
                    "limit", limit + "",
                    "minimart", "1"
            });
        }

        HttpGet httpGet = new HttpGet(baseURL);

        HttpClient httpClient = new DefaultHttpClient();

        try {
            HttpResponse httpResponse = httpClient.execute(httpGet);

            // Get statusCode
            int statusCode = httpResponse.getStatusLine().getStatusCode();
            Log.d("test", "get all restaurants at page " + page + " response status code : " + statusCode);

            String res = BSHttpUtils.newInstance().streamToString(httpResponse.getEntity().getContent());
            Log.d("test", "get all restaurants at page " + page + " response string : " + res);

            // Parse json here
            JSONObject jsonObject = new JSONObject(res);
            if(jsonObject.has("header")) {
                JSONObject header = jsonObject.getJSONObject("header");
                if(header.has("code"))                  pageResponse.setCode( header.getInt("code") );
                if(header.has("message"))               pageResponse.setMessage( header.getString("message"));
            }

            if(jsonObject.has("property")) {
                JSONObject property = jsonObject.getJSONObject("property");
                if(property.has("totalrecord"))         pageResponse.setTotalRecord( property.getInt("totalrecord") );
                if(property.has("totalpage"))           pageResponse.setTotalPage( property.getInt("totalpage") );
                if(property.has("page"))                pageResponse.setPage( property.getInt("page") );
                if(property.has("limit"))               pageResponse.setLimit( property.getInt("limit") );
            }

            if(jsonObject.has("data")) {
                JSONArray places = jsonObject.getJSONArray("data");
                ArrayList<MyPlace> list = new ArrayList<MyPlace>();

                for(int i=0, len=places.length(); i<len; i++) {

                    JSONObject place = places.getJSONObject(i);
                    MyPlace myPlace = new MyPlace();
                    if(place.has("id"))                 myPlace.id = place.getInt("id");
                    if(place.has("title"))              myPlace.title = place.getString("title");
                    if(place.has("desc"))               myPlace.desc = place.getString("desc");
                    if(place.has("category"))           myPlace.category = place.getInt("category");
                    if(place.has("onairdate"))          myPlace.onairdate = place.getInt("onairdate");
                    if(place.has("address"))            myPlace.address = place.getString("address");
                    if(place.has("cover"))              myPlace.cover = place.getString("cover");
                    if(place.has("latitude"))           myPlace.latitude = place.getDouble("latitude");
                    if(place.has("longitude"))          myPlace.longitude = place.getDouble("longitude");
                    if(place.has("distance"))           myPlace.distance = place.getDouble("distance") + "";
                    list.add( myPlace );
                }

                pageResponse.setmPlaces( list );
            }

            if(jsonObject.has("minimart")) {
                JSONArray marts;

                if(jsonObject.isNull("minimart")) {
                    marts = new JSONArray();
                } else {
                    marts = jsonObject.getJSONArray("minimart");
                }

                ArrayList<MyMinimart> list = new ArrayList<MyMinimart>();

                for(int i=0, len=marts.length(); i<len; i++) {

                    JSONObject mart = marts.getJSONObject(i);
                    MyMinimart myMart = new MyMinimart();
                    if(mart.has("mart_id"))             myMart.setMart_id( mart.getString("mart_id") );
                    if(mart.has("mart_name"))           myMart.setMart_name( mart.getString("mart_name") );
                    if(mart.has("mart_lat"))            myMart.setMart_lat( mart.getString("mart_lat") );
                    if(mart.has("mart_lon"))            myMart.setMart_lng( mart.getString("mart_lon"));
                    myMart.antiNull();

                    list.add( myMart );
                }

                pageResponse.setmMinimarts( list );
            }
             
            return res;

        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);

        BSProgressHUD.dismissProgressHUD();

        if(s == null) {
            Log.d("test", "get restaurants return null response");
            return;
        }

        if(pageResponse.getmPlaces() == null) {
            pageResponse.setmPlaces( new ArrayList<MyPlace>() );
        }
        if(pageResponse.getmMinimarts() == null) {
            pageResponse.setmMinimarts( new ArrayList<MyMinimart>() );
        }

        Log.d("test", "places count : " + pageResponse.getmPlaces().size() + ", minimarts count : " + pageResponse.getmMinimarts().size());

        if(this.listener != null) {
            this.listener.onFinishedGetRestaurant( pageResponse );
        }
    }
}