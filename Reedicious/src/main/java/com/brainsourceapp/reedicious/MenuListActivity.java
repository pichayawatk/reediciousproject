package com.brainsourceapp.reedicious;

import android.app.Activity;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.widget.ListView;

import com.brainsourceapp.reedicious.beans.MyAccount;
import com.brainsourceapp.reedicious.rest.ReedClient;
import com.brainsourceapp.reedicious.rest.results.MenuResult;
import com.brainsourceapp.reedicious.utils.BSProgressHUD;
import com.brainsourceapp.reedicious.views.MenuCell;
import com.brainsourceapp.reedicious.views.MenuCellAdapter;
import com.flurry.android.FlurryAgent;
import com.google.android.gms.maps.model.LatLng;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.InstanceState;
import org.androidannotations.annotations.OptionsItem;
import org.androidannotations.annotations.OptionsMenu;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.rest.RestService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@EActivity(R.layout.activity_menu_list)
@OptionsMenu(R.menu.menu_list)
public class MenuListActivity extends Activity implements MenuCellAdapter.Callback, MenuCell.Callback {

    int page;

    int total_page;

    @InstanceState
    ArrayList<MenuResult> menuResults;

    @Extra
    int catID;

    @Extra
    int limit;

    @Extra
    String mTitle;

    @ViewById(R.id.menu_list)
    ListView menuList;

    @Bean
    MenuCellAdapter adapter;

    @RestService
    ReedClient reedClient;

    @AfterViews
    void afterMenuListViews() {

        limit = 10;

        setupActionBar();

        LatLng latlng = MyAccount.getInstance().getLatestLatLng();
        String apiToken = MyAccount.getInstance().getApiToken();

        Log.d("test", "MenuListActivity started...");
        boolean isCached = false;
        if(adapter != null) {
            if(adapter.getAllDatas() != null && adapter.getAllDatas().size() > 0) {
                isCached = true;
            }
        }

        if(!isCached) {
            menuList.setAdapter( adapter );

            // TODO: show progress dialog
            BSProgressHUD.showProgressHUD(this, "LOADING");

            page = 0;

            loadMenusAsync(
                    "1",
                    apiToken,
                    latlng.latitude,
                    latlng.longitude,
                    "all",
                    catID,
                    page,
                    limit
            );
        }
    }

    @Background
    void loadMenusAsync(String appid, String apiToken, double lat, double lng, String distance, int catID, int page, int limit) {

        try {

            MenuResult result = reedClient.getMenues(appid, apiToken, lat, lng, distance, catID, page + 1, limit);
            updateUI( result );
        } catch (Exception e) {
            Log.d("test", "error load menus async : " + e.getLocalizedMessage());
            updateUI( null );
        }
    }

    @UiThread
    void updateUI(MenuResult result) {
        // TODO: dismiss progress dialog
        BSProgressHUD.dismissProgressHUD();

        if(result == null ||
                result.property == null ||
                result.menues == null || result.menues.size() == 0) {
            Log.d("test", "menu get null");

            /*      Disable alert dialog

            String errorMsg = getResources().getString(R.string.error_code_201) + mTitle;
            BSAlertDialog.newInstance(errorMsg, new BSAlertDialog.CallbackDialogListener() {
                @Override
                public void onBSDialogPosBtnClicked(DialogInterface dialog, String tag) {
                    dialog.dismiss();
                }
            }).show(getFragmentManager(), "no content");

            */

        } else {

            // Get the max pages
            total_page = result.property.totalpage;

            // Add menu into the adapter
            for(MenuResult.Data data : result.menues) {
                adapter.addItem( data );
            }

            // Notify the change
            adapter.notifyDataSetChanged();

            // Increment page
            page++;
        }
    }

    private void setupActionBar() {
        assert getActionBar() != null;
        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setTitle( mTitle );
    }

    @OptionsItem(android.R.id.home)
    void didClickedHomeMenu() {
        try {
            NavUtils.navigateUpFromSameTask(this);
        } catch (Exception e) {
            finish();
        }
    }

    /**
     *
     * MenuCellAdapter CALLBACK INTERFACE
     */
    @Override
    public void OnLastItemAppeared() {
        if(page < total_page) {

            LatLng latlng = MyAccount.getInstance().getLatestLatLng();
            String apiToken = MyAccount.getInstance().getApiToken();

            // TODO: show progress dialog
            BSProgressHUD.showProgressHUD(this, "LOADING");

            loadMenusAsync(
                    page + "",
                    apiToken,
                    latlng.latitude,
                    latlng.longitude,
                    "all",
                    catID,
                    page,
                    limit
            );
        }
    }

    @Override
    public void OnMenuTitleButtonClicked(int placeID, String recipeTitle) {

        /*
        Map<String, String> param = new HashMap<String, String>();
        param.put("screenName", MyAccount.getInstance().getAccountScreenName());
        param.put("placeTitle", placeID + "");
        param.put("recipeTitle", recipeTitle);
        FlurryAgent.logEvent("Selected Recipe From List Page", param);
        */
        Map<String, String> param = new HashMap<String, String>();
        param.put("screenName", MyAccount.getInstance().getAccountScreenName());
        param.put("placeID", placeID + "");
        FlurryAgent.logEvent("View Info From Recommended Restaurant", param);

        String placeIDStr = String.valueOf(placeID);

        DetailActivity_.intent(this).placeID( placeIDStr ).start();
    }
}