package com.brainsourceapp.reedicious.rest.results;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Gallery implements Parcelable {

    @JsonProperty(value = "url")
    public String url;

    @JsonProperty(value = "image_name")
    public String imageName;

    public Gallery(@JsonProperty(value = "url")String url, @JsonProperty(value = "image_name")String imageName) {
        this.url = url;
        this.imageName = imageName;
    }

    @Override
    public int describeContents() {
        return 2;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(url);
        dest.writeString(imageName);
    }
}
