package com.brainsourceapp.reedicious.rest.results;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.ArrayList;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SocialLoginResponse {

    public Header header;

    public ArrayList<SocialLoginResponseData> data;

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class SocialLoginResponseData {

        public String member_id;

        public String screen_name;

        public String email;

        public String accesstoken;

        public String avatar;

        public SocialLoginResponseData() {}

    }

}
