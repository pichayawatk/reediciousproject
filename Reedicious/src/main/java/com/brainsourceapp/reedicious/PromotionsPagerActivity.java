package com.brainsourceapp.reedicious;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.support.v4.app.NavUtils;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.brainsourceapp.reedicious.beans.MyAccount;
import com.brainsourceapp.reedicious.beans.MyRestBean;
import com.brainsourceapp.reedicious.fragments.SingleHotPromotionFragment;
import com.brainsourceapp.reedicious.rest.results.SingleHotPromotionResult;
import com.brainsourceapp.reedicious.transformers.DepthPageTransformer;
import com.brainsourceapp.reedicious.utils.BSProgressHUD;
import com.brainsourceapp.reedicious.views.FixedViewPager;
import com.flurry.android.FlurryAgent;
import com.google.android.gms.maps.model.LatLng;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.OptionsItem;
import org.androidannotations.annotations.OptionsMenu;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.res.StringRes;
import org.springframework.web.client.RestClientException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import uk.co.senab.photoview.PhotoViewAttacher;

@EActivity(R.layout.activity_promotions_pager)
@OptionsMenu(R.menu.promotions_pager)
public class PromotionsPagerActivity extends Activity {

    ScreenSlidePager mPagerAdapter;

    int page;

    int currentPage;

    boolean isAnimated;

    ArrayList<SingleHotPromotionResult.Data> promotions;

    @StringRes(R.string.alert_promotion_clicked)
    String alertMsg;

    @Bean
    MyRestBean myRestBean;

    @ViewById(R.id.promotions_pager_viewpager)
    FixedViewPager mViewPager;

    @ViewById(R.id.promotions_pager_next_button)
    Button nextButton;

    @ViewById(R.id.promotions_pager_prev_button)
    Button prevButton;

    @ViewById(R.id.promotions_pager_current_page)
    TextView currentPageText;

    @ViewById(R.id.promotions_pager_max_page)
    TextView maxPageText;

    @Click(R.id.promotions_pager_next_button)
    void onNextButtonClicked() {

        mViewPager.setCurrentItem(currentPage + 1, true);
    }

    @Click(R.id.promotions_pager_prev_button)
    void onPrevButtonClicked() {

        mViewPager.setCurrentItem(currentPage - 1, true);
    }

    @AfterViews
   void afterPromotionsPagerViews() {

        FlurryAgent.logEvent("Enter Promotion From Menu");

        setupActionBar();

        promotions = new ArrayList<SingleHotPromotionResult.Data>();

        mViewPager.setAdapter(new ScreenSlidePager(this));

        // References
        mPagerAdapter = (ScreenSlidePager) mViewPager.getAdapter();

        mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i2) {

            }

            @Override
            public void onPageSelected(int i) {
                currentPage = i;

                currentPageText.setText( i+1 );

                if(currentPage - 1 < 0) {
                    prevButton.setEnabled( false );
                } else {
                    if(!prevButton.isEnabled()) prevButton.setEnabled( true );
                }

                if(currentPage + 1 >= promotions.size()) {
                    nextButton.setEnabled( false );
                } else {
                    if(!nextButton.isEnabled()) nextButton.setEnabled( true );
                }
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });

        mViewPager.setPageTransformer(true, new DepthPageTransformer());

        prevButton.setEnabled(false);

        if(promotions.size() <= 1) {
            nextButton.setEnabled( false );
        }

        // show progress dialog
        BSProgressHUD.showProgressHUD(this, "LOADING");
        loadPromotionsAsync();
   }

    @Background
    void loadPromotionsAsync() {

        LatLng latlng = MyAccount.getInstance().getLatestLatLng();
        double lat = latlng.latitude;
        double lng = latlng.longitude;

        int limit = 20;

        try {
            ArrayList<SingleHotPromotionResult.Data> result = myRestBean.getPromotionsByLocation(lat, lng, ++page, limit);

            updateUI( result );
        } catch (RestClientException e) {
            Log.d("test", e.getMessage());
        }
    }

    @UiThread
    void updateUI(ArrayList<SingleHotPromotionResult.Data> result) {
        // dismiss progress dialog
        BSProgressHUD.dismissProgressHUD();

        if(result == null || result.isEmpty() || result.size() == 0) {

            showNoPromotionAlertView(true);
        } else {
            showNoPromotionAlertView(false);

            for(SingleHotPromotionResult.Data item : result) {

                promotions.add( item );
                mPagerAdapter.addItem( item );
            }

            mPagerAdapter.notifyDataSetChanged();

            maxPageText.setText( promotions.size() + "" );
        }
    }

    private void setupActionBar() {
        ActionBar actionBar = getActionBar();
        if(actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle(getResources().getString(R.string.title_activity_promotions_pager));
        }
    }

    @OptionsItem(android.R.id.home)
    void didClickedHomeMenu() {
        try {
            NavUtils.navigateUpFromSameTask(this);
        } catch (Exception e) {
            finish();
        }
    }

    @ViewById(R.id.promotions_pager_indicator)
    View pagerIndicatorView;

    @ViewById(R.id.no_promotion_view)
    View alertView;

    @UiThread
    public void showNoPromotionAlertView(boolean show) {
        if(show) {
            pagerIndicatorView.setVisibility(View.INVISIBLE);
            alertView.setVisibility(View.VISIBLE);
        } else {
            pagerIndicatorView.setVisibility(View.VISIBLE);
            alertView.setVisibility(View.INVISIBLE);
        }
    }

    public class ScreenSlidePager extends PagerAdapter {

        private PhotoViewAttacher mAttacher;
        private ImageView imageView;

        private LayoutInflater mInflater;
        private Context context;
        private ArrayList<SingleHotPromotionResult.Data> mData;

        ScreenSlidePager(Context context) {
            this.context = context;
            this.mInflater = ((Activity) context).getLayoutInflater();
            this.mData = new ArrayList<SingleHotPromotionResult.Data>();
        }

        public void addItem(SingleHotPromotionResult.Data item) {
            this.mData.add( item );
        }

        @Override
        public int getCount() {
            return promotions.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object o) {
            return view == o;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            View view = mInflater.inflate(R.layout.single_hot_promotion, null);
            assert view != null;
            Button cancelButton = (Button) view.findViewById(R.id.single_hot_promotion_closebtn);
            cancelButton.setOnClickListener( new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });


                imageView = (ImageView) view.findViewById(R.id.single_hot_promotion_imageview);

                final SingleHotPromotionResult.Data data = this.mData.get( position );

                Picasso.with(context).load(data.cover).into(imageView, new Callback() {
                    @Override
                    public void onSuccess() {
                        mAttacher = new PhotoViewAttacher( imageView );

                        mAttacher.setOnPhotoTapListener(new PhotoViewAttacher.OnPhotoTapListener() {
                            @Override
                            public void onPhotoTap(View view, float x, float y) {
                                if(data.promotionLink != null && !data.promotionLink.isEmpty() && !data.promotionLink.equals("")) {
                                    SingleHotPromotionFragment.PromotionAlertDialog.newInstance(alertMsg, data.promotionLink).show(getFragmentManager(), "alert");
                                }
                            }
                        });
                    }

                    @Override
                    public void onError() {

                    }
                });

            Map<String, String> param = new HashMap<String, String>();
            param.put("promotionID", mData.get(position).ID);
            FlurryAgent.logEvent("View Promotion", param);

                container.addView(view, 0);

                // Animation
                Animation animation = AnimationUtils.loadAnimation(context, R.anim.zoom_up_bounce);

                if(animation != null && !isAnimated) {
                    view.startAnimation(animation);
                    isAnimated = true;
                }


            return view;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }
    }


}
