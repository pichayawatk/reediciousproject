package com.brainsourceapp.reedicious;

import android.app.Activity;
import android.location.Location;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;

import com.brainsourceapp.bslib.BSLib;
import com.brainsourceapp.reedicious.beans.MyAccount;
import com.brainsourceapp.reedicious.rest.task.GetTokenTask;
import com.brainsourceapp.reedicious.services.GPSTracker;
import com.crashlytics.android.Crashlytics;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.location.LocationClient;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.App;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Fullscreen;
import org.androidannotations.annotations.UiThread;

@Fullscreen
@EActivity(R.layout.activity_splash)
public class SplashActivity extends Activity implements GooglePlayServicesClient.ConnectionCallbacks, GooglePlayServicesClient.OnConnectionFailedListener {

    int retryCount;

    public static boolean canStartMainActivity;

    boolean tokenTask;
    boolean locationTask;
    boolean timeoutTask;

    int maxTokenAttemp = 5;

    CountDownTimer timer;

    LocationClient mLocationClient;
    Location mLocation;

    @App
    MyApplication myApp;

    @AfterViews
    public void afterSplashViews() {

        canStartMainActivity = true;

        Crashlytics.start(this);
        retryCount = 1;

        BSLib.newInstance(this).startOpenApp("3269");

        timer = new CountDownTimer(5000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                Log.d("test", "will finished in : " + millisUntilFinished);
            }

            @Override
            public void onFinish() {
                Log.d("test", "finish countdown timer");
                timeoutTask = true;
                joinWork();
            }
        };

        mLocationClient = new LocationClient(this, this, this);

        this.getTokenTask();

        timer.start();

        mLocationClient.connect();
    }

    @Override
    protected void onStop() {
        timer.cancel();
        mLocationClient.disconnect();
        super.onStop();
    }

    public void getTokenTask() {
        if(!myApp.isApiToken()) {
            Log.d("test", "Updating token...");
            new GetTokenTask(new GetTokenTask.GetTokenListener() {
                @Override
                public void didFinishGetToken(String token) {
                    if(token.length() > 0) {

                        Log.d("test", "update token : " + token);
                        MyAccount.getInstance().setApiToken( token );
                        MyAccount.getInstance().setApiToken( token );
                        myApp.setApiToken( true );
                        tokenTask = true;
                        joinWork();
                    } else {

                        if(maxTokenAttemp > 0) {
                            maxTokenAttemp--;
                            Log.d("test", "max token attemp " + maxTokenAttemp + " ...");
                            getTokenTask();
                        } else {
                            onFailedOpenApp(0);
                        }
                    }
                }
            }).execute();
        }
    }

    @Override
    public void onConnected(Bundle bundle) {

        if(null == bundle) {

            useGPSLocation();
        } else {

            mLocation = mLocationClient.getLastLocation();
            Log.d("test", "location updated at ( " + mLocation.getLatitude() + ", " + mLocation.getLongitude() + ")");
            MyAccount.getInstance().setLatestLatLng( mLocation.getLatitude(), mLocation.getLongitude() );
            locationTask = true;
            joinWork();
        }

    }

    public void joinWork() {
        if(tokenTask && locationTask) { // && !timeoutTask ) {

            openMainActivity();
        }

        if(timeoutTask) {

            openMainActivity();
        }
    }

    /** 0 = token, 1 = location */
    @UiThread
    public void onFailedOpenApp(int cause) {
        String errorMsg;
        if(cause == 0) {
            errorMsg = "ApiToken";
        } else {
            errorMsg = "Location Token";
        }
        Log.d("test", "cannot open the application cause : " + errorMsg);

        useGPSLocation();
    }


    /**
     *  Location Client Callback
     *
     *
     *
     */
    @Override
    public void onDisconnected() {
        Log.d("test", "location disconnected");
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

        Log.d("test", "Google Map Location Connection Failed");
        Log.d("test", "get native android location");

        useGPSLocation();
    }

    protected void useGPSLocation() {
        GPSTracker gpsTracker = myApp.getGpsTracker(this);

        if(gpsTracker.canGetLocation()) {
            double lat = gpsTracker.getLatitude();
            double lng = gpsTracker.getLongitude();

            Log.d("test", "gps location : ( " + lat + ", " + lng + " )");
            MyAccount.getInstance().setLatestLatLng(lat, lng);

            openMainActivity();
        } else {

            GPSTracker.showSettingsAlert(this);
        }
    }

    @UiThread
    public void openMainActivity() {

        if(canStartMainActivity) {
            canStartMainActivity = false;

            MainActivity_.intent(this).start();
            finish();
        }
    }

}
